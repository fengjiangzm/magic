package tech.aistar.utill;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarkDownUtil {

    public static String getChinese(String str){

        int account = 0;
        String exp="^[\u4E00-\u9FA5|\\！|\\,|\\。|\\（|\\）|\\《|\\》|\\“|\\”|\\？|\\：|\\；|\\【|\\】]$";
        Pattern pattern=Pattern.compile(exp);

        StringBuffer b = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {// 遍历字符串每一个字符
            char c = str.charAt(i);
            Matcher matcher=pattern.matcher(c + "");
            if(matcher.matches()) {
                b.append(c);
                account++;
            }
            if (account>80) break;
        }


        return String.valueOf(b);
    }
}
