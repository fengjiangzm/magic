package tech.aistar.utill;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String dateFormat(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return sdf.format(date);
    }

    public static String dateFormat2(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
