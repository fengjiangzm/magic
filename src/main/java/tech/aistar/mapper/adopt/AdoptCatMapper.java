package tech.aistar.mapper.adopt;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.adopt.AdoptCat;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/23.
 */
public interface AdoptCatMapper {

    @Select("select * from adopt_cat where species_id = #{value} and status = 0")
    List<AdoptCat> findBySpeciesId(Integer speciesId);

    @Select("select * from adopt_cat where status = 0")
    List<AdoptCat> findAll();

    @Select("select * from adopt_cat where cat_id=#{value}")
    AdoptCat findOneById(Integer id);

    @Update("update adopt_cat set status = 1 where cat_id = #{value}")
    void delById(Integer id);

    @Insert("insert into adopt_cat(id,cat_id,growth_id,img_url,species_id,cat_price,introduction) " +
            "values(#{id},#{catId},#{growthId},#{imgUrl},#{speciesId},#{catPrice},#{introduction})")
    void save(AdoptCat adoptCat);

    @Update("update adopt_cat set cat_id=#{catId},growth_id=#{growthId},img_url=#{imgUrl},species_id=#{speciesId},cat_price=#{catPrice},introduction=#{introduction} where id=#{id}")
    void update(AdoptCat adoptCat);

    @Update("update adopt_cat set status = 0 where cat_id = #{value}")
    void updateStatus(Integer id);

}
