package tech.aistar.mapper.adopt;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.adopt.AdoptApplication;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
public interface AdoptApplicationMapper {
    @Select("select * from adopt_application")
    List<AdoptApplication> findAll();

    @Select("select * from adopt_application where ord_id=#{value}")
    AdoptApplication findOneById(Integer id);

    @Insert("insert into adopt_application(cat_id,user_name,phone,user_email,salary,identity,date,sex,taking_way,userAddress_id,note,ord_id) " +
            "values(#{catId},#{userName},#{phone},#{userEmail},#{salary},#{identity},#{date},#{sex},#{takingWay},#{userAddressId},#{note},#{ordId})")
    void save(AdoptApplication adoptApplication);

    @Update("update adopt_application set cat_id=#{catId} user_name=#{userName},phone=#{phone}" +
            ",user_email=#{userEmail},salary=#{salary},identity=#{identity},date=#{date},sex=#{sex},taking_way=#{takingWay},userAddress_id=#{userAddressId},note=#{note},ord_id=#{ordId} where id=#{id}")
    void update(AdoptApplication adoptApplication);
}
