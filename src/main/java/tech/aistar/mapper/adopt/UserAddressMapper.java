package tech.aistar.mapper.adopt;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.adopt.UserAddress;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/25.
 */
public interface UserAddressMapper {

    @Select("select * from user_address")
    List<UserAddress> findAll();

    @Select("select * from user_address where user_id=#{value}")
    List<UserAddress> findById(Integer id);

    @Select("select * from user_address where id=#{value}")
    UserAddress findOneById(Integer id);

    @Update("update user_address set status = 1 where user_id = #{value}")
    void delById(Integer id);

    @Insert("insert into user_address(name,user_id,user_phone,user_name,province_name,city_name,area_name,detail_address) " +
            "values(#{name},#{userId},#{userPhone},#{userName},#{provinceName},#{cityName},#{areaName},#{detailAddress})")
    void save(UserAddress userAddress);

    @Update("update user_address set name=#{name},user_id=#{userId},user_phone=#{userPhone},user_name=#{userName},province_name=#{provinceName},city_name=#{cityName}," +
            "area_name=#{areaName},detail_address=#{detailAddress} where id=#{id}")
    void update(UserAddress userAddress);

    @Update("update user_address set isdefault = 1 where id = #{value}")
    void setDefault(Integer id);

    @Select("select * from user_address where id=#{value}")
    UserAddress findId(Integer id);

    @Update("update user_address set status = 1 where id = #{value} ")
    void delId(Integer id);

    @Update("update user_address set isdefault = 0 where id = #{value}")
    void setDefault1(Integer id);
}
