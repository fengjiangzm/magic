package tech.aistar.mapper.adopt;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.adopt.Collect;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
public interface CollectMapper {

    @Select("select * from collect")
    List<Collect> findAll();

    @Select("select * from collect where user_id=#{value}")
    List<Collect> findById(Integer id);

    @Update("update collect set status = 1 where cat_id = #{cat_id} and user_id=#{user_id}")
    void delById(@Param(value = "cat_id") Integer cat_id,@Param(value = "user_id") Integer user_id);

    @Insert("insert into collect(user_id,cat_id,add_time) values(#{userId},#{catId},#{addTime})")
    void save(Collect adoptCat);

    @Update("update collect set user_id=#{userId},cat_id=#{catId},add_time=#{addTime},status=#{status} where id=#{id}")
    void update(Collect adoptCat);
}
