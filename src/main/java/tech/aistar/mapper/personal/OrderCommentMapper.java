package tech.aistar.mapper.personal;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.personal.OrderComment;

import java.util.List;

/**
 * Created by LJX on 2019/9/24.
 */
public interface OrderCommentMapper {

    /**
     * 通过订单id查找评论,一个订单一个评论
     */
    @Select("select * from order_comment where order_id = #{orderId}")
//    List<OrderComment> findByOrderId(Integer orderId);
    OrderComment findByOrderId(Integer orderId);

    /**
     * 更新 修改status = 1,为删除
     */
    @Update("update order_comment set status = 1 where id = #{id}")
    void update(Integer id);

    /**
     * 根据id进行查找
     */
   @Select("select * from order_comment where id = #{id}")
   OrderComment findById(Integer id);

    /**
     * 根据用户id查找
     */
    @Select("select * from order_comment where user_id=#{userId}")
    List<OrderComment> findByUserId(Integer userId);

    /**
     * 评论添加
     */
    @Insert("insert into order_comment(order_id,order_time,content,user_id,status) values(#{orderId},#{orderTime},#{content},#{userId},#{status})")
    void save(OrderComment comment);
}

