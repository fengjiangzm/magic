package tech.aistar.mapper.personal;

import org.apache.ibatis.annotations.*;
import tech.aistar.entity.personal.Purchase;

import java.util.List;

/**
 * Created by LJX on 2019/9/23.
 */
public interface PurchaseMapper {
    /**
     * 根据id查找订单
     *
     * @param id
     * @return
     */
    @Select("select * from purchase where id = #{id}")
    Purchase findById(@Param("id") Integer id);

    /**
     * 根据用户user_id查找订单
     */
    @Select("select * from purchase where user_id = #{userId}")
    List<Purchase> findByuserId(Integer userId);

    /**
     * 更新  修改status = 1
     */
    @Update("update purchase set status = 1 where id = #{value} ")
    void update(Integer id);

    /**
     * 更新  修改ordertype=1
     */
    @Update("update purchase set ordercancel = 1 where id = #{value}")
    void updateOrdercancel(Integer id);

    @Update("update purchase set payment = 1 where id = #{value}")
    void updatePayment(Integer id);

    /**
     * 增加订单
     */
    @Insert("insert into purchase(ordno,time,user_id,ordercancel,status,ordmoney,payment,comment_id,ordertype) " +
            "values(#{ordno},#{time},#{userId},#{ordercancel},#{status},#{ordmoney},#{payment},#{commentId},#{ordertype}) ")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(Purchase purchase);

    /**
     * 根据订单编号查找
     */
    @Select("select * from purchase where ordno = #{ordno}")
    Purchase findByOrderno(String ordno);

    @Select("select * from purchase")
    List<Purchase> findAll();

}
