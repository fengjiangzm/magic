package tech.aistar.mapper.personal;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.personal.User;

/**
 * Created by LJX on 2019/9/24.
 */
public interface UserMapper {
    @Select("select * from user where username = #{username}")
    User findByName(String username);

    @Insert("insert into user(username,password) values(#{username},#{password}) ")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void save(User user);
//    @Update("update user set username=#{username},password=#{password} where id = #{value}")
//    void update(Integer id);
}
