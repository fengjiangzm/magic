package tech.aistar.mapper.personal;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.personal.UserInfo;

import java.util.List;

/**
 * Created by LJX on 2019/9/24.
 */
public interface UserInfoMapper {

    /**
     * 根据id进行查找
     */
    @Select("select * from user_info where id = #{value}")
    UserInfo findById(Integer id);

    /**
     * 根据用户名查找用户信息
     *
     * @param nickname
     * @return
     */
    @Select("select * from user_info where nickname = #{value} ")
    List<UserInfo> findByName(String nickname);

    /**
     * 更新当  status=1,删除
     */
    @Update("update user_info set status = 1 where id = #{value}")
    void update(Integer id);

    /**
     * 根据id 根据user表添加
     * 添加用户信息
     */
    @Insert("insert into user_info(id,gender,birthday,cat_id,login_time,address,nickname,phone,avatar,status,add_time,update_time,useremail)" +
            " values(#{id},#{gender},#{birthday},#{catId},#{loginTime},#{address},#{nickname},#{phone},#{avatar},#{status},#{addTime},#{updateTime}," +
            "#{useremail})")
    void save(UserInfo userInfo);

    /**
     * 更新个人信息
     *
     * @param userInfo
     */
    @Update("update user_info set gender=#{gender},birthday=#{birthday},cat_id=#{catId}," +
            "login_time=#{loginTime},address=#{address},nickname=#{nickname},phone=#{phone}," +
            "avatar=#{avatar},status=#{status},add_time=#{addTime},update_time=#{updateTime},useremail=#{useremail} where id = #{id}")
    void updateById(UserInfo userInfo);

    /**
     * 根据id更新avatar头像
     */
    @Update("update user_info set avatar=#{avatar} where id = #{id}")
    void uploadById(UserInfo userInfo);

}
