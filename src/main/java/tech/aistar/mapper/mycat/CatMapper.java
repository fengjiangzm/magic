package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.*;
import tech.aistar.entity.mycat.Cat;

import java.util.List;

/**
 * Created by Administrator on 2019/9/23.
 */
public interface CatMapper {
    //根据用户id查找猫咪
    @Select("select * from cat where user_id = #{userId}")
    List<Cat> findByUserId(Integer userId);

    //根据猫咪id查找猫咪
    @Select("select * from cat where id = #{id}")
    Cat findById(Integer id);

    //根据猫咪名字进行模糊查询
    List<Cat> findAll(@Param("name") String name);

    @Select("select * from cat where user_id = -1")
    List<Cat> findAdoptCatAll();

    @Select("select id from cat where user_id = -1 and name like #{name}")
    List<Integer> findByCatName(String name);

    //删除猫咪
       @Update("update cat set status = 1 where id = #{id}")
    void delById(Integer id);

    //保存猫咪
    @Insert("insert into cat(id,name,age,sex,weight,is_sterilization,species_id,species_name,user_id,status) values(#{id},#{name},#{age},#{sex},#{weight},#{isSterilization},#{speciesId},#{speciesName},#{userId},#{status})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void save(Cat cat);

    //修改
    @Update("update cat set name=#{name},age=#{age},sex=#{sex},weight=#{weight},is_sterilization=#{isSterilization},species_id=#{speciesId},species_name=#{speciesName},user_id=#{userId},status=#{status} where id = #{id}")
    void updata(Cat cat);
}
