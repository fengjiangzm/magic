package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.CatDetail;

/**
 * Created by Administrator on 2019/10/9.
 */
public interface CatDetailMapper {
    @Select("select * from cat_detail")
    CatDetail findAll();

    @Select("select * from cat_detail where id = #{id}")
    CatDetail findById(Integer id);

    @Select("select * from cat_detail where id = #{userId}")
    CatDetail findByUserId(Integer id);

    @Insert("insert into cat_detail(id,user_id,catname,species_name,date,url_img,text) values(#{id},#{userId},#{catname},#{speciesName},#{date},#{urlImg},#{text})")
    void save(CatDetail catDetail);

}
