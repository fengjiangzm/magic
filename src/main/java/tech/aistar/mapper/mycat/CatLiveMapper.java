package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.CatLiveRequest;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface CatLiveMapper {

    @Select("select * from cat_live_request where id = #{catId}")
    CatLiveRequest findById(Integer id);
}
