package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.CatSpecies;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface CatSpeciesMapper {

    @Select("select * from cat_species")
    List<CatSpecies> findAll();

    @Select("select * from cat_species where id = #{id}")
    CatSpecies findById(Integer id);
}
