package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.CatGrowth;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface CatGrowthMapper {
    //根据猫咪id查找猫咪的成长状态

    @Select("select * from cat_growth where id = #{id}")
    CatGrowth findById(Integer id);





}
