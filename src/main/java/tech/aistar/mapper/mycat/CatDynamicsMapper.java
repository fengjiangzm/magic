package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.CatDynamics;

import java.util.List;

/**
 * Created by Administrator on 2019/10/8.
 */
public interface CatDynamicsMapper {
    @Select("select * from cat_dynamics")
    List<CatDynamics> findAll();

    @Insert("insert into cat_dynamics(id,user_id,urlimg,cat_id,text) values(#{id},#{userId},#{urlimg},#{catId},#{text})")
    void save(CatDynamics catDynamics);

    @Select("select * from cat_dynamics where id = #{id}")
    List<CatDynamics> findById(Integer id);
}
