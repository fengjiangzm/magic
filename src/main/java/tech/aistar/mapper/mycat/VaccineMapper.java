package tech.aistar.mapper.mycat;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.Vaccine;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface VaccineMapper {

    @Select("select * from vaccine where id = #{id}")
    Vaccine findById(Integer id);

    @Insert("insert into vaccine(id,cat_id,vaccine_name,date,username,user_id,user_phone,is_vaccine,text) values(#{id},#{catId},#{vaccineName},#{date},#{username},#{userId},#{userPhone},#{isVaccine},#{text})")
    void save(Vaccine vaccine);

//    cat_name,cat_age,cat_species,    #{catName},#{catAge},#{catSpecies},
}




































