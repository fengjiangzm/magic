package tech.aistar.mapper.foster;


import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.foster.CatActivity;

import java.util.List;

public interface CatActivityMapper {

    @Insert("insert into cat_activity(cat_id,content,create_time,state) values(#{catId},#{content},#{createTime},#{state})")
    void add(CatActivity catActivity);


    @Select("select * from cat_activity where cat_id = #{id}")
    List<CatActivity> findByCatId(Integer id);

    @Update("update cat_activity set cat_id = #{catId}, content = #{content},create_time = #{createTime} where id = #{id}")
    void update(CatActivity catActivity);

    @Update("update cat_activity set state = 1 where id = #{id}")
    void deleteById(Integer id);

}
