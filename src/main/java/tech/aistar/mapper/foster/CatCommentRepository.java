package tech.aistar.mapper.foster;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.aistar.entity.foster.CatComment;

import java.util.List;

public interface CatCommentRepository extends MongoRepository<CatComment,String> {

    List<CatComment> findByOwnerId(String id);

    List<CatComment> findByFromId(String fromId);
}
