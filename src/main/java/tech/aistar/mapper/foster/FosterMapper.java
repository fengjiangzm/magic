package tech.aistar.mapper.foster;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.foster.Foster;

import java.util.List;

public interface FosterMapper {

    @Insert("insert into foster(cat_id,user_id,start_time,end_time,room_id,remark,order_id,status,foster_state) " +
            "values(#{catId},#{userId},#{startTime},#{endTime},#{roomId},#{remark},#{orderId},#{status},#{fosterState})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void insert(Foster foster);

    @Select("select * from foster where id = #{id}")
    Foster findById(Integer id);

    @Update("update foster set cat_id=#{catId},user_id=#{userId},start_time=#{startTime},end_time=#{endTime},room_id=#{roomId},remark=#{remark},order_id=#{orderId} where id = #{id}")
    void updateById(Foster foster);

    @Update("update foster set status = 1 where id = #{id}")
    void deleteById(Integer id);

    @Select("select * from foster where status = 0")
    List<Foster> findAll();

    @Select("select * from foster where order_id = #{orderId}")
    Foster findByOrderId(Integer orderId);

    @Update("update foster set foster_state = 'doing' where id = #{id}")
    void startFoster(Integer id);

    @Update("update foster set foster_state = 'finished' where id = #{id}")
    void endFoster(Integer id);
}
