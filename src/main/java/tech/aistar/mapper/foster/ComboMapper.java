package tech.aistar.mapper.foster;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.foster.Combo;

import java.util.List;

public interface ComboMapper {

    @Insert("insert into combo(foster_id,event_id) values(#{fosterId},#{eventId})")
    Integer addCombo(Combo combo);

    @Select("select * from combo where event_id = #{id}")
    List<Combo> findByEventId(Integer id);

    @Select("select * from combo where foster_id = #{id}")
    List<Combo> findByFosterId(Integer id);
}
