package tech.aistar.mapper.foster;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tech.aistar.entity.foster.Room;

import java.util.List;

public interface RoomMapper {

    @Select("select * from room")
    List<Room> findAll();

    @Update("update room set number = number - 1 where id = #{id}")
    void fosterAdd(Integer id);

    @Update("update room set number = number + 1 where id = #{id}")
    void fosterLeave(Integer id);


}
