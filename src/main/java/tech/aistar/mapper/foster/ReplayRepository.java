package tech.aistar.mapper.foster;

import org.springframework.data.mongodb.repository.MongoRepository;
import tech.aistar.entity.foster.Replay;

import java.util.List;

public interface ReplayRepository extends MongoRepository<Replay,String> {

    List<Replay> findByToId(String toId);

    Replay findByCommentId(String commentId);
}
