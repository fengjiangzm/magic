package tech.aistar.mapper.foster;

import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.foster.Event;

import java.util.List;

public interface EventMapper {

    @Select("select * from event")
    List<Event> findAll();
}
