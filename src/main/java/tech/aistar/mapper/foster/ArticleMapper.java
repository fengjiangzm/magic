package tech.aistar.mapper.foster;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import tech.aistar.entity.foster.Article;

public interface ArticleMapper extends MongoRepository<Article,String> {

    Page<Article> findByTypeLike(String type, Pageable pageable);

    Page<Article> findByTitleLike(String title, Pageable pageable);
}
