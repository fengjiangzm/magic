package tech.aistar.entity.personal;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.ibatis.annotations.Param;
import tech.aistar.entity.mycat.ValidGroup1;
import tech.aistar.entity.mycat.ValidGroup2;
import tech.aistar.entity.mycat.ValidGroup3;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * Created by LJX on 2019/9/23.
 * 用户信息实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User implements Serializable {

    private Integer id;

    @NotNull(message = "用户名不能为空")
    @Pattern(regexp = "[a-z]{6}", message = "{v.user.username}", groups = {ValidGroup1.class, ValidGroup3.class})
    private String username;//用户姓名

    @NotNull(message = "密码不能为空")
    @Pattern(regexp = "[0-9]{6}", message = "{v.user.password}", groups = {ValidGroup2.class, ValidGroup3.class})
    private String password;//用户密码


}
