package tech.aistar.entity.personal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LJX on 2019/9/24.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserInfo implements Serializable {

    private Integer id;
    //  private Integer userId;//用户id
//  private String username;
//  private String password;

    private Integer gender;//用户性别0/表示女

    private Date birthday;

    private Integer catId;

    private Date loginTime;//登录时间

    private String address;//地址

    private String nickname;//昵称

    private String phone;//手机号

    private String avatar;//头像

    private Integer status;//状态 0，没删

    private Date addTime;//个人信息添加时间

    private Date updateTime;//修改时间

    private String useremail;

    public UserInfo(Integer id) {
        this.id = id;
    }

}
