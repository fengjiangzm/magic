package tech.aistar.entity.personal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by LJX on 2019/9/23.
 * 评论实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderComment implements Serializable {

    private Integer id;

    private Integer orderId;

//    private String orderType;
    private Date orderTime;

    private String content;

    private Integer userId;

    private Integer status;


}
