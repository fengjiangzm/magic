package tech.aistar.entity.personal;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by LJX on 2019/9/23.
 * 订单实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Purchase implements Serializable {
    private Integer id;

    //订单编号
    private String ordno;

    //订单时间
    private Date time;

    //用户id
    private Integer userId;

    //用户是否取消订单，0取消,1已取消
    private Integer ordercancel;

    //是否删除订单/1，删除
    private Integer status;

    //订单状态，是否付款，1已付款
    private Integer payment;

    //订单金额
    private Double ordmoney;

    //评论id
    private Integer commentId;

    //订单类型，0寄养，1领养
    private Integer ordertype;

}
