package tech.aistar.entity.personal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import tech.aistar.entity.mycat.Cat;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by LJX on 2019/10/15.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatCollect implements Serializable {
   private  Cat  cat;
   private Date addTime;
   private Double catPrice;
   private String introduction;
   private Integer status;
   private String imgUrl;
}
