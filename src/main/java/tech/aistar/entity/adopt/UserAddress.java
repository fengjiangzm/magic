package tech.aistar.entity.adopt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by 陆锦鹏 on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserAddress implements Serializable{

    private Integer id;

    private String name;    //该地址的备注信息,比如该地址是家还是公司还是别的地方

    private Integer userId;

    private String userPhone;

    private String userName;

    private String provinceName;

    private String cityName;

    private String areaName;

    private String detailAddress;

    private Integer isdefault;    //该地址是不是默认地址 0是默认

    private Integer status;    //是否删除
}
