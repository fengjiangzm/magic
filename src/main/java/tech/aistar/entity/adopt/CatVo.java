package tech.aistar.entity.adopt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by 陆锦鹏 on 2019/9/26.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatVo implements Serializable{

    private Integer catId;

    private String imgUrl;    //猫咪图片地址

    private Double catPrice;

    private String speciesName;

    private String introduction;    //猫咪的介绍

}
