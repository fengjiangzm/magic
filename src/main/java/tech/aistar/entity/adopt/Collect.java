package tech.aistar.entity.adopt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by 陆锦鹏 on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Collect implements Serializable{

    private Integer id;

    private Integer userId;

    private Integer catId;

    private Date addTime;    //添加时间

    private Integer status;    //0代表在页面显示  1代表从页面删除显示
}
