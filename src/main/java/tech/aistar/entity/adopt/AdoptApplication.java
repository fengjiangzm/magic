package tech.aistar.entity.adopt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by 陆锦鹏 on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AdoptApplication implements Serializable{

    private Integer id;

    private Integer catId;

    private String userName;

    private String phone;

    private String userEmail;

    private Double salary;

    private String identity;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date date;

    private Integer sex;    //0代表男   1代表女

    private Integer takingWay;    //0代表到店自取  1代表店里配送

    private Integer userAddressId;

    private String note;

    private Integer ordId;

}
