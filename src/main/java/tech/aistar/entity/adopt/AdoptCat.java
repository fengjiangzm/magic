package tech.aistar.entity.adopt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by 陆锦鹏 on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AdoptCat implements Serializable{

    private Integer id;

    private Integer catId;

    private Integer growthId;

    private String imgUrl;    //猫咪图片地址

    private Integer speciesId;

    private Double catPrice;

    private String introduction;    //猫咪的介绍

    private Integer status;    //0代表没有删除  1代表删除

}
