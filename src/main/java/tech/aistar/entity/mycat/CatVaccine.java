package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * Created by Administrator on 2019/9/24.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatVaccine {
    private Integer id;

    private Integer catId;

    private Integer vaccineId;
}
