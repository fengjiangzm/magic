package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Created by Administrator on 2019/10/6.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatDetail {
    private Integer id;

    private Integer userId;

    private String catname;

    private String speciesName;

    private Date date;

    private String urlImg;

    private String text;


}
