package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by Administrator on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatLiveRequest {
    //猫咪生活所需表id
    private Integer id;

    //猫咪种类名称
    private String speciesName;

    private Integer speciesId;

    private Integer catId;
    //备注
    private String text;
}
