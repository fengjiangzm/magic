
package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Created by Administrator on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Vaccine {
    //疫苗id
    private Integer id;
    //猫咪id
    private Integer catId;
    //疫苗名称
    private String vaccineName;
    //日期
    private Date date;

    private String username;

    private Integer userId;

    private String userPhone;

//    private String catName;
//
//    private Integer catAge;
//
//    private String catSpecies;

    private Integer isVaccine;

    private String text;
}
