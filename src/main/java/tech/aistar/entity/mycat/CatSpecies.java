package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/9/23.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatSpecies implements Serializable{
    private Integer id;

    private String speciesName;
    //对品种的简述
    private String description;
}
