-- auto Generated on 2019-10-10 18:05:35 
DROP TABLE IF EXISTS `cat_dynamics`;
CREATE TABLE `cat_dynamics`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `user_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'userId',
    `urlimg` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'urlimg',
    `cat_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'catId',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`cat_dynamics`';
