package tech.aistar.entity.mycat;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * Created by Administrator on 2019/9/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatGrowth {
    //成长表id
    private Integer id;

    //猫咪id
    private Integer catId;

    //猫咪生日
    private Date birthday;

    //猫咪年龄
    private Integer catAge;

    //描述
    private String description;
}
