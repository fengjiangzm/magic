package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by Administrator on 2019/10/6.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatDynamics {
    private Integer id;

    private Integer userId;

    private String urlimg;

    private Integer catId;

    private String text;
}
