package tech.aistar.entity.mycat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by Administrator on 2019/9/23.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Cat {
    //猫的ID
    private Integer id;
    //猫的名字
    private String name;
    //年龄
    private Integer age;
    //性别
    private Integer sex;
    //体重
    private Double weight;
    //是否绝育
    private Integer isSterilization;
    //种类
    private Integer speciesId;
    //种类名字
    private String speciesName;
    //主人id
    private Integer userId;
    //状态
    private Integer status;
}
