package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 服务和订单的多对多关系类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Combo {
    private Integer id;

    //服务的id
    private Integer eventId;

    //寄养订单的id
    private Integer fosterId;
}
