package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 服务表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Event {
    //服务的id
    private Integer id;

    //服务的名字(洗澡)
    private String name;

    //服务的价格(998)
    private Double price;

    //服务的描述
    private String description;

}
