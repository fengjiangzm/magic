package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 房间表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Room {
    //房间的id
    private Integer id;

    //房间的名称(豪华总统套)
    private String name;

    //剩余房间的数量
    private Integer number;
}
