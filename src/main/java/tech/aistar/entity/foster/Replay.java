package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Replay {
    //主键Id
    @Id
    private String id;

    //父评论id，即父亲的id
    @Field
    private String commentId;

    //评论者id
    @Field
    private String fromId;

    //评论者昵称
    @Field
    private String fromName;

    //被评论者id
    @Field
    private String toId;

    //被评论者昵称
    @Field
    private String toName;

    //评论内容
    @Field
    private String content;

    //评论时间
    @Field
    private String date;
}
