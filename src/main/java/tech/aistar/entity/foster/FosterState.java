package tech.aistar.entity.foster;

public enum FosterState {
    //等待寄养
    waiting,
    //寄养中
    doing,
    //寄养完成
    finished
}
