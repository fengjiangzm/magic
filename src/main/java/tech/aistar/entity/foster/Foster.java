package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * 寄养表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Foster {
    //寄养id
    private Integer id;

    //寄养服务的猫
    private Integer catId;

    //寄养订单的所属用户
    private Integer userId;

    //寄养的开始时间
    private Date startTime;

    //寄养的结束时间
    private Date endTime;

    //寄养的房间id
    private Integer roomId;

    //寄养的备注信息
    private String remark;

    //维护一个订单id
    private Integer orderId;

    //寄养状态
    private FosterState fosterState;

    //status 是判断是否删除 0 为存在 1 为删除
    private Integer status;
}
