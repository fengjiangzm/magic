package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Article {

    @Id
    private String id;

    @Field
    private String title;

    @Field
    private String content;

    @Field
    private String createDate;

    @Field
    private String desc;

    @Field
    private String type;

    @Field
    private Integer star;

    @Field
    private Integer viewNumber;

}
