package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatActivity {
    private Integer id;

    private Integer catId;
    //活动内容
    private String content;
    //创建时间
    private Date createTime;
    //是否被删除
    private Integer state;


}
