package tech.aistar.entity.foster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CatComment {
    @Id
    private String id;

    //评论时间
    @Field
    private String date;

    //动态的Id
    @Field
    private String ownerId;

    //评论者Id
    @Field
    private String fromId;

    //评论者昵称
    @Field
    private String fromName;

    //点赞数
    @Field
    private String likeNum;

    //评论内容
    @Field
    private String content;

    @Field
    private List<Replay> replayList;

}
