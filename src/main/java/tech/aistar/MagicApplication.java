package tech.aistar;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@MapperScan(basePackages = "tech.aistar.mapper")
@EnableTransactionManagement
@Transactional
@ServletComponentScan
public class MagicApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicApplication.class, args);
    }

}
