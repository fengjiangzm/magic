package tech.aistar.controller.foster;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tech.aistar.entity.adopt.AdoptCat;
import tech.aistar.entity.foster.Article;
import tech.aistar.entity.foster.Foster;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.entity.personal.Purchase;
import tech.aistar.service.adopt.IAdoptCatService;
import tech.aistar.service.foster.IArticleService;
import tech.aistar.service.foster.IFosterService;
import tech.aistar.service.foster.IRoomService;
import tech.aistar.service.mycat.ICatService;
import tech.aistar.service.personal.IPurchaseService;
import tech.aistar.utill.DateUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/man")
public class ManController {

    @Autowired
    private IPurchaseService purchaseService;

    @Autowired
    private IFosterService fosterService;

    @Autowired
    private IRoomService roomService;

    @Autowired
    private ICatService catService;

    @Autowired
    private IAdoptCatService adoptCatService;

    @Autowired
    private IArticleService articleService;

    @PostMapping("/addArticle")
    public void addArticle(@RequestBody Article article) {
        article.setCreateDate(DateUtil.dateFormat(new Date()));
        System.out.println(article);
//        articleService.add(article);
    }


    @GetMapping("/purchase")
    public @ResponseBody
    List<Purchase> getAllPurchase() {
        return purchaseService.findAll();
    }


    @GetMapping("/foster")
    public @ResponseBody
    List<Foster> getAllFoster() {
        return fosterService.findAll();
    }


    //将订单状态转为正在寄养
    @PostMapping("/startFoster/{id}")
    public void postFosterState(@PathVariable Integer id) {
        //将寄养状态设置为正在进行
        fosterService.startFoster(id);
    }

    //不知道哪有问题打个注释

    //将订单状态转为正在寄养
    @PostMapping("/endFoster/{id}")
    public void endFosterState(@PathVariable Integer id) {
        //将寄养状态设置为完成
        fosterService.endFoster(id);

        //将房间退出来
        roomService.fosterLeave(fosterService.findById(id).getRoomId());
    }


    @PostMapping("/addCat")
    public void addCat(@RequestBody JSONObject json) {
        //创建猫咪
        System.out.println(json);
        Cat cat = new Cat();
        //这只猫咪名字
        cat.setName(json.getString("name"));

        //猫咪种类
        cat.setSpeciesId(1);

        //年龄
        cat.setAge(1);

        cat.setStatus(0);
        //性别
        cat.setSex(0);

        catService.save(cat);


        //创建待领养猫咪

        AdoptCat adoptCat = new AdoptCat();
        adoptCat.setId(cat.getId());
        adoptCat.setCatId(cat.getId());
        adoptCat.setGrowthId(cat.getId());
        adoptCat.setCatPrice(json.getDouble("price"));
        adoptCat.setIntroduction(json.getString("desc"));
        adoptCat.setStatus(0);
        adoptCatService.save(adoptCat);

    }

}
