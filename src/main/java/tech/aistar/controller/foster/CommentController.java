package tech.aistar.controller.foster;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tech.aistar.entity.foster.CatComment;
import tech.aistar.entity.foster.Replay;
import tech.aistar.entity.personal.User;
import tech.aistar.entity.personal.UserInfo;
import tech.aistar.service.foster.ICatCommentService;
import tech.aistar.service.foster.IReplayService;
import tech.aistar.service.personal.IUserService;
import tech.aistar.utill.DateUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequestMapping("/comment")
@Controller
public class CommentController {

    @Autowired
    private ICatCommentService catCommentService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IReplayService replayService;

    @RequestMapping("/addReplay")
    @ResponseBody
    public CatComment addReplay(Replay replay, HttpServletRequest request) {

        if (replay.getContent().trim().equals(""))
            return null;
        replay.setId("1");

        //父评论id，即父亲的id
        //commentId;
        CatComment catComment = catCommentService.findById(replay.getCommentId());

        User user = (User) request.getSession().getAttribute("user");
        UserInfo userInfo = (UserInfo) request.getSession().getAttribute("userInfo");

        replay.setFromId(String.valueOf(user.getId()));
        replay.setFromName(String.valueOf(userInfo.getNickname()));

        //评论时间
        //date;
        replay.setDate(DateUtil.dateFormat(new Date()));

//        System.out.println(replay);
        replayService.save(replay);

        catComment.getReplayList().add(replay);

        catCommentService.updateComment(catComment);

        return catComment;
//        return "redirect:/comment/commentView";
    }


    @GetMapping("/getReplay")
    @ResponseBody
    public List<Replay> findByToId(String toId){
        List<Replay> replays = replayService.findByToId(toId);
        return replays;
    }


    @RequestMapping("/addComment")
    @ResponseBody
    public CatComment addComment(CatComment catComment, HttpServletRequest request) {

        if (catComment.getContent().trim().equals(""))
            return null;

        //评论时间
        catComment.setDate(DateUtil.dateFormat(new Date()));

        //ownerId  猫的id 文章的id

        //fromId
        User user = (User) request.getSession().getAttribute("user");
        UserInfo userInfo = (UserInfo) request.getSession().getAttribute("userInfo");
        catComment.setFromId(String.valueOf(user.getId()));
        catComment.setFromName(userInfo.getNickname());

        //likeNum
        catComment.setLikeNum("0");

        //content

        //replayList
        catComment.setReplayList(new ArrayList<Replay>());

        System.out.println(catComment);
        catCommentService.addCatComment(catComment);

        return catComment;
//        return "redirect:/comment/commentView";
    }



    @RequestMapping("commentView")
    public String CommentView(Model model) {
        model.addAttribute("comments", catCommentService.findAll());
        return "foster/commentTemplate";
    }

}
