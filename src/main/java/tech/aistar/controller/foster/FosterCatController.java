package tech.aistar.controller.foster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.aistar.entity.foster.Article;
import tech.aistar.service.foster.IArticleService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("fosterCat")
public class FosterCatController {

    @Autowired
    private IArticleService articleService;

    //跳转myCat界面
    @RequestMapping("/myCat")
    public String test() {
        //添加无效代码,用于展示提交
        //再来两行
        return "foster/myCat";
    }

    @GetMapping("/articleView")
    public String articleView(Model model,String id) {
        Article article = articleService.findById(id);
        System.out.println(article);
        model.addAttribute("article", article);
        return "foster/article";
    }

}
