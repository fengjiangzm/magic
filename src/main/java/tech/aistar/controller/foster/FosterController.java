package tech.aistar.controller.foster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import tech.aistar.entity.foster.*;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.entity.personal.Purchase;
import tech.aistar.entity.personal.User;
import tech.aistar.service.foster.*;
import tech.aistar.service.mycat.ICatService;
import tech.aistar.service.personal.IPurchaseService;
import tech.aistar.utill.DateUtil;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/foster")
public class FosterController {

    @Autowired
    private IRoomService roomService;

    @Autowired
    private IFosterService fosterService;

    @Autowired
    private ICatService catService;

    @Autowired
    private IEventService eventService;

    @Autowired
    private IComboService comboService;

    @Autowired
    private IPurchaseService purchaseService;

    @Autowired
    private ICatCommentService catCommentService;

    //跳转到我的一只猫咪界面
    // /myCatView?catId = 1
    @RequestMapping("/myCatView")
    public String myCat(Model model, Integer catId, HttpServletRequest request) {
        Cat cat = catService.findById(catId);
        model.addAttribute("cat", cat);
        request.getSession().setAttribute("cat", cat);
        model.addAttribute("comments", catCommentService.findAll());

        return "foster/myCat";
    }

    @RequestMapping("/myCatUpdate")
    public String myCatUpdate(Cat cat, HttpServletRequest req) {
        User user = (User) req.getSession().getAttribute("user");
        cat.setUserId(user.getId());
        cat.setIsSterilization(1);
        cat.setSpeciesId(1);
        cat.setStatus(1);
        catService.updata(cat);
        return "redirect:myCatView?catId=" + cat.getId();
    }


    //创建寄养订单
    @RequestMapping("/fosterCreate")
    public String fosterCreateAction(Foster foster, Double price, HttpServletRequest request, String[] events) {
        //用户id通过session中获取
        User user = (User) request.getSession().getAttribute("user");
        System.out.println("price:" + price);
        //待添加
        //添加order
        //回显order的id并赋值给foster的orderId
        Purchase purchase = new Purchase(null, "1998", new Date(), user.getId(), 0, 0, 0, price, 1, 0);
        purchaseService.save(purchase);
        //通过mapper可以获取回显的数据
        //在purchase中会回显id 的值
//        purchase.getId();
        //然后给foster设置订单的id
        foster.setOrderId(purchase.getId());
        //给foster设置id
        foster.setUserId(user.getId());
//        foster.setUserId(1);
        //status 是判断是否删除 0 为存在 1 为删除
        foster.setStatus(0);

//        System.out.println(foster);

        //设置寄养状态为待寄养
        foster.setFosterState(FosterState.waiting);

        fosterService.insert(foster);

        System.out.println(foster.getId());
        //获取服务的id
        if (null != events) {
            for (String event : events) {
                comboService.add(foster.getId(), Integer.valueOf(event));
                System.out.println(event);
            }
        }

        //添加成功后需将对应房间型号的房间的数量建议
        roomService.fosterAdd(foster.getRoomId());

        return "redirect:/alipay/pay/web?amt=" + purchase.getOrdmoney() + "&orderId=" + purchase.getId();
    }


    //跳转到寄养创建界面
    @RequestMapping("/fosterCreateView")
    public String fosterCreateView(Model model, HttpServletRequest request) {

        if (null == request.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) request.getSession().getAttribute("user");
        //通过catService获取该用户的猫咪并发送给页面
        //先设置为1
        List<Cat> cats = catService.findByUserId(user.getId());
//        System.out.println(cats);
        model.addAttribute("cats", cats);
        request.getSession().setAttribute("cats", cats);

        //获取房间信息
        List<Room> rooms = roomService.findAll();
        model.addAttribute("rooms", rooms);


        //获取服务列表
        List<Event> events = eventService.findAll();
        model.addAttribute("events", events);


        return "foster/fosterCreate";
    }

    //跳转到寄养介绍界面
    @RequestMapping("/fosterView")
    public String fosterView() {
        return "foster/foster";
    }

    //跳转到主页
    @RequestMapping("/index")
    public String indexView() {
        return "index";
    }

    @RequestMapping("/comment")
    public String CommentView(Model model) {
        model.addAttribute("comments", catCommentService.findAll());

        return "foster/commentTemplate";
    }


    @RequestMapping("/confirmView")
    public String confirmView() {
        return "foster/confirm";
    }

    //让时间数据返回的格式为yyyy-MM-dd
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
