package tech.aistar.controller.personal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by LJX on 2019/9/27.
 */
@Controller
@RequestMapping("/aboutus")
public class aboutusController {
    @RequestMapping("/aboutusView")
    public String aboutusView() {
        return "personal/aboutus";
    }
}
