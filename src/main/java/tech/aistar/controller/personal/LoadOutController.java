package tech.aistar.controller.personal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by LJX on 2019/9/28.
 */
@Controller
@RequestMapping("/load")
public class LoadOutController {
    @RequestMapping("/out")
    public String loadoutlist(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().invalidate();
        return "index";
    }
}
