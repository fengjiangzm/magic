package tech.aistar.controller.personal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import tech.aistar.entity.personal.UserInfo;
import tech.aistar.service.personal.IUserInfoService;
import tech.aistar.utill.UploadImgUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by LJX on 2019/10/8.
 */
@Controller
@RequestMapping("/per")
public class personaluploadController {
    @Autowired
    private IUserInfoService iUserInfoService;
    @RequestMapping("/imgupload")
    @ResponseBody
    public Map upload(MultipartFile myfile) {
        Map<String,String> map = new HashMap<>();

        try {
            String imgUrl = UploadImgUtil.upload(myfile.getBytes(),myfile.getOriginalFilename());
            map.put("imgUrl", imgUrl);

        } catch (IOException e) {
            e.printStackTrace();
            map.put("imgUrl", null);

        }
        //因为要作为结果返回
        return map;
    }


}
