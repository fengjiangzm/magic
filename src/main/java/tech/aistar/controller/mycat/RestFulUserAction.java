package tech.aistar.controller.mycat;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author success
 * @version 1.0
 * @description:本类用来演示:
 * @date 2019/9/23 0023
 */
//@Controller
public class RestFulUserAction {
    /**
     * GetMapping - 动作 - 查询
     * url的参数使用{参数占位符}
     * @return
     */
    @GetMapping("/user/{id}")
    public String get(Integer id){
        System.out.println("get_id:"+id);
        return "";
    }

    @DeleteMapping("/user/{id}")
    public String del(Integer id){
        System.out.println("del_id:"+id);
        return "";
    }
}
