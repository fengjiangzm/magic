package tech.aistar.controller.mycat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.entity.mycat.CatDetail;
import tech.aistar.entity.mycat.CatDynamics;
import tech.aistar.entity.personal.User;
import tech.aistar.service.mycat.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2019/10/10.
 */
@Controller
@RequestMapping("/mycat")
public class CatDynamicsController {
    @Autowired
    private ICatService catService;

    @Autowired
    private ICatSpeciesService catSpeciesService;

    @Autowired
    private CatDynamicsService catDynamicsService;

    @Autowired
    private ICatdetailService catdetailServiceservice;
    //热门猫咪
    @RequestMapping("catshow")
    public String upload(HttpServletRequest req, Model model, CatDynamics catDynamics){
        return "mycat/catshow";
    }


    //跳转到猫咪展示
    @GetMapping("/catshow")
    public String catShow() {
        return "mycat/catshow";
    }

    //发个动态
    @RequestMapping("addimg")
    public String mycatDynamics(HttpServletRequest request, Model model,CatDynamics catDynamics){

        if (null == request.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) request.getSession().getAttribute("user");
        catDynamics.setUserId(user.getId());
        String urlimg= request.getParameter("urlimg");
        catDynamicsService.save(catDynamics);
        return "forward:catdetail";
    }

    @GetMapping("/catdynamics")
    public String catDynamics(HttpServletRequest request, Model model, @ModelAttribute CatDynamics catDynamics,Integer catId){
        if (null == request.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) request.getSession().getAttribute("user");
        //通过catService获取该用户的猫咪并发送给页面
        List<Cat> cats = catService.findByUserId(user.getId());
        model.addAttribute("cats", cats);
        request.getSession().setAttribute("cats", cats);
        return "mycat/catdynamics";
    }



    //跳转到猫咪详情
    @GetMapping("/catdetail")
    public String catDetail(Model model,HttpServletRequest request,CatDetail catDetail,CatDynamics catDynamics) {
        if (null == request.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) request.getSession().getAttribute("user");
        //设置值
        catDetail.setUserId(user.getId());
        catDetail.setDate(new Date());
        System.out.println(catDynamics);
        catDetail.setUrlImg(catDynamics.getUrlimg());
        Cat cat = catService.findById(catDynamics.getCatId());
        catDetail.setCatname(cat.getName());
        catDetail.setSpeciesName(cat.getSpeciesName());
        System.out.println(cat);
        catdetailServiceservice.save(catDetail);

        //放入到作用域中
        model.addAttribute("cat", cat);
        return "mycat/catdetail";
    }

    //动态详情
    @RequestMapping("catdetailView")
    public String catdetail(HttpServletRequest request,Model model,CatDetail catDetail){

        return "mycat/hotcatdetail";
    }

    @GetMapping("hotcatdetail")
    public String hotcatdetail(HttpServletRequest request,Model model,CatDetail catDetail){

        return "mycat/hotcatdetail";
    }
}
