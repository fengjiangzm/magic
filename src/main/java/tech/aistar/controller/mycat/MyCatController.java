package tech.aistar.controller.mycat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.aistar.entity.mycat.*;
import tech.aistar.entity.personal.User;
import tech.aistar.service.mycat.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2019/9/27.
 */
@Controller
@RequestMapping("/mycat")
public class MyCatController {
    @Autowired
    private ICatService catService;

    @Autowired
    private IVaccineService vaccineService;

    @Autowired
    private ICatSpeciesService catSpeciesService;

    @Autowired
    private CatDynamicsService catShowService;

    @Autowired
    private ICatdetailService catdetailServiceservice;

    //添加猫咪界面
    @RequestMapping("/addcatView")
    public String addCat(Model model, HttpServletRequest req, Cat cat) {
        if (null == req.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) req.getSession().getAttribute("user");
        cat.setUserId(user.getId());
        cat.setStatus(0);
        //获取到猫咪类型
        cat.setSpeciesName(catSpeciesService.findById(cat.getSpeciesId()).getSpeciesName());
        catService.save(cat);
        return "redirect:/foster/fosterCreateView";

    }

    @RequestMapping("/vaccineView")
    public String vaccine(Model model, HttpServletRequest request, Vaccine vaccine,Cat cat) {
        if (null == request.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) request.getSession().getAttribute("user");
        System.out.println(user);
        vaccine.setUserId(user.getId());


        vaccineService.save(vaccine);
        return "redirect:/foster/fosterCreateView";
    }


    //跳转到疫苗表单
    @RequestMapping("/vaccine")
    public String Vaccine(Model model, HttpServletRequest request, Vaccine vaccine,Cat cat) {
        if (null == request.getSession().getAttribute("user")) {
            return "redirect:/user/loginView";
        }
        //通过session获取用户信息
        User user = (User) request.getSession().getAttribute("user");
        cat.setUserId(user.getId());
        //通过catService获取该用户的猫咪并发送给页面
        List<Cat> cats = catService.findByUserId(user.getId());
        model.addAttribute("cats", cats);
        request.getSession().setAttribute("cats", cats);

        List<CatSpecies> catSpecies = catSpeciesService.findAll();
        model.addAttribute("catSpecies", catSpecies);
        return "mycat/vaccine";
    }

    //跳转到添加猫咪表单
    @GetMapping("/addcat")
    public String registerview(Model model) {
        List<CatSpecies> catSpecies = catSpeciesService.findAll();
        model.addAttribute("catSpecies", catSpecies);
        return "mycat/addcat";
    }



    @GetMapping("/catstrategy")
    public String catStrtegy() {
        return "mycat/catstrategy";
    }

    @GetMapping("/catstrategy-link")
    public String catstrategyLink() {
        return "mycat/catstrategy-link";
    }

    @GetMapping("/catwiki")
    public String catWiki() {
        return "mycat/catwiki";
    }

    @GetMapping("/catwiki-link")
    public String catWikiLink() {
        return "mycat/catwiki-link";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
