package tech.aistar.controller.adopt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import tech.aistar.entity.adopt.AdoptApplication;
import tech.aistar.entity.adopt.AdoptCat;
import tech.aistar.entity.adopt.UserAddress;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.entity.mycat.CodeStatus;
import tech.aistar.entity.mycat.Result;
import tech.aistar.entity.personal.Purchase;
import tech.aistar.entity.personal.User;
import tech.aistar.service.adopt.IAdoptApplicationService;
import tech.aistar.service.adopt.IAdoptCatService;
import tech.aistar.service.adopt.IUserAddressService;
import tech.aistar.service.mycat.ICatService;
import tech.aistar.service.personal.IPurchaseService;
import tech.aistar.utill.DateUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/27.
 */
@Controller
@RequestMapping("/createAdopt")
public class AdoptCreateController {

    @Autowired
    private ICatService catService;

    @Autowired
    private IPurchaseService purchaseService;

    @Autowired
    private IUserAddressService userAddressService;

    @Autowired
    private IAdoptCatService adoptCatService;

    @Autowired
    private IAdoptApplicationService adoptApplicationService;

    //跳转到填写领养表界面
    @RequestMapping("/view")
    public @ResponseBody
    Result view(HttpServletRequest request) {

        User user = (User) request.getSession().getAttribute("user");

        if (user == null) {
            return new Result(CodeStatus.ERROR, false, "请您先登录!");
        }

        return new Result(CodeStatus.OK, true, "前往填写领养表!");
    }

    @RequestMapping("/createView")
    public String createView(Model model, HttpServletRequest request, @ModelAttribute UserAddress userAddress) {

        User user = (User) request.getSession().getAttribute("user");

        userAddress.setUserId(user.getId());

        if (userAddress.getProvinceName() != null) {
            userAddressService.save(userAddress);
        }

        List<UserAddress> userAddresses = userAddressService.findById(user.getId());

        model.addAttribute("userAddresses", userAddresses);

        String cat_id = request.getParameter("cat_id");

        Cat cat = catService.findById(Integer.valueOf(cat_id));

        model.addAttribute("cat", cat);

        return "adopt/adoptCreate";
    }

    //选择地址后将姓名电话自动填入到表单中
    @RequestMapping("selectAddress")
    @ResponseBody
    public Result selectAddress(String addressId){

        UserAddress userAddress = userAddressService.findOneById(Integer.valueOf(addressId));
//        System.out.println(userAddress);

        return new Result(CodeStatus.OK,true,"成功!",userAddress);
    }

    //跳转到确认订单页面并显示所填的信息
    @RequestMapping("/create")
    public String create(@ModelAttribute AdoptApplication adoptApplication, HttpServletRequest request, Model model) {

        User u = (User) request.getSession().getAttribute("user");

        String cat_id = request.getParameter("cat_id");

        Cat cat = catService.findById(Integer.valueOf(cat_id));
        UserAddress userAddress = userAddressService.findOneById(adoptApplication.getUserAddressId());

        AdoptCat adoptCat = adoptCatService.findOneById(Integer.valueOf(cat_id));
//        System.out.println(DateUtil.dateFormat(adoptApplication.getDate()).split(" ")[0]);

        model.addAttribute("date",DateUtil.dateFormat(adoptApplication.getDate()).split(" ")[0]);

        model.addAttribute("adoptCat",adoptCat);
        model.addAttribute("adoptApplication", adoptApplication);
        model.addAttribute("cat", cat);
        model.addAttribute("userAddress", userAddress);

        return "adopt/orderConfirm";
    }

    //创建订单和申请表并写入数据库
    @RequestMapping("/complete")
    @ResponseBody
    public Result complete(@ModelAttribute AdoptApplication adoptApplication, HttpServletRequest request, String cat_id,String dateTime) {

//        System.out.println(dateTime);
        User u = (User) request.getSession().getAttribute("user");
        AdoptCat adoptCat = adoptCatService.findOneById(Integer.valueOf(cat_id));

        Purchase order = new Purchase();

        order.setOrdno("123");
        order.setTime(new Date());
        order.setUserId(u.getId());
        order.setOrdmoney(adoptCat.getCatPrice());
        order.setStatus(0);
        order.setOrdercancel(0);
        order.setPayment(0);
        order.setOrdertype(1);

        adoptApplication.setDate(new Date(dateTime));

//        System.out.println(adoptApplication);
//        System.out.println(order);
//        System.out.println(cat_id);

        purchaseService.save(order);
        adoptApplication.setCatId(Integer.valueOf(cat_id));
        adoptApplication.setOrdId(order.getId());
        adoptApplicationService.save(adoptApplication);
        adoptCatService.delById(Integer.valueOf(cat_id));

        return new Result(CodeStatus.OK,true,"订单创建成功!",order.getId());
    }

    //跳转到订单完成页面
    @RequestMapping("/completeView")
    public String completeView(String orderId) {

        purchaseService.updatePayment(Integer.valueOf(orderId));

        return "adopt/orderComplete";
    }
}
