package tech.aistar.controller.adopt;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alipay.api.AlipayConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import tech.aistar.AlipayClient;
import tech.aistar.pay.alipay.core.AlipayCore;
import tech.aistar.pay.alipay.entity.request.AlipayRefundRequest;
import tech.aistar.pay.alipay.entity.request.AlipayWebSiteRequest;
import tech.aistar.pay.alipay.entity.response.AlipayRefundResponse;
import tech.aistar.pay.config.AlipayConfig;

/**
 * Created by Administrator on 2019/10/8.
 */
@Controller
@RequestMapping("/alipay")
public class AlipayController {

    private static final Log log= LogFactory.get();

    @Autowired
    private AlipayConfig alipayConfig;

    @RequestMapping("/pay/web")
    public String payInWebSite(@RequestParam(name = "amt")String amt,
                               ModelMap modelMap,String orderId){
        try {
            AlipayCore.ClientBuilder clientBuilder = new AlipayCore.ClientBuilder();
            AlipayCore alipayCore = clientBuilder.setAlipayPublicKey(alipayConfig.getAlipay_public_key())
                    .setAppId(alipayConfig.getAppid())
                    .setPrivateKey(alipayConfig.getPrivate_key())
                    .setSignType(AlipayConstants.SIGN_TYPE_RSA2).build();

            AlipayWebSiteRequest request = new AlipayWebSiteRequest();

            //http://你的异步处理地址/alipay/notify_mobile
            //request.setNotifyUrl("http://你的异步处理地址/alipay/notify_mobile");

            request.setReturnUrl("http://localhost:8888/magic/createAdopt/completeView?orderId="+orderId);

            AlipayWebSiteRequest.BizContent bizContent = new AlipayWebSiteRequest.BizContent();
            bizContent.setTotalAmount(amt);
            bizContent.setSubject("测试电脑网站支付");
            bizContent.setBody("测试");
            bizContent.setOutTradeNo(System.currentTimeMillis()+"1000");
            request.setBizContent(bizContent);
            String html = AlipayClient.payInWebSite(request,alipayCore);
            modelMap.addAttribute("form", html);

        } catch (Exception e) {
            log.error("电脑网站支付失败",e);
        }

        return "adopt/alipay_web";
    }

    @RequestMapping(value = "/refund",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object refund(@RequestParam(name = "amt")String amt,
                         @RequestParam(name = "out_trade_no")String outTradeNo){

        try {
            AlipayCore.ClientBuilder clientBuilder = new AlipayCore.ClientBuilder();
            AlipayCore alipayCore = clientBuilder.setAlipayPublicKey(alipayConfig.getAlipay_public_key())
                    .setAppId(alipayConfig.getAppid())
                    .setPrivateKey(alipayConfig.getPrivate_key())
                    .setSignType(AlipayConstants.SIGN_TYPE_RSA2).build();

            AlipayRefundRequest request = new AlipayRefundRequest();
            request.setOutTradeNo(outTradeNo);
            request.setRefundAmount(amt);
            request.setRefundReason("测试退款");
            request.setOutRequestNo(System.currentTimeMillis() + "");
            AlipayRefundResponse response = AlipayClient.refund(request, alipayCore);
            return response;
        } catch (Exception e) {
            log.error("支付宝退款失败.",e);
            return e;
        }
    }
}
