package tech.aistar.controller.adopt;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import tech.aistar.entity.adopt.AdoptCat;
import tech.aistar.entity.adopt.Collect;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.entity.mycat.CatSpecies;
import tech.aistar.entity.mycat.CodeStatus;
import tech.aistar.entity.mycat.Result;
import tech.aistar.entity.personal.User;
import tech.aistar.service.adopt.IAdoptCatService;
import tech.aistar.service.adopt.ICollectService;
import tech.aistar.service.mycat.ICatService;
import tech.aistar.service.mycat.ICatSpeciesService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/25.
 */
@Controller
@RequestMapping("/adoptCat")
public class AdoptCatController {

    @Autowired
    private ICatSpeciesService catSpeciesService;

    @Autowired
    private IAdoptCatService adoptCatService;

    @Autowired
    private ICatService catService;

//    @Autowired
//    private IPageInfoService pageInfoService;
//
//    @Autowired
//    private ICatVoService catVoService;

    @Autowired
    private ICollectService collectService;

    //跳转到领养猫咪的显示页面
    @RequestMapping("/adoptCatView")
    public String adoptCatView() {

        return "adopt/adoptCat";
    }

    //展示可以领养的猫咪
    @RequestMapping("/list")
    public String list(Model model, Integer catSpecies_id, String catName, @RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "6") Integer pageSize, HttpServletRequest request) {
//    public String list(Model model, Integer catSpecies_id, Integer cat_id, HttpServletRequest request) {
        Integer tid = null;
        //判断
        if (catSpecies_id == null || -1 == catSpecies_id) {
            tid = null;
        } else {
            tid = Integer.valueOf(catSpecies_id);
            //数据的回显
        }

//        System.out.println(pageSize);

        PageInfo<AdoptCat> pageInfo = adoptCatService.find(tid, catName, pageNum, pageSize);

//        List<CatVo> catVos = catVoService.findBySpeciesId(tid);

//        System.out.println(pageInfo);

        model.addAttribute("pageInfo", pageInfo);

//        model.addAttribute("catVos",catVos);

        User user = (User) request.getSession().getAttribute("user");

        List<Collect> collects = null;

        if (user != null) {
            collects = collectService.findById(user.getId());
        }

        List<Integer> list = new ArrayList<>();

        if (collects != null) {
            for (Collect c : collects) {
                if (c.getStatus() != 1) {
                    list.add(c.getCatId());
                }
            }
        }

        model.addAttribute("list", list);

        List<CatSpecies> catSpeciesList = catSpeciesService.findAll();

        List<Cat> cats = catService.findAdoptCatAll();

        model.addAttribute("cats",cats);

        model.addAttribute("catSpeciesList", catSpeciesList);

        model.addAttribute("catSpecies_id", tid);

        model.addAttribute("catName", catName);

        return "adopt/pageData";
    }

    //收藏猫咪
    @RequestMapping("collect")
    public @ResponseBody
    Result collect(HttpServletRequest request) {

        User user = (User) request.getSession().getAttribute("user");

//        System.out.println(user);

        String cat_id = request.getParameter("cat_id");

//        System.out.println(cat_id);

        if (user == null) {
            return new Result(CodeStatus.ERROR, false, "请您先登录!");
        } else {

            List<Collect> collects = collectService.findAll();

            for (Collect c : collects) {

                if (c.getUserId() == user.getId() && c.getCatId() == Integer.valueOf(cat_id)) {

                    if (c.getStatus() == 0) {
                        c.setStatus(1);
                        collectService.update(c);
                        return new Result(CodeStatus.OK, true, "取消收藏成功!");

                    } else if (c.getStatus() == 1) {
                        c.setStatus(0);
                        c.setAddTime(new Date());
                        collectService.update(c);
                        return new Result(CodeStatus.OK, true, "收藏成功!");
                    }

                }
            }

            Collect collect = new Collect();
            collect.setUserId(user.getId());
            collect.setCatId(Integer.valueOf(cat_id));
            collect.setAddTime(new Date());

            collectService.save(collect);

            return new Result(CodeStatus.OK, true, "收藏成功!");

        }
    }

}
