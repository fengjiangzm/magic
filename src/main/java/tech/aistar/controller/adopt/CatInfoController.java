package tech.aistar.controller.adopt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.aistar.entity.adopt.AdoptCat;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.entity.mycat.CatSpecies;
import tech.aistar.service.adopt.IAdoptCatService;
import tech.aistar.service.mycat.ICatService;
import tech.aistar.service.mycat.ICatSpeciesService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 陆锦鹏 on 2019/9/27.
 */
@Controller
@RequestMapping("/catInfo")
public class CatInfoController {

    @Autowired
    private ICatService catService;

    @Autowired
    private IAdoptCatService adoptCatService;

    @Autowired
    private ICatSpeciesService catSpeciesService;

    //跳转到该猫咪的详情页面
    @RequestMapping("/view")
    public String view(Model model, HttpServletRequest request) {

        String cat_id = request.getParameter("cat_id");

        Cat cat = catService.findById(Integer.valueOf(cat_id));

        model.addAttribute("cat", cat);

        AdoptCat adoptCat = adoptCatService.findOneById(Integer.valueOf(cat_id));

        model.addAttribute("adoptCat", adoptCat);

        CatSpecies catSpecies = catSpeciesService.findById(cat.getSpeciesId());

        model.addAttribute("catSpecies", catSpecies);

        return "adopt/catInfo";
    }
}
