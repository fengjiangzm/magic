package tech.aistar.controller.adopt;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 陆锦鹏 on 2019/10/2.
 */
@Controller
@RequestMapping("/user")
public class LoginOutController {

    //注销用户
    @RequestMapping("/loginOut")
    public String LoginOut(HttpServletRequest request){
        request.getSession().invalidate();

        return "index";
    }
}
