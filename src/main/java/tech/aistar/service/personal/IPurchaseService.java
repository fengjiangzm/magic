package tech.aistar.service.personal;


import tech.aistar.entity.personal.Purchase;

import java.util.List;

/**
 * Created by LJX on 2019/9/23.
 */
public interface IPurchaseService {
    Purchase findById(Integer id);
    void update(Integer id);
    void updateOrdercancel(Integer id);
    void save(Purchase purchase);
    List<Purchase> findByuserId(Integer userId);
    Purchase findByOrderno(String ordno);

    List<Purchase> findAll();

    void updatePayment(Integer id);

}
