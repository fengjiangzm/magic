package tech.aistar.service.personal;

import tech.aistar.entity.personal.UserInfo;

import java.util.List;

/**
 * Created by LJX on 2019/9/26.
 */
public interface IUserInfoService {
    UserInfo findById(Integer id);
    List<UserInfo> findByName(String nickname);
    void update(Integer id);
    void save(UserInfo userInfo);
    void updateById(UserInfo userInfo);
    void uploadById(UserInfo userInfo);
}
