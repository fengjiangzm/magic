package tech.aistar.service.personal;

import tech.aistar.entity.personal.User;

/**
 * Created by LJX on 2019/9/26.
 */
public interface IUserService {
    User findByName(String username);
    void save(User user);
}
