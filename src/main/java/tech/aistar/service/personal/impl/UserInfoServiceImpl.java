package tech.aistar.service.personal.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.aistar.entity.personal.UserInfo;
import tech.aistar.mapper.personal.UserInfoMapper;
import tech.aistar.service.personal.IUserInfoService;

import java.util.List;

/**
 * Created by LJX on 2019/9/26.
 */
@Service
public class UserInfoServiceImpl implements IUserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Override
    public UserInfo findById(Integer id) {
        UserInfo userInfo = userInfoMapper.findById(id);
        return userInfo;
    }

    @Override
    public List<UserInfo> findByName(String nickname) {
        List<UserInfo> userInfos = userInfoMapper.findByName(nickname);
        return userInfos;
    }

    @Override
    public void update(Integer id) {
        userInfoMapper.update(id);

    }

    @Override
    public void save(UserInfo userInfo) {
        userInfoMapper.save(userInfo);
    }

    @Override
    public void updateById(UserInfo userInfo) {
        userInfoMapper.updateById(userInfo);

    }

    @Override
    public void uploadById(UserInfo userInfo) {
        userInfoMapper.uploadById(userInfo);
    }
}
