package tech.aistar.service.personal.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.aistar.entity.personal.Purchase;
import tech.aistar.mapper.personal.PurchaseMapper;
import tech.aistar.service.personal.IPurchaseService;

import java.util.List;

/**
 * Created by LJX on 2019/9/23.
 */
@Service
public class PurchaseServiceImpl implements IPurchaseService {
    @Autowired
    private PurchaseMapper PMapper;
//    @Autowired
//    private UserMapper userMapper;
    @Override
    public Purchase findById(Integer id) {
        Purchase o = PMapper.findById(id);
        return o;
    }

    @Override
    public void update(Integer id) {
        PMapper.update(id);
    }

    @Override
    public void updateOrdercancel(Integer id) {
        PMapper.updateOrdercancel(id);
    }

    @Override
    public void save(Purchase purchase) {

        PMapper.save(purchase);
    }

    @Override
    public List<Purchase> findByuserId(Integer userId) {
        List<Purchase> purchases = PMapper.findByuserId(userId);
        return purchases;
    }

    @Override
    public Purchase findByOrderno(String ordno) {
        Purchase p =PMapper.findByOrderno(ordno);
        return p;
    }

    @Override
    public List<Purchase> findAll() {
        return PMapper.findAll();
    }

    @Override
    public void updatePayment(Integer id) {
        PMapper.updatePayment(id);
    }
}
