package tech.aistar.service.personal.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.aistar.entity.personal.OrderComment;
import tech.aistar.mapper.personal.OrderCommentMapper;
import tech.aistar.service.personal.IOrderCommentService;

import java.util.List;

/**
 * Created by LJX on 2019/9/26.
 */
@Service
public class IOrderCommentServiceImpl implements IOrderCommentService {
    @Autowired
    private OrderCommentMapper commentMapper;
//    @Override
//    public List<OrderComment> findByOrderId(Integer orderId) {
//        List<OrderComment> comments = commentMapper.findByOrderId(orderId);
//        return  comments;
//    }

    @Override
    public OrderComment findByOrderId(Integer orderId) {
        OrderComment comment = commentMapper.findByOrderId(orderId);
        return comment;
    }

    @Override
    public void update(Integer id) {
        commentMapper.update(id);
    }

    @Override
    public OrderComment findById(Integer id) {
        OrderComment c = commentMapper.findById(id);
        return c;
    }

    @Override
    public List<OrderComment> findByUserId(Integer userId) {
        List<OrderComment> c = commentMapper.findByUserId(userId);
        return c;
    }

    @Override
    public void save(OrderComment comment) {
        commentMapper.save(comment);

    }
}
