package tech.aistar.service.personal.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.aistar.entity.personal.User;
import tech.aistar.mapper.personal.UserMapper;
import tech.aistar.service.personal.IUserService;

/**
 * Created by LJX on 2019/9/26.
 */
@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;
//
//    @Autowired
//    private RedisTemplate redisTemplate;


    @Override
    public User findByName(String username) {
        User u = userMapper.findByName(username);
        return u;
    }

    @Override
    public void save(User user) {
        userMapper.save(user);

    }
}
