package tech.aistar.service.personal;

import tech.aistar.entity.personal.OrderComment;

import java.util.List;

/**
 * Created by LJX on 2019/9/26.
 */
public interface IOrderCommentService {
//    List<OrderComment> findByOrderId(Integer orderId);
OrderComment findByOrderId(Integer orderId);
    void update(Integer id);
    OrderComment findById(Integer id);
   List<OrderComment>  findByUserId(Integer userId);
    void save(OrderComment comment);
}
