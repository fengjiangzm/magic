package tech.aistar.service.foster;

import tech.aistar.entity.foster.Room;

import java.util.List;

public interface IRoomService {

    List<Room> findAll();

    void fosterAdd(Integer id);

    void fosterLeave(Integer id);
}
