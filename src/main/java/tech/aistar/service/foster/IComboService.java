package tech.aistar.service.foster;

import tech.aistar.entity.foster.Combo;

import java.util.List;

public interface IComboService {
    void insert(Combo combo);

    void add(Integer fosterId, Integer eventId);

    List<Combo> findByEventId(Integer id);

    List<Combo> findByFosterId(Integer id);
}
