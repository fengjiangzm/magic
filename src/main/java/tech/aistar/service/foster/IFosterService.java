package tech.aistar.service.foster;

import com.github.pagehelper.PageInfo;
import tech.aistar.entity.foster.Foster;

import java.util.List;

public interface IFosterService {

    void insert(Foster foster);

    Foster findById(Integer id);

    void updateById(Foster foster);

    void deleteById(Integer id);

    List<Foster> findAll();

    Foster findByOrderId(Integer orderId);

    void startFoster(Integer id);

    void endFoster(Integer id);

    PageInfo<Foster> findAllPage(Integer pageNum, Integer pageSize);
}
