package tech.aistar.service.foster;

import org.springframework.data.domain.Page;
import tech.aistar.entity.foster.Article;

import java.util.List;

public interface IArticleService {

    void add(Article article);

    Article findById(String id);

    List<Article> findAll();

    Page<Article> findByType(String type, Integer pageNum, Integer pageSize);

    Page<Article> findByTitle(String title, Integer pageNum, Integer pageSize);

    Page<Article> findA(Integer pageNum, Integer pageSize);
    void save(Article article);

    void addView(Article article);
}
