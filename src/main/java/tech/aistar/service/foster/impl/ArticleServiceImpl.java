package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import tech.aistar.entity.foster.Article;
import tech.aistar.mapper.foster.ArticleMapper;
import tech.aistar.service.foster.IArticleService;
import tech.aistar.utill.MarkDownUtil;

import java.util.List;
import java.util.Optional;

@Service
public class ArticleServiceImpl implements IArticleService {

    @Autowired
    private ArticleMapper articleMapper;


    @Override
    public void add(Article article) {
        article.setId(String.valueOf(articleMapper.count() + 1));
        article.setDesc(MarkDownUtil.getChinese(article.getContent()));
        article.setStar(0);
        article.setViewNumber(0);
        articleMapper.save(article);
    }

    @Override
    public void save(Article article) {
        articleMapper.save(article);
    }

    @Override
    public void addView(Article article) {
        article.setViewNumber(article.getViewNumber() + 1);
        articleMapper.save(article);
    }

    @Override
    public Article findById(String id) {
        Optional<Article> article = articleMapper.findById(id);
        if (article.isPresent()) {
            Article a = article.get();
            return a;
        }
        return null;
    }

    @Override
    public List<Article> findAll() {
        return articleMapper.findAll();
    }

    @Override
    public Page<Article> findByType(String type, Integer pageNum, Integer pageSize) {
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        Page<Article> articles = articleMapper.findByTypeLike(type, pageRequest);
        return articles;
    }

    @Override
    public Page<Article> findByTitle(String title, Integer pageNum, Integer pageSize) {
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        Page<Article> articles = articleMapper.findByTitleLike(title, pageRequest);
        return articles;
    }


    @Override
    public Page<Article> findA(Integer pageNum, Integer pageSize) {
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        Page<Article> articles = articleMapper.findAll(pageRequest);
        return articles;
    }


}
