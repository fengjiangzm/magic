package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.foster.Room;
import tech.aistar.mapper.foster.RoomMapper;
import tech.aistar.service.foster.IRoomService;

import java.util.List;

@Service
@Transactional
public class RoomServiceImpl implements IRoomService {

    @Autowired
    private RoomMapper roomMapper;

    @Override
    public List<Room> findAll() {
        return roomMapper.findAll();
    }

    @Override
    public void fosterAdd(Integer id) {
        roomMapper.fosterAdd(id);
    }

    @Override
    public void fosterLeave(Integer id) {
        roomMapper.fosterLeave(id);
    }

}
