package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.foster.Combo;
import tech.aistar.mapper.foster.ComboMapper;
import tech.aistar.service.foster.IComboService;

import java.util.List;

@Service
@Transactional
public class ComboServiceImpl implements IComboService {

    @Autowired
    private ComboMapper comboMapper;

    @Override
    public void insert(Combo combo) {
        comboMapper.addCombo(combo);
    }

    @Override
    public void add(Integer fosterId, Integer eventId) {
        Combo combo = new Combo(null,fosterId,eventId);
        comboMapper.addCombo(combo);
    }

    @Override
    public List<Combo> findByEventId(Integer id) {
        return comboMapper.findByEventId(id);
    }

    @Override
    public List<Combo> findByFosterId(Integer id) {
        return comboMapper.findByFosterId(id);
    }
}
