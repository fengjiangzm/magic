package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.foster.CatActivity;
import tech.aistar.mapper.foster.CatActivityMapper;
import tech.aistar.service.foster.ICatActivityService;

import java.util.List;

@Service
@Transactional
public class CatActivityServiceImpl implements ICatActivityService {

    @Autowired
    private CatActivityMapper catActivityMapper;

    @Override
    public void add(CatActivity catActivity) {
        catActivityMapper.add(catActivity);
    }

    @Override
    public List<CatActivity> findByCatId(Integer id) {
        return catActivityMapper.findByCatId(id);
    }

    @Override
    public void update(CatActivity catActivity) {
        catActivityMapper.update(catActivity);
    }

    @Override
    public void deleteById(Integer id) {
        catActivityMapper.deleteById(id);
    }
}
