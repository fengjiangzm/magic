package tech.aistar.service.foster.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.foster.Foster;
import tech.aistar.mapper.foster.FosterMapper;
import tech.aistar.service.foster.IFosterService;

import java.util.List;

@Service
@Transactional
public class FosterServiceImpl implements IFosterService {

    @Autowired
    private FosterMapper fosterMapper;

    @Override
    @Transactional
    public void insert(Foster foster) {
        fosterMapper.insert(foster);
    }

    @Override
    public Foster findById(Integer id) {
        return fosterMapper.findById(id);
    }

    @Override
    public void updateById(Foster foster) {
        fosterMapper.updateById(foster);
    }

    @Override
    public void deleteById(Integer id) {
        fosterMapper.deleteById(id);
    }

    @Override
    public List<Foster> findAll() {
        return fosterMapper.findAll();
    }

    @Override
    public Foster findByOrderId(Integer orderId) {
        return fosterMapper.findByOrderId(orderId);
    }

    @Override
    public void startFoster(Integer id) {
        fosterMapper.startFoster(id);
    }

    @Override
    public void endFoster(Integer id) {
        fosterMapper.endFoster(id);
    }

    @Override
    public PageInfo<Foster> findAllPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);

        List<Foster> fosters = fosterMapper.findAll();
//        for (Foster foster : fosters) {
//            System.out.println(foster);
//        }
        PageInfo<Foster> pageInfo = new PageInfo<>(fosters);


        return pageInfo;
    }
}
