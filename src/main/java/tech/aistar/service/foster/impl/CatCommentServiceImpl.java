package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.aistar.entity.foster.CatComment;
import tech.aistar.mapper.foster.CatCommentRepository;
import tech.aistar.service.foster.ICatCommentService;

import java.util.List;
import java.util.Optional;

@Service
public class CatCommentServiceImpl implements ICatCommentService {

    @Autowired
    private CatCommentRepository catCommentRepository;

    @Override
    public List<CatComment> findAll() {
        return catCommentRepository.findAll();
    }

    @Override
    public void addCatComment(CatComment catComment) {
        catComment.setId(String.valueOf(catCommentRepository.count()+1));
        catCommentRepository.save(catComment);
    }

    @Override
    public CatComment findById(String id) {
        Optional<CatComment> optional =   catCommentRepository.findById(id);
        if (optional.isPresent()){
            CatComment c = optional.get();
            return c;
        }
        return null;

    }

    @Override
    public List<CatComment> findByOwnerId(String id) {
        List<CatComment> catComment =   catCommentRepository.findByOwnerId(id);

        return catComment;
    }

    @Override
    public List<CatComment> findByFromId(String id) {
        return catCommentRepository.findByFromId(id);
    }

    @Override
    public void updateComment(CatComment catComment) {
        catCommentRepository.save(catComment);
    }
}
