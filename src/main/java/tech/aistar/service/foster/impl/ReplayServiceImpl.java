package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.aistar.entity.foster.Replay;
import tech.aistar.mapper.foster.ReplayRepository;
import tech.aistar.service.foster.IReplayService;

import java.util.List;
import java.util.Optional;

@Service
public class ReplayServiceImpl implements IReplayService {

    @Autowired
    private ReplayRepository replayRepository;
    @Override
    public void save(Replay replay) {
        replay.setId(String.valueOf(replayRepository.count()+1));
        replayRepository.save(replay);
    }

    @Override
    public List<Replay> findByToId(String toId) {
        List<Replay> replays = replayRepository.findByToId(toId);
        return replays;
    }

    @Override
    public Replay findByCommentId(String commentId) {
        return replayRepository.findByCommentId(commentId);
    }

    @Override
    public Replay findById(String id) {
        Optional<Replay> article = replayRepository.findById(id);
        if (article.isPresent()){
            Replay a = article.get();
            return a;
        }
        return null;
    }
}
