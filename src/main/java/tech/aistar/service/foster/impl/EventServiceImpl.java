package tech.aistar.service.foster.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.foster.Event;
import tech.aistar.mapper.foster.EventMapper;
import tech.aistar.service.foster.IEventService;

import java.util.List;

@Service
@Transactional
public class EventServiceImpl implements IEventService {

    @Autowired
    private EventMapper eventMapper;

    @Override
    public List<Event> findAll() {
        return eventMapper.findAll();
    }
}
