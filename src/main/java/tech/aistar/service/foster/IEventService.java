package tech.aistar.service.foster;

import tech.aistar.entity.foster.Event;

import java.util.List;

public interface IEventService {

    List<Event> findAll();


}
