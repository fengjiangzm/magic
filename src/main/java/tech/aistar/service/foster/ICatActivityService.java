package tech.aistar.service.foster;

import tech.aistar.entity.foster.CatActivity;

import java.util.List;

public interface ICatActivityService {

    void add(CatActivity catActivity);

    List<CatActivity> findByCatId(Integer id);

    void update(CatActivity catActivity);

    void deleteById(Integer id);
}
