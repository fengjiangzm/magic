package tech.aistar.service.foster;

import tech.aistar.entity.foster.CatComment;

import java.util.List;

public interface ICatCommentService {

    List<CatComment> findAll();

    void addCatComment(CatComment catComment);

    CatComment findById(String id);

    List<CatComment> findByOwnerId(String id);

    List<CatComment> findByFromId(String id);

    void updateComment(CatComment catComment);

}
