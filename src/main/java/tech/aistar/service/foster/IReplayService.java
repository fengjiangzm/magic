package tech.aistar.service.foster;

import tech.aistar.entity.foster.Replay;

import java.util.List;

public interface IReplayService {

    void save(Replay replay);

    List<Replay> findByToId(String toId);

    Replay findByCommentId(String commentId);

    Replay findById(String id);
}
