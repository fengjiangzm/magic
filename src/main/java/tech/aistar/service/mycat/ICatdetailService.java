package tech.aistar.service.mycat;

import org.apache.ibatis.annotations.Select;
import tech.aistar.entity.mycat.CatDetail;

/**
 * Created by Administrator on 2019/10/9.
 */
public interface ICatdetailService {
    CatDetail findAll();

    CatDetail findById(Integer id);

    CatDetail findByUserId(Integer id);

    void save(CatDetail catDetail);
}
