package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.CatDynamics;
import tech.aistar.mapper.mycat.CatDynamicsMapper;
import tech.aistar.service.mycat.CatDynamicsService;

import java.util.List;

/**
 * Created by Administrator on 2019/10/8.
 */
@Service
@Transactional
public class CatDynamicsServiceImpl implements CatDynamicsService {
    @Autowired
    private CatDynamicsMapper mapper;

    @Override
    public List<CatDynamics> findAll() {
        return mapper.findAll();
    }

    @Override
    public void save(CatDynamics catDynamics) {
        mapper.save(catDynamics);
    }

    @Override
    public List<CatDynamics> findById(Integer id) {
        return mapper.findById(id);
    }
}
