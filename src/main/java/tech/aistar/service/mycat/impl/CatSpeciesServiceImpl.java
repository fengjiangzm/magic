package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.CatSpecies;
import tech.aistar.mapper.mycat.CatSpeciesMapper;
import tech.aistar.service.mycat.ICatSpeciesService;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24.
 */
@Service
@Transactional
public class CatSpeciesServiceImpl implements ICatSpeciesService {

    @Autowired
    private CatSpeciesMapper mapper;
    @Override
    public List<CatSpecies> findAll() {
        return mapper.findAll();
    }

    @Override
    public CatSpecies findById(Integer id) {
        return mapper.findById(id);
    }


}
