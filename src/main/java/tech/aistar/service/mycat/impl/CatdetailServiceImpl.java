package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.CatDetail;
import tech.aistar.mapper.mycat.CatDetailMapper;
import tech.aistar.service.mycat.ICatdetailService;

/**
 * Created by Administrator on 2019/10/9.
 */
@Service
@Transactional
public class CatdetailServiceImpl implements ICatdetailService{
    @Autowired
    private CatDetailMapper mapper;
    @Override
    public CatDetail findAll() {
        return mapper.findAll();
    }

    @Override
    public CatDetail findById(Integer id) {
        return mapper.findById(id);
    }

    @Override
    public CatDetail findByUserId(Integer id) {
        return mapper.findByUserId(id);
    }

    @Override
    public void save(CatDetail catDetail) {
        mapper.save(catDetail);
    }
}
