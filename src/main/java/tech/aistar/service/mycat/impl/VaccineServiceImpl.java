package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.Vaccine;
import tech.aistar.mapper.mycat.VaccineMapper;
import tech.aistar.service.mycat.IVaccineService;

/**
 * Created by Administrator on 2019/9/24.
 */
@Service
@Transactional
public class VaccineServiceImpl implements IVaccineService {
    @Autowired
    private VaccineMapper mapper;
    @Override
    public Vaccine findById(Integer id) {
      return   mapper.findById(id);
    }

    @Override
    public void save(Vaccine vaccine) {
         mapper.save(vaccine);
    }
}
