package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.CatGrowth;
import tech.aistar.mapper.mycat.CatGrowthMapper;
import tech.aistar.service.mycat.ICatGrowthService;

/**
 * Created by Administrator on 2019/9/24.
 */
@Service
@Transactional
public class CatGrowthServiceImpl implements ICatGrowthService {
    @Autowired
    private CatGrowthMapper mapper;
    @Override
    public CatGrowth findById(Integer id) {
      return mapper.findById(id);
    }
}
