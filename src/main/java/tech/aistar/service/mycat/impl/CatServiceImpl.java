package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.Cat;
import tech.aistar.mapper.mycat.CatMapper;
import tech.aistar.service.mycat.ICatService;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24.
 */
@Service
@Transactional
public class CatServiceImpl implements ICatService {
    @Autowired
    private CatMapper mapper;

    @Override
    public List<Cat> findByUserId(Integer userId) {
        return mapper.findByUserId(userId);
    }

    @Override
    public Cat findById(Integer id) {
        return mapper.findById(id);
    }

    @Override
    public List<Cat> findAll(String name) {
        return mapper.findAll(name);
    }

    @Override
    public List<Cat> findAdoptCatAll() {

        List<Cat> cats = mapper.findAdoptCatAll();

        return cats;
    }

    @Override
    public void delById(Integer id) {
        mapper.delById(id);
    }

    @Override
    public void save(Cat cat) {
        mapper.save(cat);
    }

    @Override
    public void updata(Cat cat) {
        mapper.updata(cat);
    }
}
