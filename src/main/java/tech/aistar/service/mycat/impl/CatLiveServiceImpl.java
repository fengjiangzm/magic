package tech.aistar.service.mycat.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.mycat.CatLiveRequest;
import tech.aistar.mapper.mycat.CatLiveMapper;
import tech.aistar.service.mycat.ICatLiveService;

/**
 * Created by Administrator on 2019/9/24.
 */
@Service
@Transactional
public class CatLiveServiceImpl implements ICatLiveService{
    @Autowired
    private CatLiveMapper mapper;
    @Override
    public CatLiveRequest findById(Integer id) {
      return mapper.findById(id);
    }
}
