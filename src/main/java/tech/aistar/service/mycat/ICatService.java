package tech.aistar.service.mycat;

import tech.aistar.entity.mycat.Cat;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface ICatService {

    List<Cat> findByUserId(Integer userId);

    //根据猫咪id查找猫咪
    Cat findById(Integer id);

    List<Cat> findAll(String name);

    List<Cat> findAdoptCatAll();

    void delById(Integer id);

    //保存猫咪
    void save(Cat cat);

    //修改
    void updata(Cat cat);
}
