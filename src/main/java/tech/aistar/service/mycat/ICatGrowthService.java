package tech.aistar.service.mycat;

import tech.aistar.entity.mycat.CatGrowth;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface ICatGrowthService {

    CatGrowth findById(Integer id);
}
