package tech.aistar.service.mycat;

import tech.aistar.entity.mycat.CatSpecies;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface ICatSpeciesService {
    List<CatSpecies> findAll();

    CatSpecies findById(Integer id);
}
