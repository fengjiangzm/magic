package tech.aistar.service.mycat;

import tech.aistar.entity.mycat.CatLiveRequest;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface ICatLiveService {
    CatLiveRequest findById(Integer id);
}
