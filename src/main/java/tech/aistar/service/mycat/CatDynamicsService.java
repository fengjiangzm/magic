package tech.aistar.service.mycat;


import tech.aistar.entity.mycat.CatDynamics;

import java.util.List;

/**
 * Created by Administrator on 2019/10/8.
 */
public interface CatDynamicsService {

    List<CatDynamics> findAll();


    void save(CatDynamics catDynamics);

    List<CatDynamics> findById(Integer id);
}
