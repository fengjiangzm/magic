package tech.aistar.service.mycat;

import tech.aistar.entity.mycat.Vaccine;

/**
 * Created by Administrator on 2019/9/24.
 */
public interface IVaccineService {

    Vaccine findById(Integer id);

    void save(Vaccine vaccine);
}
