package tech.aistar.service.adopt;

import com.github.pagehelper.PageInfo;
import tech.aistar.entity.adopt.CatVo;

/**
 * Created by 陆锦鹏 on 2019/9/27.
 */
public interface IPageInfoService {
    PageInfo<CatVo> find(Integer speciesId, Integer pageNum, Integer pageSize);
}
