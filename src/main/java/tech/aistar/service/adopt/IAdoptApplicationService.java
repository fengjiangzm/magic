package tech.aistar.service.adopt;

import tech.aistar.entity.adopt.AdoptApplication;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
public interface IAdoptApplicationService {
    List<AdoptApplication> findAll();

    AdoptApplication findOneById(Integer id);

    void save(AdoptApplication adoptApplication);

    void update(AdoptApplication adoptApplication);
}
