package tech.aistar.service.adopt;

import tech.aistar.entity.adopt.Collect;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
public interface ICollectService {

    List<Collect> findAll();

    List<Collect> findById(Integer id);

    void delById(Integer cat_id, Integer user_id);

    void save(Collect adoptCat);

    void update(Collect adoptCat);
}
