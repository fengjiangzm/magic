package tech.aistar.service.adopt.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.adopt.Collect;
import tech.aistar.mapper.adopt.CollectMapper;
import tech.aistar.service.adopt.ICollectService;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
@Service
@Transactional
public class CollectServiceImpl implements ICollectService {

    @Autowired
    private CollectMapper collectMapper;


    @Override
    public List<Collect> findAll() {

        List<Collect> collects = collectMapper.findAll();

        return collects;
    }

    @Override
    public List<Collect> findById(Integer id) {

        List<Collect> collects = collectMapper.findById(id);

        return collects;
    }

    @Override
    public void delById(Integer cat_id,Integer user_id) {
        collectMapper.delById(cat_id,user_id);
    }

    @Override
    public void save(Collect collect) {
        collectMapper.save(collect);
    }

    @Override
    public void update(Collect collect) {
        collectMapper.update(collect);
    }
}
