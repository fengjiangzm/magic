package tech.aistar.service.adopt.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.adopt.CatVo;
import tech.aistar.service.adopt.ICatVoService;
import tech.aistar.service.adopt.IPageInfoService;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/27.
 */
@Service
@Transactional
public class PageInfoServiceImpl implements IPageInfoService {

    @Autowired
    private ICatVoService catVoService;

    @Override
    public PageInfo<CatVo> find(Integer speciesId, Integer pageNum, Integer pageSize) {

        PageHelper.startPage(pageNum,pageSize);

        List<CatVo> catVos = catVoService.findBySpeciesId(speciesId);

        PageInfo<CatVo> pageInfo = new PageInfo<>(catVos);

        return pageInfo;
    }
}
