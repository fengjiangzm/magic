package tech.aistar.service.adopt.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.adopt.AdoptApplication;
import tech.aistar.mapper.adopt.AdoptApplicationMapper;
import tech.aistar.service.adopt.IAdoptApplicationService;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
@Service
@Transactional
public class AdoptApplicationServiceImpl implements IAdoptApplicationService{

    @Autowired
    private AdoptApplicationMapper adoptApplicationMapper;

    @Override
    public List<AdoptApplication> findAll() {

        List<AdoptApplication> adoptApplications = adoptApplicationMapper.findAll();

        return adoptApplications;
    }

    @Override
    public AdoptApplication findOneById(Integer id) {

        AdoptApplication adoptApplication = adoptApplicationMapper.findOneById(id);

        return adoptApplication;
    }

    @Override
    public void save(AdoptApplication adoptApplication) {
        adoptApplicationMapper.save(adoptApplication);
    }

    @Override
    public void update(AdoptApplication adoptApplication) {
        adoptApplicationMapper.update(adoptApplication);
    }
}
