package tech.aistar.service.adopt.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.adopt.AdoptCat;
import tech.aistar.entity.adopt.CatVo;
import tech.aistar.service.adopt.IAdoptCatService;
import tech.aistar.service.adopt.ICatVoService;
import tech.aistar.service.mycat.ICatService;
import tech.aistar.service.mycat.ICatSpeciesService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/26.
 */
@Service
@Transactional
@SuppressWarnings("all")
public class CatVoServiceImpl implements ICatVoService {

    @Autowired
    private ICatService catService;

    @Autowired
    private IAdoptCatService adoptCatService;

    @Autowired
    private ICatSpeciesService catSpeciesService;

    @Override
    public List<CatVo> findBySpeciesId(Integer speciesId) {

        List<AdoptCat> adoptCats = adoptCatService.findAll();

        List<CatVo> catVos = new ArrayList<>();

        if (null == speciesId) {
            for (AdoptCat ac : adoptCats) {

                if (ac.getStatus() == 1){
                    continue;
                }

                CatVo catVo = new CatVo();
                catVo.setCatId(ac.getCatId());
                catVo.setCatPrice(ac.getCatPrice());
                catVo.setImgUrl(null);
                catVo.setIntroduction(ac.getIntroduction());
                Integer Id = catService.findById(ac.getCatId()).getSpeciesId();
                catVo.setSpeciesName(catSpeciesService.findById(Id).getSpeciesName());

                catVos.add(catVo);
            }
        }else{
            for (AdoptCat ac : adoptCats) {

                if (ac.getStatus() == 1){
                    continue;
                }

                if (speciesId.equals(catService.findById(ac.getCatId()).getSpeciesId())) {
                    CatVo catVo = new CatVo();

                    String name = catSpeciesService.findById(speciesId).getSpeciesName();

                    catVo.setCatId(ac.getCatId());
                    catVo.setSpeciesName(name);
                    catVo.setCatPrice(ac.getCatPrice());
                    catVo.setImgUrl(null);
                    catVo.setIntroduction(ac.getIntroduction());

                    catVos.add(catVo);
                } else{
                    continue;
                }
            }
        }

        return catVos;
    }


}
