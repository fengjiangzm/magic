package tech.aistar.service.adopt.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.aistar.entity.adopt.UserAddress;
import tech.aistar.mapper.adopt.UserAddressMapper;
import tech.aistar.service.adopt.IUserAddressService;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/25.
 */
@Service
@Transactional
public class UserAddressServiceImpl implements IUserAddressService {

    @Autowired
    private UserAddressMapper userAddressMapper;

    @Override
    public List<UserAddress> findAll() {

        List<UserAddress> userAddresses = userAddressMapper.findAll();

        return userAddresses;
    }

    @Override
    public List<UserAddress> findById(Integer id) {

        List<UserAddress> userAddress = userAddressMapper.findById(id);

        return userAddress;
    }

    @Override
    public UserAddress findOneById(Integer id) {

        UserAddress userAddress = userAddressMapper.findOneById(id);

        return userAddress;
    }

    @Override
    public void delById(Integer id) {

        userAddressMapper.delById(id);
    }

    @Override
    public void save(UserAddress userAddress) {

        userAddressMapper.save(userAddress);
    }

    @Override
    public void update(UserAddress userAddress) {
        userAddressMapper.update(userAddress);
    }

    @Override
    public void setDefault(Integer id) {
        userAddressMapper.setDefault(id);
    }

    @Override
    public UserAddress findId(Integer id) {
        UserAddress userAddress = userAddressMapper.findId(id);
        return userAddress;
    }

    @Override
    public void delId(Integer id) {
        userAddressMapper.delId(id);
    }

    @Override
    public void setDefault1(Integer id) {
        userAddressMapper.setDefault1(id);
    }
}
