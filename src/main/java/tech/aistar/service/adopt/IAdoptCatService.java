package tech.aistar.service.adopt;

import com.github.pagehelper.PageInfo;
import tech.aistar.entity.adopt.AdoptCat;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/24.
 */
public interface IAdoptCatService {

    List<AdoptCat> findAll();

    AdoptCat findOneById(Integer id);

    void delById(Integer id);

    void save(AdoptCat adoptCat);

    void update(AdoptCat adoptCat);

    void updateStatus(Integer id);

    PageInfo<AdoptCat> find(Integer speciesId, String catName, Integer pageNum, Integer pageSize);
}
