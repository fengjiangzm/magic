package tech.aistar.service.adopt;

import tech.aistar.entity.adopt.UserAddress;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/25.
 */
public interface IUserAddressService {
    List<UserAddress> findAll();

    List<UserAddress> findById(Integer id);

    UserAddress findOneById(Integer id);

    void delById(Integer id);

    void save(UserAddress userAddress);

    void update(UserAddress userAddress);

    void setDefault(Integer id);

    UserAddress findId(Integer id);

    void delId(Integer id);

    void setDefault1(Integer id);
}
