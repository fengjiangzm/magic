package tech.aistar.service.adopt;

import tech.aistar.entity.adopt.CatVo;

import java.util.List;

/**
 * Created by 陆锦鹏 on 2019/9/26.
 */
public interface ICatVoService {

    List<CatVo> findBySpeciesId(Integer speciesId);
}
