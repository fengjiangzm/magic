/**
 * Created by Administrator on 2019/9/6 0006.
 */

var path = $("#path").val();

// 类型的改变的事件
function changeType(obj) {//obj -> select对象
    var catSpeciesId = obj.value;//选中的option的value

    var catName = document.getElementById("searchCatName").value;
    //发送到后台
    $("#content").load(path + "/adoptCat/list", {catName: catName, catSpecies_id: catSpeciesId});
}

function searchCat() {
    var catName = document.getElementById("searchCatName").value;

    var catSpeciesId = document.getElementById("catSpeciesType").value;

    $("#content").load(path + "/adoptCat/list", {catName: catName, catSpecies_id: catSpeciesId});
}

function collect(cat_id, obj) {
    $.get(path + '/adoptCat/collect', {cat_id: cat_id}, function (data) {
        if (data.flag == false) {
            alert(data.message);
            window.location.href = path + "/user/loginView";
        } else {
            alert(data.message);
            if (data.message == "收藏成功!") {
                $(obj).html("已收藏");
            } else {
                $(obj).html("点击收藏");
            }
        }
    });
}


