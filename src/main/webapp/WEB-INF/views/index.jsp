<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/9/20
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>膜蛤猫舍</title>

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">

    <style>
        a{
            text-decoration: none;
        }

        a:hover{
            text-decoration: none;
        }

        a:visited{
            text-decoration: none;
        }

        a:link{
            text-decoration: none;
        }

        .imgs_1 {
            background: url(<%=request.getContextPath()%>/imgs/adopt_img1.jpg) no-repeat fixed;
        }

        .imgs_2 {
            background: url(<%=request.getContextPath()%>/imgs/adopt_img2.jpg) no-repeat fixed;
        }

        .imgs_3 {
            background: url(<%=request.getContextPath()%>/imgs/adopt_img3.jpg) no-repeat fixed;
        }

        .imgs_4 {
            background: url(<%=request.getContextPath()%>/imgs/adopt_img4.jpg) no-repeat fixed;
        }

        .imgs_1,
        .imgs_2,
        .imgs_3,
        .imgs_4 {
            background-size: cover;
            min-height: 795px;
        }
    </style>

</head>
<body >

<!-- 轮播图片 -->
<div class="container-fluid">
    <div class="row">
        <div id="myCarousel" class="carousel slide">
            <!-- 轮播（Carousel）指标 -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <!-- 轮播（Carousel）项目 -->
            <div class="carousel-inner">
                <div class="item active imgs_1">
                    <div class="w3l-overlay">
                        <div class="container">
                            <div class="banner-text-info">
                                <h3>我们提供您的宠物应得的照顾！</h3>
                                <p>全面的猫护理指南，让您的宠物感受到您的爱</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item imgs_2">
                    <div class="w3l-overlay">
                        <div class="container">
                            <div class="banner-text-info">
                                <h3>我们提供您的宠物应得的照顾！</h3>
                                <p>全面的猫护理指南，让您的宠物感受到您的爱</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item imgs_3">
                    <div class="w3l-overlay">
                        <div class="container">
                            <div class="banner-text-info">
                                <h3>我们提供您的宠物应得的照顾！</h3>
                                <p>全面的猫护理指南，让您的宠物感受到您的爱</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item imgs_4">
                    <div class="w3l-overlay">
                        <div class="container">
                            <div class="banner-text-info">
                                <h3>我们提供您的宠物应得的照顾！</h3>
                                <p>全面的猫护理指南，让您的宠物感受到您的爱</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 轮播（Carousel）导航 -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<!-- 导航栏 -->
<jsp:include page="/WEB-INF/views/common/nav.jsp"></jsp:include>


<!-- 欢迎 -->
<div class="container">
    <h3 class="welcome1">欢迎光临本站</h3>
    <div class="row">
        <div class="col-md-6">
            <img src="<%=request.getContextPath()%>/imgs/adopt_img5.jpg" alt="" style="width: 570px;height: 450px;">
        </div>
        <div class="col-md-6">
            <div class="about-wel">
                <h1>
                    一些关于猫的话
                </h1>
                <p>猫能在高墙上若无其事地散步，轻盈跳跃，不禁折服于它的平衡感。这主要得益于猫的出类拔萃的反应神经和平衡感。
                    它只需轻微地改变尾巴的位置和高度就可取得身体的平衡，再利用后脚强健的肌肉和结实的关节就可敏捷地跳跃，
                    即使在高空中落下也可在空中改变身体姿势，轻盈准确地落地。善于爬高，但却不善于从顶点下落。
                    即使从高处掉下或者跳下来的时候，猫靠尾巴调整平衡，使带软垫的四肢着地。
                    注意不要拽断猫的尾巴，会影响它的平衡能力，也会容易使猫腹泻，减短猫的寿命。</p>
                <%--<ul class="welcome_ul">--%>
                    <%--<li>--%>
                        <%--<i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Cat health and Care</li>--%>
                    <%--<li>--%>
                        <%--<i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Cat grooming</li>--%>
                    <%--<li>--%>
                        <%--<i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Food for cats</li>--%>
                    <%--<li>--%>
                        <%--<i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Cat behavior</li>--%>
                <%--</ul>--%>
            </div>
<%--            <div class="btn button-styles">--%>
<%--                <a href="${path}/mycat/catstrategy">养猫知识</a>--%>
<%--            </div>--%>
            <div class="btn button-styles" style="margin-top:110px;">
                <a href="${path}/mycat/catwiki" >疾病百科</a>
            </div>
            <div class="btn button-styles" style="margin-top:110px;">
                <a href="${path}/article/allArticleView" >优秀文章</a>
            </div>
            <div class="btn button-styles" style="margin-top:110px;">
                <a href="${path}/mycat/catshow" >有趣动态</a>
            </div>
        </div>
    </div>
</div>

<!-- 相关服务介绍 -->
<div class="container" style="margin-top:20px;">
    <div>
        <h3 class="welcome1">服务介绍</h3>
    </div>
    <div class="row" style="text-align: center;">
        <div class="col-md-4">
            <div class="ih-item">
                <div></div>
                <div class="imgs">
                    <%--uploadView:36 string--%>
                    <img src="http://pzyjho639.bkt.clouddn.com/14ea0276-4bb1-481e-ae5f-71941ba9a4fc.jpg" alt="" class="img-responsive" style="height:200px;width:300px;">
                </div>
            </div>
            <fieldset>
                <legend>寄养服务</legend>
                在你没有时间的时候，无法照顾宠物的时候，你可以选择我们的寄养服务，专业服务，保障您的爱猫日常生活和健康。
            </fieldset>
        </div>
        <div class="col-md-4 ">
            <div class="ih-item">
                <div></div>
                <div class="imgs">
                    <%--http://py5ztkhmu.bkt.clouddn.com/6e180846-e648-4f45-acf6-18a51cb23393.jpg--%>
                    <img src="http://pzyjho639.bkt.clouddn.com/2379fc15-f797-4947-b3b2-de37cff1bb8e.jpg" alt="" class="img-responsive" style="height:200px;width:300px;">
                </div>
            </div>

            <fieldset>
                <legend>领养服务</legend>
                想拥有猫咪，但是又没有正规渠道？这项服务提供给你一个好选择，本店猫咪健康活泼，给你放心体验,还为您讲解如何喂养。
            </fieldset>
        </div>
        <div class="col-md-4">
            <div class="ih-item">
                <div></div>
                <div class="imgs">
                    <img src="http://pzyjho639.bkt.clouddn.com/605f9231-1b42-4e4f-9c4b-552d2325b75b.jpg" alt="" class="img-responsive" style="height:200px;width:300px;">
                </div>
            </div>
            <fieldset>
                <legend>猫咪相关服务</legend>
                提供给您的爱猫护理，体检，治疗等相关服务。全程严格把控，不让猫咪受到一点伤害，绝对放心的服务。
            </fieldset>
        </div>
    </div>
</div>

<!-- 美图展示 -->
<div style="margin-top: 30px;">
    <div class="container">
        <h3 class="welcome1">美图展示</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m1.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m2.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m3.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m4.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m5.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m6.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m7.jpg" alt="">
                </div>
            </div>
            <div class="col-md-3 .w3gallery-grids">
                <div class="thumbnail">
                    <img src="<%=request.getContextPath()%>/imgs/adopt_m8.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


<jsp:include page="common/bottom.jsp"></jsp:include>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

<script>
    layui.use('element', function () {
        var element = layui.element;

    });
</script>
</body>
</html>