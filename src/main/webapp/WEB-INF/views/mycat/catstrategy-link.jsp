<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/27
  Time: 14:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>养猫知识链接</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/mycat/catstrategy-link.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">

</head>

<body>
<jsp:include page="../common/nav.jsp"></jsp:include>


<!-- 导航信息 -->
<div class="m">
    <div class="nav">
        <a href="">首页</a>
        <i>&gt;</i>
        <a href="">养猫指南</a>
        <i>&gt;</i>
        <a href="">养猫知识</a>
    </div>
    <div class="b20 bd-t"></div>
</div>


<div class="all">
    <div class="main">
        <h1 class="title" id="title">秋季猫咪的饲养管理知识</h1>
        <div class="info">

            日期：2019-07-23&nbsp;&nbsp;&nbsp;&nbsp;
            浏览：
            <span id="hits">20</span>

        </div>
        <div class="content">
            <div class="content" id="article">
                当炎炎夏日过去，会迎来了我们最喜欢的秋高气爽的季节，猫咪在经过一个酷热的夏季，主人在秋天应为猫咪好好补充营养，
                再者秋天柳絮飘飘，可能会导致猫咪的过敏症，主人应当在秋季猫咪的生活管理上多加注意。
                <br>
                <br>
                <div style="text-align:center">
                    <img src="../../../imgs/MyCat-article-maomi1.png" alt="">
                </div>

                秋季秋高气爽，气候宜人，冷热适宜，同时为了迎接寒冬的来临和适应早晚气温的变化，被毛开始逐渐增厚，因而猫的食欲旺盛，
                此时应提高饲料的质量和增加食量，增强猫的体力。猫在秋季又进入一个繁殖季节，此时注意求偶外出猫，有无外伤和产科方面的疾病，
                在春季管理中提到的注意事项仍应加强。此外，深秋昼夜温差变化大，注意保温和加强锻炼，在管理上要预防感冒及呼吸道疾病。

            </div>
        </div>
        <div class="b20">&nbsp;</div>
        <div class="b20">&nbsp;</div>
        <div class="head-txt">
                <span>
                    <a href="" style="float: right">更多<i>&gt;</i></a>
                    <strong>同类信息</strong>
                </span>
        </div>

        <div class="related">
            <table width="100%">
                <tbody>
                <tr>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 暹罗猫好养吗？暹罗猫的性格</a>
                    </td>
                    <td width="50%">
                        <a href="" title="• 猫咪会假装睡觉吗">
                            • • 猫咪会假装睡觉吗</a>
                    </td>
                </tr>

                <tr>
                    <td width="50%">
                        <a href="" title="猫咪的牙齿有多少颗">
                            • 猫咪的牙齿有多少颗</a>
                    </td>
                    <td width="50%">
                        <a href="" title=" 猫咪吃鱼过多的危害性">
                            • 猫咪吃鱼过多的危害性</a>
                    </td>
                </tr>

                <tr>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 猫咪的基础美容护理有哪些？</a>
                    </td>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 如何让胆小的猫咪适应猫笼或航空箱</a>
                    </td>
                </tr>

                <tr>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 如何清洗猫咪的尾巴？</a>
                    </td>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 猫咪绝育后最佳的照顾方法</a>
                    </td>
                </tr>

                <tr>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 猫咪舌头的特点</a>
                    </td>
                    <td width="50%">
                        <a href="" title="暹罗猫好养吗？暹罗猫的性格">
                            • 关于猫咪腺囊：猫的皮肤里含有三种腺体</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- 右面 -->
    <div class="m3r">
        <div class="head-sub">
            <strong>推荐图文</strong>
        </div>

        <div class="list-thumb">
            <table width="100%">
                <tbody>
                <tr>
                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-1.png" width="134" height="103"
                                 alt="宠物猫的名字：猫咪起什么名字好听的方法">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">宠物猫的名字：猫咪起</a>
                            </li>
                        </ul>
                    </td>

                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-2.png" width="134" height="103"
                                 alt="养猫指南：饲养猫咪的25个小知识">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">养猫指南：饲养猫咪的</a>
                            </li>
                        </ul>
                    </td>
                </tr>

                <tr>
                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-3.png" width="134" height="103"
                                 alt="宠物猫的名字：猫咪起什么名字好听的方法">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">被猫抓伤怎么办？如何</a>
                            </li>
                        </ul>
                    </td>

                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-4.png" width="134" height="103"
                                 alt="养猫指南：饲养猫咪的25个小知识">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">你家的上榜了吗？趣味</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="b10">&nbsp;</div>
        <div class="head-sub">
            <strong>推荐阅读</strong>
        </div>
        <div class="list-rank">
            <ul>
                <li>
                    <em>1</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">宠物猫的名字：猫咪起什么名字好听的方法
                    </a>
                </li>
                <li>
                    <em>2</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">养猫指南：饲养猫咪的25个小知识
                    </a>
                </li>
                <li>
                    <em>3</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">被猫抓伤怎么办？如何避免被猫咪抓挠?
                    </a>
                </li>
                <li>
                    <em>4</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">你家的上榜了吗？趣味猫咪颜值排行榜TOP10
                    </a>
                </li>
                <li>
                    <em>5</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">买猫必备：买猫合同协议范本
                    </a>
                </li>
                <li>
                    <em>6</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">养个“毛孩子”到底会花多少钱？医疗成“最贵”花费
                    </a>
                </li>
                <li>
                    <em>7</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">新手猫奴遇到猫咪受伤怎么办？收好这些应急方法不慌乱
                    </a>
                </li>
                <li>
                    <em>8</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">孝顺猫咪给猫妈按摩，另两只则整天只会卖萌
                    </a>
                </li>
                <li>
                    <em>9</em>
                    <a href="" target="_blank"
                       title="宠物猫的名字：猫咪起什么名字好听的方法">猫见到老虎布偶后，全身炸毛，上去一顿猛揍
                    </a>
                </li>
            </ul>
        </div>
        <div class="head-sub">
            <strong>猫咪图片</strong>
        </div>
        <div class="list-thumb">
            <table width="100%">
                <tbody>
                <tr>
                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-5.png" width="134" height="103"
                                 alt="宠物猫的名字：猫咪起什么名字好听的方法">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">美国卷毛猫图片</a>
                            </li>
                        </ul>
                    </td>

                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-6.png" width="134" height="103"
                                 alt="养猫指南：饲养猫咪的25个小知识">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">马恩岛猫图片</a>
                            </li>
                        </ul>
                    </td>
                </tr>

                <tr>
                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-7.png" width="134" height="103"
                                 alt="宠物猫的名字：猫咪起什么名字好听的方法">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">索马里猫图片</a>
                            </li>
                        </ul>
                    </td>

                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-8.png" width="134" height="103"
                                 alt="养猫指南：饲养猫咪的25个小知识">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">新加坡猫图片</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-9.png" width="134" height="103"
                                 alt="宠物猫的名字：猫咪起什么名字好听的方法">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">阿比西尼亚猫图片</a>
                            </li>
                        </ul>
                    </td>

                    <td width="50%" valign="top">
                        <a href="">
                            <img src="<%=request.getContextPath()%>/imgs/MyCat-article-10.png" width="134" height="103"
                                 alt="养猫指南：饲养猫咪的25个小知识">
                        </a>
                        <ul>
                            <li>
                                <a href=""
                                   title="宠物猫的名字：猫咪起什么名字好听的方法">缅甸猫图片</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="container">
    <div>
        <h3 id="welcome">联系我们</h3>
    </div>
    <div class="col-md-4">
        <h4>
            <i class="	glyphicon glyphicon-map-marker"></i>&nbsp;&nbsp;Location
        </h4>
        <p>345 Setwant natrer,</p>
        <p>Metropolitan, Italy.</p>
    </div>
    <div class="col-md-4">
        <h4>
            <i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;PHONE
        </h4>
        <p>+1(401) 1234 567.</p>
        <p>+1(804) 4261 150.</p>
    </div>
    <div class="col-md-4">
        <h4>
            <i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;E-MAIL
        </h4>
        <p>
            <a href="mailto:info@example.com">Example1@gmail.com</a>
        </p>
        <p>
            <a href="mailto:info@example.com">Example2@gmail.com</a>
        </p>
    </div>
</div>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>

</body>

</html>
