<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/28
  Time: 14:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
        <meta charset="utf-8">
        <title>疫苗表单</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    </head>
<body>

<jsp:include page="../common/nav.jsp"></jsp:include>
<form class="layui-form" action="<%=request.getContextPath()%>/mycat/vaccineView" style="width:40%; margin: 0 auto;" >
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-block">
            <input type="text" name="username" placeholder="请输入" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机号</label>
        <div class="layui-input-block">
            <input type="text" name="userPhone" placeholder="请输入" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">选择猫咪</label>
        <div class="layui-input-block">
            <select name="catId">
                <option value="-1" selected>请选择猫咪</option>
                <c:forEach items="${requestScope.cats}" var="cat">
                    <option value="${cat.id}">${cat.name}</option>
                </c:forEach>
            </select>
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">是否接种过疫苗</label>
        <div class="layui-input-block">
            <input type="radio" name="isVaccine" value="0" title="是">
            <input type="radio" name="isVaccine" value="1" title="否" checked>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">请选择预约时间</label>
        <div class="layui-inline">
            <input type="text" name="date" class="layui-input" id="test1">
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">请填写注意事项</label>
        <div class="layui-input-block">
            <textarea name="text" placeholder="请输入内容" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="*">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>

</form>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use('form', function(){
        var form = layui.form;

    });
</script>


<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#test1' //指定元素
        });
    });
</script>
</body>
</html>
