<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/10
  Time: 16:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="utf-8">
    <title>发表动态</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.css">

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>
<div class="layui-col-md6 layui-col-lg-offset3">
    <form action="<%=request.getContextPath()%>/mycat/addimg" class="layui-form">


        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">

            <legend><li class=""><a class="" data-toggle="modal" data-target="#myModal1" lay-event="edit">
                给猫咪选个美照吧!
            </a></li></legend>
        </fieldset>
        <%--<div class="" style="">--%>
            <%--<li class=""><a class="" data-toggle="modal" data-target="#myModal1" lay-event="edit">--%>
                <%--选图片--%>
            <%--</a></li>--%>
        <%--</div>--%>
        <div class="layui-form-item">
            <label class="layui-form-label">选猫咪</label>
            <div class="layui-input-block">
                <select name="catId">
                    <option value="-1" selected>请选择猫咪</option>
                    <c:forEach items="${requestScope.cats}" var="cat">
                        <option value="${cat.id}">${cat.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <textarea placeholder="说点什么吧" class="layui-textarea" name="text"></textarea>
            <input hidden name="urlimg" id="urlimg">

        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="layui-btn layui-btn-primary" lay-submit="" lay-filter="demo1">立即提交
                </button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
</div>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×
                </button>
                <h4 class="modal-title" id="myModalLabel1">
                    我也要发
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="imgformmycat">
                    <input type="file" name="myfile" id="myfile">
                </form>
                <div id="preview">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal" onclick="send()">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src=" https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" language="JavaScript">

    $('#myfile').change(function () {
        var abc = " ";
        $("#imgformmycat").ajaxSubmit({
            url: "${path}/mycat/mycatimg",

            method: 'POST',
            datType: 'json',

            success: function (data) {
                console.log(data.imgUrl);
                abc = data.imgUrl;
                document.getElementById("urlimg").value = abc
//                console.log(typeof data.imgUrl);
                var ele = "<img src=\"\"/>"
                $("#preview").append(ele);//编程一个jquery的对象

                $("#preview").find("img").attr("src", data.imgUrl);//调用jquery的方法找到
                // 图片的标签的名称，再设置里面的src的属性的值


//                window.location = '/magic/mycat/addimg?urlimg=' + urlimg;

            },
            error: function (data) {
                alert(data);
            }
        })

    })

</script>
<script>
    layui.use('form', function () {
        var form = layui.form;
    });
</script>
</body>
</html>

