<%@ taglib prefix="v" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/27
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>添加猫咪</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">
    <style>
        .layui-form-label {
            text-align: center;
        }
    </style>
</head>

<body>
<jsp:include page="../common/nav.jsp"></jsp:include>

<!-- 添加猫咪表 -->
<div class="layui-row">
    <div class="layui-col-md2 layui-col-md-offset1">
        <jsp:include page="../foster/side.jsp"></jsp:include>
    </div>
    <div class="layui-col-md6 layui-col-md-offset3" style="margin-top: 30px">
        <form class="layui-form" action="<%=request.getContextPath()%>/mycat/addcatView">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">猫咪名字</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" autocomplete="off" class="layui-input" lay-verify="required">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">猫咪几岁</label>
                    <div class="layui-input-block">
                        <input id="age" type="number" name="age" class="layui-input" lay-verify="required">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">猫咪体重</label>
                    <div class="layui-input-block">
                        <input id="weight" type="number" name="weight" class="layui-input" lay-verify="required">
                    </div>
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align: center">性别</label>
                <div class="layui-input-block">
                    <input type="radio" name="sex" value="1" title="公" checked="">
                    <input type="radio" name="sex" value="2" title="母">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否疫苗</label>
                <div class="layui-input-block">
                    <input type="radio" name="isSterilization" value="1" title="是" checked="">
                    <input type="radio" name="isSterilization" value="2" title="否">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">猫咪品种</label>
                    <div class="layui-input-block">
                        <select name="speciesId" lay-verify="required" lay-search>
                            <option value="-1"></option>
                            <v:forEach items="${catSpecies}" var="catSpecie">
                                <option value="${catSpecie.id}">${catSpecie.speciesName}</option>
                            </v:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button type="submit" class="layui-btn layui-btn-primary" lay-submit lay-filter="sub">立即提交
                    </button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- 底部 -->
<jsp:include page="../common/bottom.jsp"></jsp:include>

<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>


<script>
    layui.use('form', function () {
        var form = layui.form;

        form.on('submit(sub)', function (data) {
            // console.log($("#age").val());
            if ($("select").val() == "-1") {
                layer.msg("请填写未填项");
                return false;
            }

            // if (/^\d$/.test($("#age").val())) {
            //     layer.msg("年纪请填写数字项");
            //     return false
            // }
            //
            // if (/^\d+\d$/.test($("#weight").val())||/^\d$/.test($("#weight").val())) {
            //     layer.msg("体重请填写数字项");
            //     return false
            // }

            return true;
        });
    });
</script>

</body>

</html>


