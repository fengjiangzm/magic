<%--
  Created by IntelliJ IDEA.
  User: success
  Date: 2019/9/12 0012
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.net.URLDecoder" %>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>用户注册</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">

    <title>注册</title>
    <style>
        .passport-pageMainCnt {
            width: 100%;
            height: 650px;
            position: relative;
            clear: both;
            background-repeat: no-repeat;
            background-position: center top;
        }

        .w {
            max-width: 1190px;
            min-width: 1190px;
            margin: 0 auto;
        }

        .pr {
            position: relative;
        }

        .login-page-title {
            padding: 10px;
            position: absolute;
            z-index: 1;
            top: 260px;
            left: 20px;
            color: #fff;
        }

        .login-page-title h1 {
            font-size: 40px;
        }

        .login-page-title h2 {
            font-size: 20px;
            line-height: 60px;
        }

        .loginBlock {
            width: 400px;
            z-index: 0;
            top: 40px;
            padding: 40px 30px;
            border-radius: 8px;
            border: 1px solid #e2e2e2;
            background: #fff;
            margin-top: 150px;
        }

        .base-mr10 {
            margin-right: 10px;
        }

        .xs-Element {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
        }

        .fr {
            float: right;
            display: inline;
        }

        .wel {
            font-size: 20px;
            float: left;
        }

        .go-login {
            color: #00a2d4;
            float: right;
        }

        .error-tip {
            color: red;
            margin-left: 10px;
            font-size: 12px;
        }

        .fl {
            float: left;
            display: inline;
        }

        .login-page-a {
            background: #f9f9f9;
            color: #333;
            font-size: 14px;
            line-height: 40px;
            width: 50%;
            text-align: center;
            cursor: pointer;
        }

        .ipt {
            position: relative;
        }

        .ipt-t input {
            border-radius: 10px;
        }

        .ipt i {
            position: absolute;
            top: 10px;
            left: 15px;
        }

        .ipt input {
            text-indent: 30px;
            border-radius: 10px;
        }

        .layui-inline {
            width: 100%;

        }


        .d-code {
            width: 230px;
            float: left;
        }

        .get-imgcode {
            width: 100px;
            height: 40px;
            border-radius: 10px;
            float: right;
        }

        .get-imgcode img {
            width: 100px;
            height: 40px;
            border-radius: 10px;

        }

        .get-code {
            width: 100px;
            border-radius: 10px;
            background-color: #cfcfcf;
            color: #fff;
            float: right;
        }


        .register-btn {
            border-radius: 10px;
            /* background-color: #cfcfcf; */
            background-color: #008ff4;
            width: 100%;
        }

        .agreement {
            font-size: 12px;
            vertical-align: bottom;
            color: #008ff4;
        }
    </style>
</head>
<body style="background-color: turquoise">


<jsp:include page="../common/nav.jsp"></jsp:include>


<div class="passport-pageMainCnt" >
    <%--    background-image:url('<%=request.getContextPath()%>/imgs/person-all.jpg'--%>
    <div class="w clearFix pr">
        <div class="login-page-title">
            <h1>值得信任的寄养领养平台</h1>
            <h2>环境优美&nbsp;&nbsp;值得信赖&nbsp;&nbsp;是您的不二选择</h2>
        </div>
        <div class="fr  loginBlock base-mr10 xs-Element">
            <form class="layui-form" action="<%=request.getContextPath()%>/user/registerAction" method='post'>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <span class="wel">欢迎注册</span><a href="<%=request.getContextPath()%>/user/loginView" class="go-login">已有账号,立即登录</a>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <div class="ipt">
                            <i class="layui-icon layui-icon-username"></i>
                            <input type="text" onblur="checkRepeatUser(this)" name="username" placeholder="请输入您的用户名"
                                   autocomplete="off" class="layui-input">
                            <span class='glyphicon glyphicon-user form-control-feedback'></span>
                            <span id="uname_span"></span>
                        </div>
                        <span class="error-tip" id="error-tip1"></span>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <div class="ipt">
                            <i class="layui-icon layui-icon-password"></i>
                            <input type="password" name="password" onblur="checkPassword()" lay-verify="pass" placeholder="请设置登录密码"
                                   autocomplete="off" class="layui-input">
                            <span class='form-control-feedback glyphicon glyphicon-lock'></span>
                        </div>
                        <span class="error-tip" id="error-tip2"></span>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <div class="ipt">
                            <i class="layui-icon layui-icon-password"></i>
                            <input type="password" name="pwd" onblur="checkPassword()" lay-verify="pass" placeholder="请确认登录密码"
                                   autocomplete="off" class="layui-input">
                            <span class='form-control-feedback glyphicon glyphicon-lock'></span>
                        </div>
                        <span class="error-tip"></span>
                    </div>
                </div>
                <div class="layui-form-item">
                    <button type="submit" class="layui-btn register-btn" lay-submit lay-filter="register">立即注册</button>
                </div>
                <div class="layui-form-item" pane="">
                    <div class="c-box">
                        <input type="checkbox" lay-skin="primary" title="我已阅读并接受 "
                               checked=""><span class="agreement"><a href="">《膜蛤猫舍服务协议》</a></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script>

    var usernameFlag = false;

    var passwordFlag = false;

    function checkRepeatUser(obj) {
        //获取用户输入的名字
        var uname = obj.value;
        //用户名不为空
        if (uname != "") {
            //发送ajax异步请求,检测用户名是否存在
            $.get('${path}/user/checkName', {username: uname}, function (data) {
                if (data.flag == false) {
                    $("#error-tip1").html("用户名已经存在了!");
                    usernameFlag = false;
                } else {
                    $("#error-tip1").html("");
                    usernameFlag = true;
                }
            });
        }
    }

    function checkPassword() {
        var pwd1 = $("input[name='password']").val();
        var pwd2 = $("input[name='pwd']").val();

        if (pwd1!=""&&pwd2!=""){
            if (pwd1!=pwd2){
                passwordFlag = false;
                $("#error-tip2").html("两次密码不相同")
            }else {
                passwordFlag = true;
                $("#error-tip2").html("")
            }
        }
    }

    layui.use('form',function () {
        var form = layui.form;

        form.on('submit(register)', function(data){
            if (passwordFlag && usernameFlag){
                return true;
            }
            return false;
        });
    })
</script>
</body>