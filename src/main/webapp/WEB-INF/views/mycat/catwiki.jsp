<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/27
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>疾病百科</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/mycat/catwiki.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">

</head>

<body>
<jsp:include page="../common/nav.jsp"></jsp:include>
<!-- 导航信息 -->
<div class="m">
    <div class="nav">
        <a href="<%=request.getContextPath()%>/foster/index">首页</a>
        <i>&gt;</i>
        <a href="<%=request.getContextPath()%>/mycat/catstrategy">养猫指南</a>
        <i>&gt;</i>
        <a href="">疾病百科</a>
    </div>
    <div class="b20 bd-t"></div>
</div>

<!-- 主体 -->
<div class="container-fluid" style="width: 80%; margin-top: 5%;">
    <div class="row">
        <div class="col-md-9 col-xs-9">
            <div class="catlist">
                <ul>
                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪精神压抑的表现和导致的疾病现象">猫咪精神压抑的表现和导致的疾病现象</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="建议收藏：猫咪家庭急救小手册">建议收藏：猫咪家庭急救小手册</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪不正常掉毛会有哪些原因呢？">猫咪不正常掉毛会有哪些原因呢？</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫伤风疾病的潜伏期通常是多久呢？ 猫感冒的症状">猫伤风疾病的潜伏期通常是多久呢？ 猫感冒的症状</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="英国短毛猫常见的疾病之打喷嚏现象的原因">英国短毛猫常见的疾病之打喷嚏现象的原因</a>
                    </li>
                    <li class="sp">&nbsp;</li>

                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="注射猫三联需要注意的事项及猫三联的免疫流程">注射猫三联需要注意的事项及猫三联的免疫流程</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪会对食物过敏吗？猫咪过敏常见症状">猫咪会对食物过敏吗？猫咪过敏常见症状</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="为什么猫咪不能喝牛奶？猫咪喝牛奶腹泻怎么办？">为什么猫咪不能喝牛奶？猫咪喝牛奶腹泻怎么办？</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="春季忽冷忽热的天气谨防猫咪疾病">春季忽冷忽热的天气谨防猫咪疾病</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="宠物猫咪眼睛发炎的症状及治疗方法">宠物猫咪眼睛发炎的症状及治疗方法</a>
                    </li>
                    <li class="sp">&nbsp;</li>

                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪的嘴巴肿的原因及消除肿胀治疗方法">猫咪的嘴巴肿的原因及消除肿胀治疗方法</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪小脑先天性脑发育障碍（小脑运动失调症）的症状">猫咪小脑先天性脑发育障碍（小脑运动失调症）的症状</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="母猫生殖系统常见疾病有哪些？">母猫生殖系统常见疾病有哪些？</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪肝功能化验单的生化指标及诊断意义">猫咪肝功能化验单的生化指标及诊断意义</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪肾功能部分化验值所代表的意思">猫咪肾功能部分化验值所代表的意思</a>
                    </li>
                    <li class="sp">&nbsp;</li>

                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫肥厚性心脏病的症状">猫肥厚性心脏病的症状</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="喜马拉雅猫有哪些遗传性疾病">喜马拉雅猫有哪些遗传性疾病</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="宠物猫蠕形螨病的症状及治疗方法">宠物猫蠕形螨病的症状及治疗方法</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="猫咪直肠炎的症状及治疗方法？主要呈现呻吟和排便困难">猫咪直肠炎的症状及治疗方法？主要呈现呻吟和排便困难</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catwiki-link" target="_blank"
                           title="夏天猫咪皮肤病的预防方法">夏天猫咪皮肤病的预防方法</a>
                    </li>
                    <li class="sp">&nbsp;</li>
                </ul>
            </div>

        </div>

        <!-- 右面部分 -->

        <div class="col-md-3 col-xs-3">
            <div class="right-category">
                <ul>
                    <li class="side_on">
                        <a href="">养猫知识 <em>(649)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">猫咪疾病 <em>(474)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">宠物猫品种 <em>(41)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">宠物猫资讯 <em>(53)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">自制猫粮 <em>(88)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">猫咪趣事 <em>(11)</em></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div>
        <h3 id="welcome">联系我们</h3>
    </div>
    <div class="col-md-4">
        <h4>
            <i class="	glyphicon glyphicon-map-marker"></i>&nbsp;&nbsp;Location
        </h4>
        <p>345 Setwant natrer,</p>
        <p>Metropolitan, Italy.</p>
    </div>
    <div class="col-md-4">
        <h4>
            <i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;PHONE
        </h4>
        <p>+1(401) 1234 567.</p>
        <p>+1(804) 4261 150.</p>
    </div>
    <div class="col-md-4">
        <h4>
            <i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;E-MAIL
        </h4>
        <p>
            <a href="mailto:info@example.com">Example1@gmail.com</a>
        </p>
        <p>
            <a href="mailto:info@example.com">Example2@gmail.com</a>
        </p>
    </div>
</div>

<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>

</body>

</html>
