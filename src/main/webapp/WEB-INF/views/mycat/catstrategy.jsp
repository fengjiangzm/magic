<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/27
  Time: 14:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>养猫知识</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/mycat/catstrategy.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">
</head>

<body>

<jsp:include page="../common/nav.jsp"></jsp:include>

<!-- 导航信息 -->
<div class="m">
    <div class="nav">
        <a href="<%=request.getContextPath()%>/foster/index">首页</a>
        <i>&gt;</i>
        <a href="">养猫指南</a>
        <i>&gt;</i>
        <a href="">养猫知识</a>
    </div>
    <div class="b20 bd-t"></div>
</div>
<!-- 主体 -->
<div class="container-fluid" style="width: 80%; margin-top: 5%;">
    <div class="row">
        <div class="col-md-9 col-xs-9">
            <div class="catlist">
                <ul>
                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="秋季猫咪的饲养管理知识">秋季猫咪的饲养管理知识</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="暹罗猫好养吗？暹罗猫的性格">暹罗猫好养吗？暹罗猫的性格</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪会假装睡觉吗">猫咪会假装睡觉吗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪的牙齿有多少颗">猫咪的牙齿有多少颗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪吃鱼过多的危害性">猫咪吃鱼过多的危害性</a>
                    </li>
                    <li class="sp">&nbsp;</li>

                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="秋季猫咪的饲养管理知识">秋季猫咪的饲养管理知识</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="l" target="_blank"
                           title="暹罗猫好养吗？暹罗猫的性格">暹罗猫好养吗？暹罗猫的性格</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪会假装睡觉吗">猫咪会假装睡觉吗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪的牙齿有多少颗">猫咪的牙齿有多少颗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪吃鱼过多的危害性">猫咪吃鱼过多的危害性</a>
                    </li>
                    <li class="sp">&nbsp;</li>

                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="秋季猫咪的饲养管理知识">秋季猫咪的饲养管理知识</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="暹罗猫好养吗？暹罗猫的性格">暹罗猫好养吗？暹罗猫的性格</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪会假装睡觉吗">猫咪会假装睡觉吗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪的牙齿有多少颗">猫咪的牙齿有多少颗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪吃鱼过多的危害性">猫咪吃鱼过多的危害性</a>
                    </li>
                    <li class="sp">&nbsp;</li>

                    <li>
                        <i>2019-07-23 16:30</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="秋季猫咪的饲养管理知识">秋季猫咪的饲养管理知识</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:25</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="暹罗猫好养吗？暹罗猫的性格">暹罗猫好养吗？暹罗猫的性格</a>
                    </li>
                    <li>
                        <i>2019-07-23 16:24</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪会假装睡觉吗">猫咪会假装睡觉吗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:51</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪的牙齿有多少颗">猫咪的牙齿有多少颗</a>
                    </li>
                    <li>
                        <i>2019-07-20 09:42</i>
                        <a href="<%=request.getContextPath()%>/mycat/catstrategy-link" target="_blank"
                           title="猫咪吃鱼过多的危害性">猫咪吃鱼过多的危害性</a>
                    </li>
                    <li class="sp">&nbsp;</li>
                </ul>
            </div>

        </div>

        <!-- 右面部分 -->

        <div class="col-md-3 col-xs-3">
            <div class="right-category">
                <ul>
                    <li class="side_on">
                        <a href="">养猫知识 <em>(649)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">猫咪疾病 <em>(474)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">宠物猫品种 <em>(41)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">宠物猫资讯 <em>(53)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">自制猫粮 <em>(88)</em></a>
                    </li>
                    <li class="side_li">
                        <a href="">猫咪趣事 <em>(11)</em></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- 底部 -->
<div class="container">
    <a href="#" name="contactus"></a>
    <div>
        <h3 id="welcome">联系我们</h3>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4>
                <i class="	glyphicon glyphicon-map-marker"></i>&nbsp;&nbsp;Location
            </h4>
            <p>345 Setwant natrer,</p>
            <p>Metropolitan, Italy.</p>
        </div>
        <div class="col-md-4">
            <h4>
                <i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;PHONE
            </h4>
            <p>+1(401) 1234 567.</p>
            <p>+1(804) 4261 150.</p>
        </div>
        <div class="col-md-4">
            <h4>
                <i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;E-MAIL
            </h4>
            <p>
                <a href="mailto:info@example.com">Example1@gmail.com</a>
            </p>
            <p>
                <a href="mailto:info@example.com">Example2@gmail.com</a>
            </p>
        </div>
    </div>
</div>

<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>

<script type="text/javascript">
    $(".carousel").carousel({
        interval: 2500
    })
</script>
</body>

</html>
