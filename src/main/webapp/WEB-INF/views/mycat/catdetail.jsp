<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/27
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <%--<link rel='stylesheet' href='<%=request.getContextPath()%>/css/mycat/catdetail.css' type='text/css'>--%>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <title>猫咪详情</title>
    <style>
        .a-box h3 {
            height: 22px;
            margin: 20px 0 0 0px;
            padding-left: 10px;
            line-height: 22px;
            background-repeat: no-repeat;
            color: #fff;
            font-size: 12px;
        }

        .profile .attach {
            line-height: 25px;
            height: 25px;
        }

        em,
        i {
            font-style: normal;
        }

        li {
            list-style: none;
        }

        .hd {
            margin-bottom: 30px;
            color: red;
        }

        .les-rit {
            float: left;
            margin-left: 40px;
            border: 1px solid #999999;
        }

        .les-let {
            float: left;
        }

        .wrap {
            margin: 0 auto;
            margin-left: 150px;
        }

        .content {
            width: 800px;
        }

        .profile .head img {
            width: 45px;
            height: 45px;
            border-radius: 30px;
        }

        .icon-time {
            padding-left: 25px;

            color: #999;
        }

        img {
            max-width: 100%;
            border: none;
            vertical-align: middle;
        }

        .pet-photo {
            margin: 0 auto;
            margin-top: 20px;
        }
    </style>
</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>
<div class="wrap">
    <!--导航面包屑 [-->
    <div class="path">
        当前位置 : <a href="<%=request.getContextPath()%>/foster/index" target="_blank">首页</a> &gt;
        <a href="<%=request.getContextPath()%>/mycat/catshow" target="_blank">美图展示 </a> &gt; <span>分享详情</span>
    </div>
    <!--导航面包屑 ]-->
    <div class="mod les-one c">
        <div class="les-let">
            <!--内容主体 [-->
            <div class="content" style="border: solid 1px #999999;">
                <div class="mod q-a">
                    <div class="q-box">
                        <div class="profile">
                            <div class="head" style="margin-left: 20px;">
                                <img src="<%=request.getContextPath()%>/imgs/MyCat-catdetail-01.png" alt="🍊">
                            </div>
                            <div class="area" style="margin-left: 30px;">
                                <p class="part">
                                        <span class="name"><img alt="@" height="24" width="24" rel="emoji"
                                                                name="0xD83C0xDF4A"></span>
                                </p>

                                <p class="attach">


                                    <em>${requestScope.cat.name} | ${requestScope.cat.speciesName}&nbsp;&nbsp;


                                        <em class="icon-pink"></em>


                                    </em>

                                    <span class="icon-time fr">${requestScope.catDetail.date}</span>
                                </p>

                            </div>
                        </div>
                        <div class="pet-photo" style="margin-left: 100px; ">

                            <div class="photo"><img src="${requestScope.catDetail.urlImg}"></div>


                        </div>
                        <div class="desc c" style="margin-top: 50px; margin-left: 50px;">

                            ${requestScope.catDetail.text}
                        </div>
                        <div class="icon-list c" style="float: right;margin-bottom: 2px;">
                            <span class="icon-praise fr">153</span><span class="icon-bubble fr">13</span>
                        </div>
                    </div>
                    <div class="a-box" style="margin-top: 50px; margin-left: 30px;">
                        <h3 class="doctor-title">网友热评<em></em></h3>
                        <ul class="answer-list">


                            <li class="item" >
                                <div class="profile">
                                    <div class="head">

                                        <img src="<%=request.getContextPath()%>/imgs/MyCat-detail-03.png" alt="ll">
                                    </div>
                                    <div class="area">
                                        <p class="part">
                                            <span class="name">ll</span>
                                        </p>
                                        <div class="desc">[可爱]</div>
                                        <div class="attach">
                                            <span class="icon-time">2019-09-23 10:29</span>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="profile">
                                    <div class="head">

                                        <img src="<%=request.getContextPath()%>/imgs/MyCat-detail-04.png" alt="豪哥">
                                    </div>
                                    <div class="area">
                                        <p class="part">
                                            <span class="name">豪哥</span>
                                        </p>
                                        <div class="desc">[可爱][可爱]</div>
                                        <div class="attach">
                                            <span class="icon-time">2019-09-23 07:19</span>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="profile">
                                    <div class="head">

                                        <img src="<%=request.getContextPath()%>/imgs/MyCat-detail-04.png" alt="豪哥">
                                    </div>
                                    <div class="area">
                                        <p class="part">
                                            <span class="name">豪哥</span>
                                        </p>
                                        <div class="desc">[可爱][可爱]</div>
                                        <div class="attach">
                                            <span class="icon-time">2019-09-23 07:19</span>
                                        </div>
                                    </div>
                                </div>
                            </li>


                        </ul>
                    </div>

                </div>

            </div>
            <!--内容主体 ]-->
        </div>
        <div class="les-rit">
            <!--猜你喜欢 [-->
            <div class="photo-list-cont" style="margin-right: 25px;">
                <div class="hd">
                    <h2 class="title">猜你喜欢</h2>
                </div>
                <ul class="list c">


                    <li>
                        <a target="_blank">
                            <p class="photo">
                                <img src="<%=request.getContextPath()%>/imgs/MyCat-catdetail-06.jpg" alt="好看的猫咪">
                            </p>
                            <p class="caption ell">好看的猫咪</p>
                        </a>
                    </li>
                    <li>
                        <a target="_blank">
                            <p class="photo">
                                <img src="<%=request.getContextPath()%>/imgs/MyCat-catdetail-06.jpg" alt="有趣的猫咪">
                            </p>
                            <p class="caption ell">有趣的猫咪</p>
                        </a>
                    </li>


                    <li>
                        <a target="_blank">
                            <p class="photo">
                                <img src="<%=request.getContextPath()%>/imgs/MyCat-catdetail-08.jpg" alt="拆家的猫咪">
                            </p>
                            <p class="caption ell">拆家的猫咪</p>
                        </a>
                    </li>


                </ul>
            </div>
            <!--猜你喜欢 ]-->
        </div>
    </div>
</div>

<a class="gotop" href="#" style="display: none;"></a>
</body>
</html>
