<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/27
  Time: 14:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>猫咪展示</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.css">

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">

    <style>
        .photo:hover img {
            opacity: 0.8;
        }

        .photo img {
            -webkit-transition: all .2s;
            transition: all .2s;
        }

        img {
            max-width: 100%;
            border: none;
            vertical-align: middle;
        }

        ul,
        ol {
            list-style: none;
        }

        p,
        h1,
        h2,
        img,
        form {
            margin: 0;
            padding: 0;
            float: left;
            margin-left: 10px;
            margin-top: 10px;
        }

        .cont .hd a {
            float: right;
            width: 60px;
            text-align: right;
            color: #9A9A9A;
        }

        h2 {
            color: red;
            margin-bottom: 1%;
            line-height: 0.4;
        }
    </style>
</head>

<body>
<jsp:include page="../common/nav.jsp"></jsp:include>


<!-- 轮播 -->
<%--<div class="layui-carousel container" id="test1" style="background: none;margin-top: 30px ">--%>
<%--<div carousel-item="">--%>
<%--<div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/MyCat-catshow-13.jpg" alt=""></div>--%>
<%--<div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/MyCat-catshow-14.jpg" alt=""></div>--%>
<%--</div>--%>
<%--</div>--%>


<!--主体 -->
<%--border: solid 1px #999999;--%>
<div class="wrap" style="width: 60%; margin: 0 auto; height: 50%; ">
    <!--今日热赞 [-->
    <div class="mod">
        <div class="cont praise-cont" style="margin-top: 30px;">
            <div class="hd">
                <h2>今日热门</h2>
                <h4 class="" style="color: #c7254e"><a class=""  href="<%=request.getContextPath()%>/mycat/catdynamics">
                    发动态
                </a></h4>
            </div>
        </div>


        <div class="layui-row" style="margin-right: 30px;">
            <div class="modc">
                <div class="layui-col-md5">
                    <div class="grid-demo grid-demo-bg1">
                        <li class="one">
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-01.jpg" alt="">
                                </p>
                                <p class="a-crown">&nbsp;</p>
                            </a>
                        </li>
                    </div>
                </div>

                <div class="layui-col-md7">

                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-05.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-06.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-04.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-05.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-06.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>

                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-07.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-08.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-09.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-10.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-11.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>
                    <div class="layui-col-md3">
                        <li>
                            <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                <p class="photo">
                                    <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-12.jpg" alt="">
                                </p>
                            </a>
                        </li>
                    </div>

                        <div class="layui-col-md3">
                            <li>
                                <a target="_blank" href="<%=request.getContextPath()%>/mycat/hotcatdetail">
                                    <p class="photo">
                                        <img src="<%=request.getContextPath()%>/imgs/MyCat-catshow-12.jpg">
                                    </p>
                                </a>
                            </li>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src=" https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script>
    layui.use('carousel', function () {
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1'
            , width: '70%' //设置容器宽度
            , height: '400px'
            , arrow: 'hover' //始终显示箭头
            , anim: 'fade' //切换动画方式
        });
    });
</script>
<script>
    layui.use('laydate', function () {
        var laydate = layui.laydate;

        //常规用法
        laydate.render({
            elem: '#test1'
        });
    });
</script>

</body>

</html>
