<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/10/8
  Time: 14:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src=" https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>
    <script type="text/javascript" src="${path}/js/ajaxfileupload.js"></script>

</head>

<body>
<form method="post" enctype="multipart/form-data" id="imgform">
    <input type="file" name="myfile" id="myfile">

</form>

<div id="preview">

</div>

<script type="text/javascript" language="JavaScript" >

    $('#myfile').change( function() {
        $("#imgform").ajaxSubmit({
            url: "${path}/test/upload",
            method: 'POST',
            datType: 'json',

            success: function (data) {
                console.log(data.imgUrl);
                console.log(typeof data.imgUrl);
                var ele = "<img src=\"\"/>"
                $("#preview").append(ele);//编程一个jquery的对象
                $("#preview").find("img").attr("src",data.imgUrl);//调用jquery的方法找到
//                        图片的标签的名称，再设置里面的src的属性的值

            },
            error: function (data) {
                alert(data);
            }
        })
    })


</script>
</body>
</html>