<%@ taglib prefix="v" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/9/20
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>我的猫咪</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
</head>
<body>
<!-- 导航栏 -->
<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="layui-row">
    <div class="layui-col-md2 layui-col-lg-offset1">
        <jsp:include page="side.jsp"></jsp:include>

    </div>

    <div class="layui-col-md8 layui-col-lg-offset3">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>猫咪详情</legend>
        </fieldset>

        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <form id="fff" name="testname" action="${path}/foster/myCatUpdate">
                    <input type="text" name="id" hidden value="${cat.id}">
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        &nbsp;&nbsp;喵名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${cat.name}"
                                                                      name="name">
                        <span class="layui-badge-dot"></span>
                    </blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        &nbsp;&nbsp;年纪&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${cat.age}"
                                                                      name="age">
                        <span class="layui-badge-dot layui-bg-orange"></span>
                    </blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        &nbsp;&nbsp;性别&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${cat.sex}"
                                                                     name="sex">
                        <span class="layui-badge-dot layui-bg-green"></span>
                    </blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        &nbsp;&nbsp;体重&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${cat.weight}"
                                                                     name="weight">
                        <span class="layui-badge-dot layui-bg-cyan"></span>
                    </blockquote>

                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        &nbsp;&nbsp;品种&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${cat.speciesName}"
                                                                     name="speciesName">
                        <span class="layui-badge-dot layui-bg-cyan"></span>
                    </blockquote >
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        <div class="" style="margin-left: 120px;">
                            <input type="submit" value="提交修改"/>
                        </div>
                    
                    </blockquote >

                </form>
            </div>
        </div>
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>服务记录</legend>
        </fieldset>
        <ul class="layui-timeline">
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis"></i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">8月18日</h3>
                    <p>
                        早上八点吃狗粮
                        <br>中午十一点半去吃猫粮
                        <br>晚上五点吃杂粮<i class="layui-icon"></i>
                        <br>晚上玩游戏
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis"></i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">8月17日</h3>
                    <p>
                        早上八点吃狗粮
                        <br>中午十一点半去吃猫粮
                        <br>晚上五点吃杂粮<i class="layui-icon"></i>
                        <br>晚上玩游戏
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis"></i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">8月16日</h3>
                    <p>
                        早上八点吃狗粮
                        <br>中午十一点半去吃猫粮
                        <br>晚上五点吃杂粮<i class="layui-icon"></i>
                        <br>晚上玩游戏
                    </p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis"></i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">2019年8月15日</h3>
                    <p>
                        出生啦
                    </p>
                </div>
            </li>
        </ul>
        <br>

    </div>


</div>


<%--<jsp:include page="../common/footer.jsp"></jsp:include>--%>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

<script>
    layui.use('element', function () {
        var element = layui.element;
    });

    function replay(obj, commentId, toId, toName) {
        $(".reply_textarea").remove();
        $(obj).parent().next().append("<form class='layui-form reply_textarea' action='${path}/comment/addReplay'>\n" +
            "                <div class='layui-form-item layui-form-text'>\n" +
            "                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +
            "                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +
            "                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +
            "                    <textarea name='content' placeholder='请输入内容' class='layui-textarea'></textarea>\n" +
            "                    <button type='submit' class='layui-btn' style='margin-top: 10px'>\n" +
            "                        <i class='layui-icon'>&#xe608;</i> 回复\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "            </form>");
    }

    function replay2(obj, commentId, toId, toName) {
        $(".reply_textarea").remove();
        $(obj).parent().append("<form class='layui-form reply_textarea' action='${path}/comment/addReplay'>\n" +
            "                <div class='layui-form-item layui-form-text'>\n" +
            "                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +
            "                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +
            "                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +
            "                    <textarea name='content' placeholder='请输入内容' class='layui-textarea'></textarea>\n" +
            "                    <button type='submit' class='layui-btn' style='margin-top: 10px'>\n" +
            "                        <i class='layui-icon'>&#xe608;</i> 回复\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "            </form>");
    }
</script>
</body>
</html>