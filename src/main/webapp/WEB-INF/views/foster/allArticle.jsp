<%@ taglib prefix="v" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/10/9
  Time: 14:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>title</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/mycat/catwiki.css" type="text/css">

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
</head>
<body>

<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="layui-container" id="app">
    <div class="layui-row">
        <div class="layui-col-md8 layui-col-md-offset1" style="margin-top: -45px">

            <v:forEach var="item" items="${requestScope.articles}">
                <div class="layui-card">
                    <div class="layui-card-header"><h2>
                        <a href="${path}/article/articleView?id=${item.id}" target="_blank">${item.title}</a>
                        <span style="float: right">${item.createDate}</span>
                    </h2></div>
                    <div class="layui-card-body">
                            ${item.desc}
                    </div>
                    <br>
                    <div>
                            <%--                        <i id="star${item.id}" onclick="fun(this,${item.id})" name="star" class="layui-icon layui-icon-rate"></i>--%>
                            <%--                        <span >${item.star}</span>--%>
                        <span style="float: right">浏览量${item.viewNumber}</span>
                    </div>

                    <hr>
                    <br>

                </div>
            </v:forEach>
            <div id="test1"></div>

        </div>
        <%--layui-col-md-offset8--%>
        <div class="col-md-2 layui-col-md-offset10" style="margin-top: 100px; margin-right:10px;">
        <%--<div class="layui-col-md2" style="margin-top: 100px; margin-right:10px;">--%>

            <%--            <form class="layui-form" action="/magic/article/allArticleView">--%>
            <%--                <div class="layui-form-item">--%>
            <%--                    <label class="layui-form-label">搜索</label>--%>
            <%--                    <div class="layui-input-block">--%>
            <%--                        <input type="text" name="title" autocomplete="off" class="layui-input">--%>
            <%--                    </div>--%>
            <%--                </div>--%>
            <%--                <div class="layui-form-item">--%>
            <%--                    <div class="layui-input-block">--%>
            <%--                        <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>--%>
            <%--                    </div>--%>
            <%--                </div>--%>
            <%--            </form>--%>


            <div class="right-category">
                <ul>
                    <v:if test="${type2==''}">
                        <li class="side_on">
                            <a href="/magic/article/allArticleView">全部 <em>(649)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2!=''}">
                        <li class="side_li">
                            <a href="/magic/article/allArticleView">全部 <em>(649)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2=='养猫知识'}">
                        <li class="side_on">
                            <a href="/magic/article/allArticleView?type=养猫知识">养猫知识 <em>(649)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2!='养猫知识'}">
                        <li class="side_li">
                            <a href="/magic/article/allArticleView?type=养猫知识">养猫知识 <em>(649)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2=='猫咪疾病'}">
                        <li class="side_on">
                            <a href="/magic/article/allArticleView?type=猫咪疾病">猫咪疾病 <em>(474)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2!='猫咪疾病'}">
                        <li class="side_li">
                            <a href="/magic/article/allArticleView?type=猫咪疾病">猫咪疾病 <em>(474)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2=='宠物猫品种'}">
                        <li class="side_on">
                            <a href="/magic/article/allArticleView?type=宠物猫品种">宠物猫品种 <em>(41)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2!='宠物猫品种'}">
                        <li class="side_li">
                            <a href="/magic/article/allArticleView?type=宠物猫品种">宠物猫品种 <em>(41)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2=='宠物猫资讯'}">
                        <li class="side_on">
                            <a href="/magic/article/allArticleView?type=宠物猫资讯">宠物猫资讯 <em>(53)</em></a>
                        </li>
                    </v:if>
                    <v:if test="${type2!='宠物猫资讯'}">
                        <li class="side_li">
                            <a href="/magic/article/allArticleView?type=宠物猫资讯">宠物猫资讯 <em>(53)</em></a>
                        </li>
                    </v:if>

                </ul>
            </div>
        </div>

    </div>
</div>

<div id="type" hidden>${type2}</div>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>

<script>

    var type1 = $("#type").html();
    var title1 = $("#title").html();

    layui.use('laypage', function () {
        var laypage = layui.laypage;

        laypage.render({
            elem: 'test1'
            , count: ${pageNumber}
            , limit: 5,
            curr: function () {
                var page = ${pageNum};
                return page ? page : 1;
            }(),
            jump: function (obj, first) {
                if (!first) {
                    window.location = '/magic/article/allArticleView?pageNum=' + obj.curr + '&type=' + type1;
                }
            }
        });
    });


</script>
</body>
</html>