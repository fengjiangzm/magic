<%@ page import="tech.aistar.utill.DateUtil" %>
<%@ page import="tech.aistar.entity.personal.UserInfo" %>
<%@ taglib prefix="v" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/10/8
  Time: 18:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>文章</title>

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">

</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>
<%--${article.content}--%>
<div class="layui-container" id="app">
    <div class="layui-row">
        <%--layui-col-md-offset1--%>
        <div class="layui-col-md7 ">
            <div id="article">
                <h1>${article.title}</h1>
                <hr>
                <div id="content" hidden >${article.content}</div>
                <%--            <div id="content"># 123</div>--%>
                <div id="result" style="padding: 10px;">

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>

            <div id="comment">
                <form class="layui-form" id="form1">
                    <div class="layui-form-item layui-form-text">
                        <%--此处需将value的值设置为猫咪的id值--%>
                        <input type="text" hidden name="ownerId" value="${article.id}">
                        <%--                        <input type="text" hidden name="fromId" value="1">--%>
                        <textarea name="content" placeholder="请输入内容" class="layui-textarea" lay-verify='required'></textarea>
                        <button type="button" class="layui-btn" style="margin-top: 10px" onclick="addComment()">
                            <i class="layui-icon">&#xe608;</i> 发表评论
                        </button>
                    </div>
                </form>
                <v:forEach var="comment" items="${requestScope.comments}">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <span class="fromName">${comment.fromName}</span>
                            <span style="padding-left: 10px">${comment.date}</span>
                            <a onclick="replay(this,${comment.id},${comment.fromId},'${comment.fromName}')"
                               class="layui-layout-right reply_btn" role="button" style="margin-right: 20px">回复</a>
                        </div>
                        <div class="layui-card-body">
                                ${comment.content}
                                <%--显示评论的回复--%>
                            <v:forEach items="${comment.replayList}" var="replay">
                                <div style="margin-left:20px">
                                    <div class="layui-card">
                                        <div class="layui-card-header">
                                            <span>${replay.fromName}</span>
                                            <span style="padding-left: 10px">${replay.date}</span>

                                            <a onclick="replay(this,${replay.commentId},${replay.fromId},'${replay.fromName}')"
                                               class="layui-layout-right reply_btn" role="button"
                                               style="margin-right: 20px">回复</a>
                                        </div>
                                        <div class="layui-card-body">
                                            @${replay.toName}:
                                                ${replay.content}
                                        </div>
                                    </div>
                                </div>
                            </v:forEach>
                        </div>
                    </div>
                    <br>
                    <br>
                    <hr>
                    <br>
                    <br>

                </v:forEach>
            </div>
        </div>
        <div class="col-md-4 layui-col-md-offset8" style="margin-top:50px;">
            <div class="head-sub">
                <strong>推荐阅读</strong>
            </div>
        <div class="list-rank" style="margin-top:20px;">
            <ul>
                <li>
                    <em>1</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=3" target="_blank"
                       title="幼猫多大能打疫苗?">幼猫多大能打疫苗?
                    </a>
                </li>
                <br>

                <li>
                    <em>2</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=32" target="_blank"
                       title="  天津猫舍血统纯种的英短蓝猫价格多少钱！">  天津猫舍血统纯种的英短蓝猫价格多少钱！
                    </a>
                </li>
                <br>
                <li>
                    <em>3</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=6" target="_blank"
                       title="养猫指南">养猫指南：日常喂养 ，应该吃多少？
                    </a>
                </li>
                <br>
                <li>
                    <em>4</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=4" target="_blank"
                       title="猫瘟怎么治，小猫得了猫瘟怎么办?">猫瘟怎么治，小猫得了猫瘟怎么办?
                    </a>
                </li>
                <br>
                <li>
                    <em>5</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=5" target="_blank"
                       title="幼猫什么时候驱虫?">幼猫什么时候驱虫?常见的内寄生虫.
                    </a>
                </li>
                <br>
                <li>
                    <em>6</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=30" target="_blank"
                       title="苏格兰折耳猫">上海成年苏格兰折耳猫价格多少钱？
                    </a>
                </li>
                <br>
                <li>
                    <em>7</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=10" target="_blank"
                       title="狂犬疫苗">狂犬疫苗有没有必要打？
                    </a>
                </li>
                <br>
                <li>
                    <em>8</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=29" target="_blank"
                       title="猫咪舌头为什么有粗糙的表层？">猫咪舌头为什么有粗糙的表层？
                    </a>
                </li>
                <br>
                <li>
                    <em>9</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=38" target="_blank"
                       title="2019年下半年深圳布偶猫市场价格趋势分析">2019年下半年深圳布偶猫市场价格趋势分析
                    </a>
                </li>
                <br>
                <li>
                    <em>10</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=39" target="_blank"
                       title="The Little Cat，预计售价1800美元">韩国初创公司发布猫咪专属智能跑步机
                    </a>
                </li>
                <br>
                <li>
                    <em>11</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=11" target="_blank"
                       title="猫咪该吃自制食物还是猫粮？">猫咪该吃自制食物还是猫粮？
                    </a>
                </li>
                <br>
                <li>
                <em>12</em>
                <a href="http://localhost:8888/magic/article/articleView?id=14" target="_blank"
                   title="消毒剂是否影响猫咪健康？">消毒剂是否影响猫咪健康？
                </a>
            </li>
                <br>
                <li>
                    <em>13</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=15" target="_blank"
                       title="养猫装备">养猫装备
                    </a>
                </li>
                <br>

                <li>
                    <em>14</em>
                    <a href="http://localhost:8888/magic/article/articleView?id=4" target="_blank"
                       title="猫瘟怎么治，小猫得了猫瘟怎么办?">猫瘟怎么治，小猫得了猫瘟怎么办?
                    </a>
                </li>
                <br>
            </ul>
        </div>
        </div>


        <div>

        </div>
    </div>
</div>

<script src="<%=request.getContextPath()%>/js/foster/showdown.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<%--<script src="<%=request.getContextPath()%>/plugins/vue/vue.min.js"></script>--%>
<script>

    //    new Vue({
    //        el:'#app',
    //        data:{
    //            comments:[]
    //        },
    //    })
    layui.use(['form'], function () {
        var form = layui.form;
    });

    $(function () {

        var text = document.getElementById("content").innerHTML;

        console.log(text);

        //创建实例
        var converter = new showdown.Converter();
        //进行转换
        var html = converter.makeHtml(text);

        document.getElementById("result").innerHTML = html;
    });


    function addComment() {
        // console.log($("#form1").serialize());
        var lo = location;
        $.ajax({
            type: "POST",//方法类型
            dataType: "json",//预期服务器返回的数据类型
            url: "${path}/comment/addComment",//url
            data: $('#form1').serialize(),
            success: function (result) {
                lo.reload();
            }
        });
    }


    function addReplay() {
        var lo = location;
        $.ajax({
            type: "POST",//方法类型
            dataType: "json",//预期服务器返回的数据类型
            url: "${path}/comment/addReplay",//url
            data: $('#form2').serialize(),
            success: function (result) {
                lo.reload();
            }
        });
    }

    function replay(obj, commentId, toId, toName) {
        $(".reply_textarea").remove();
        $(obj).parent().next().append("<form id='form2' class='layui-form reply_textarea'>\n" +
            "                <div class='layui-form-item layui-form-text'>\n" +
            "                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +
            "                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +
            "                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +
            "                    <textarea name='content' placeholder='请输入内容' class='layui-textarea' lay-verify='required'></textarea>\n" +
            "                    <button type='button' class='layui-btn' style='margin-top: 10px' onclick='addReplay()'>\n" +
            "                        <i class='layui-icon'>&#xe608;</i> 回复\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "            </form>");
    }

    function replay2(obj, commentId, toId, toName) {
        $(".reply_textarea").remove();
        $(obj).parent().append("<form id='form2' class='layui-form reply_textarea'>\n" +
            "                <div class='layui-form-item layui-form-text'>\n" +
            "                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +
            "                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +
            "                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +
            "                    <textarea name='content' placeholder='请输入内容' class='layui-textarea' lay-verify='required'></textarea>\n" +
            "                    <button type='button' class='layui-btn' style='margin-top: 10px' onclick='addReplay()'>\n" +
            "                        <i class='layui-icon'>&#xe608;</i> 回复\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "            </form>");
    }
</script>
</body>
</html>