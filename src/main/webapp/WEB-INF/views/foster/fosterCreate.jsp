<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/9/21
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>寄养</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">

</head>
<body>

<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="layui-row">
    <div class="layui-col-md2 layui-col-lg-offset1">
        <jsp:include page="side.jsp"></jsp:include>
    </div>
    <div class="layui-col-md6 layui-col-lg-offset3">
        <form action="<%=request.getContextPath()%>/foster/fosterCreate" class="layui-form" method="post">
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>选择猫咪</legend>
            </fieldset>

            <div class="layui-form-item">
                <label class="layui-form-label">选择猫咪</label>
                <div class="layui-input-block">
                    <select name="catId" lay-verify="required">
                        <option value="-1" selected>请选择猫咪</option>
                        <c:forEach items="${requestScope.cats}" var="cat">
                            <option value="${cat.id}">${cat.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <br>

                <div class="layui-input-block">
                    <a class="layui-btn  layui-btn-primary" role="button" href="${path}/mycat/addcat">添加猫咪</a>
                </div>
            </div>


            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>寄养时间选择</legend>
            </fieldset>

            <div class="layui-form">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label" style="width: 100px">开始时间</label>
                        <div class="layui-input-inline">
                            <input autocomplete="off" type="text" class="layui-input" id="startTime"
                                   name="startTime" placeholder="" lay-verify="required">
                        </div>
                    </div>
                    <br>
                    <div class="layui-inline">
                        <label class="layui-form-label" style="width: 100px">结束时间</label>
                        <div class="layui-input-inline">
                            <input autocomplete="off" type="text" class="layui-input" id="endTime" name="endTime"
                                   placeholder="" lay-verify="required">
                        </div>
                    </div>
                </div>
            </div>

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>寄养套餐选择</legend>
                <div class="layui-form-item">
                    <label class="layui-form-label">房间大小</label>
                    <div class="layui-input-block">
                        <c:forEach items="${requestScope.rooms}" var="room">
                            <div>
                                <input type="radio" name="roomId" value="${room.id}" title="${room.name}"
                                       lay-filter="room">
                                <span>剩余数量:${room.number}</span>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </fieldset>


            <%--            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">--%>
            <%--                <legend>寄养门店选择</legend>--%>
            <%--            </fieldset>--%>
            <%--            <div class="layui-form-item">--%>
            <%--                <label class="layui-form-label " style="width: 100px">门店选择</label>--%>
            <%--                <div class="layui-input-block">--%>
            <%--                    <select name="interest">--%>
            <%--                        <option value=""></option>--%>
            <%--                        <option value="0" selected="">门店1</option>--%>
            <%--                        <option value="1">门店2</option>--%>
            <%--                        <option value="2">门店3</option>--%>
            <%--                    </select>--%>
            <%--                </div>--%>
            <%--                <br>--%>

            <%--                <div class="layui-input-block">--%>
            <%--                    <a class="layui-btn  layui-btn-primary" role="button" href="">查看所有门店地址</a>--%>
            <%--                </div>--%>
            <%--            </div>--%>

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>附加服务</legend>
            </fieldset>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 100px">养护美容</label>
                <div class="layui-input-block">
                    <c:forEach var="event" items="${requestScope.events}">
                        <input type="checkbox" name="events" value="${event.id}" title="${event.name}"
                               lay-filter="event">
                        <%--                        onclick="addEvent(this,${event.price})"--%>
                    </c:forEach>
                </div>
                <br>
                <%--                <label class="layui-form-label" style="width: 100px">接送方式</label>--%>
                <%--                <div class="layui-input-block">--%>
                <%--                    <input type="radio" name="roomSize" value="派车" title="派车" checked="">--%>
                <%--                    <input type="radio" name="roomSize" value="自送" title="自送">--%>
                <%--                </div>--%>
            </div>

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>备注</legend>
            </fieldset>
            <div class="layui-form-item layui-form-text">
                <textarea placeholder="请输入内容" class="layui-textarea" name="remark"></textarea>
            </div>

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>价格</legend>
            </fieldset>
            <div class="layui-form-item layui-form-text">
                <h1 id="price">价格:0</h1>
                <input id="price1" name="price" type="text" hidden value="0">
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button type="submit" class="layui-btn  layui-btn-primary" lay-submit lay-filter="sub">立即提交
                    </button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>

        </form>

    </div>
</div>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js" charset="utf-8"></script>

<script>
    var all = 0;
    var roomPrice = 0;
    var eventPrice = 0;
    var start;
    var end;
    var day = 1;
    var dayWidth = Date.parse(new Date());


    layui.use('laydate', function () {
        var laydate = layui.laydate;
        laydate.render({
            elem: '#startTime',
            min: 0,
            done: function (value, date, endDate) {
                start = Date.parse(value);
                day = Math.abs(parseInt((end - start) / 1000 / 3600 / 24));
                show();
            }
        });
        laydate.render({
            elem: '#endTime',
            min: 0,
            done: function (value, date, endDate) {
                end = Date.parse(value);
                day = Math.abs(parseInt((end - start) / 1000 / 3600 / 24));
                show();
            }
        });
    });
    layui.use(['form'], function () {
        var form = layui.form;
        form.on('checkbox(event)', function (data) {
            console.log(data.value);
            var price = 0;

            if (data.value == 1) price = 10;
            if (data.value == 2) price = 40;
            if (data.value == 3) price = 998;

            if (data.elem.checked) eventPrice += price;
            else eventPrice -= price;
            show();

        });
        form.on('radio(room)', function (data) {
            console.log(data.value);

            if (data.value == 1) roomPrice = 40;
            if (data.value == 2) roomPrice = 20;
            if (data.value == 3) roomPrice = 10;


            show();
        });

        form.on('submit(sub)', function (data) {
            if ($("select").val() == "-1") {
                layer.msg("请选择猫咪");
                return false;
            }
            if (all==0){
                layer.msg("请填写未填项");
                return  false;
            }
            if ($("input[name='roomId']:checked").val()==undefined){
                layer.msg("请填写未填项");
                return  false;
            }

            return true;
        });
    });
    layui.use('element', function () {
        var $ = layui.jquery
            , element = layui.element;
    });

    function show() {
        all = eventPrice + roomPrice * day;
        if (!isNaN(all)) {
            $("#price").html("价格:" + all);
            $("#price1").val(all);
        }
    }

</script>

</body>
</html>