<%@ taglib prefix="v" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/9/21
  Time: 16:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul class="layui-nav layui-nav-tree layui-bg-green" style="position:fixed">
    <li class="layui-nav-item layui-nav-itemed">
        <a href="javascript:;">寄养猫咪</a>
        <dl class="layui-nav-child">
            <dd><a href="${path}/foster/fosterView">寄养介绍</a></dd>
            <dd><a href="${path}/foster/fosterCreateView">寄养</a></dd>
        </dl>
    </li>
    <li class="layui-nav-item layui-nav-itemed">
        <a href="javascript:;">我的猫咪</a>
        <dl class="layui-nav-child">
            <v:forEach var="cat" items="${cats}">
                <dd><a href="${path}/foster/myCatView?catId=${cat.id}">${cat.name}</a></dd>
            </v:forEach>
            <dd><a href="${path}/mycat/addcat">添加猫咪</a></dd>
        </dl>
    </li>
</ul>
