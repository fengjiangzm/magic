<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>确认订单</title>

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
</head>
<body>


<div class="layui-row">
    <div class="layui-col-md8 layui-col-md-offset2">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend><h1>寄养猫咪</h1></legend>
        </fieldset>
        <div class="layui-card">
            <div class="layui-card-header"><h2>神猫</h2></div>
            <div class="layui-card-body">
                <h4>神猫</h4>
                <h4>神猫</h4>
                <h4>6岁</h4>
                <h4>备注</h4>
                <p>哈哈哈哈</p>
                <h4></h4>

                <hr>
                <h2>附加服务</h2>
                <p>洗澡</p>

                <hr>
                <h2>配送方式</h2>
                <p>专车</p>
                <h3>地址</h3>
                <p>江苏省苏州市张家港市长兴中路8号科技新城</p>
            </div>
        </div>
        <div>
            <div class="layui-col-lg-offset9">

            </div>
        </div>
        <hr>
        <div>

            <div class="layui-col-lg-offset5 layui-col-md3">
                <a href="http://www.layui.com" class="layui-btn layui-btn-fluid">返回</a>
            </div>
            <div class="layui-col-lg-offset9">
                <a href="http://www.layui.com" class="layui-btn layui-btn-fluid">去结算</a>
            </div>
        </div>

    </div>
</div>


<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>

<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>

</body>
</html>