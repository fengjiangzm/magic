<%@ taglib prefix="v" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/10/1
  Time: 8:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>评论模板</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
</head>
<body>

<div class="layui-row" style="background-color: #999999 ;margin: 20px">
    <div class="layui-col-md6 layui-col-md-offset3" style="margin: 20px">
        <%--打印已存在的评论--%>
        <v:forEach var="comment" items="${requestScope.comments}">
            <div class="layui-card">
                <div class="layui-card-header">
                    <span class="fromName">${comment.fromName}</span>
                    <span style="padding-left: 10px">${comment.date}</span>

                    <a onclick="replay(this,${comment.id},${comment.fromId},'${comment.fromName}')"
                       class="layui-layout-right reply_btn" role="button" style="margin-right: 20px">回复</a>
                </div>
                <div class="layui-card-body">
                        ${comment.content}
                        <%--显示评论的回复--%>
                    <v:forEach items="${comment.replayList}" var="replay">
                        <div style="margin-left:20px">
                            <div class="layui-card">
                                <div class="layui-card-header">
                                    <span>${replay.fromName}</span>
                                    <span style="padding-left: 10px">${replay.date}</span>

                                    <a onclick="replay(this,${replay.commentId},${replay.fromId},'${replay.fromName}')"
                                       class="layui-layout-right reply_btn" role="button"
                                       style="margin-right: 20px">回复</a>
                                </div>
                                <div class="layui-card-body">
                                    @${replay.toName}:
                                        ${replay.content}
                                </div>
                            </div>
                        </div>
                    </v:forEach>

                </div>
            </div>
        </v:forEach>
        <form class="layui-form" action="${path}/comment/addComment">
            <div class="layui-form-item layui-form-text">
                <%--此处需将value的值设置为猫咪的id值--%>
                <input type="text" hidden name="ownerId" value="1">
                <input type="text" hidden name="fromId" value="1">
                <textarea name="content" placeholder="请输入内容" class="layui-textarea"></textarea>
                <button type="submit" class="layui-btn" style="margin-top: 10px">
                    <i class="layui-icon">&#xe608;</i> 发表评论
                </button>
            </div>
        </form>
    </div>
</div>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>

<script type="text/javascript">
    function replay(obj, commentId, toId, toName) {
        $(".reply_textarea").remove();
        $(obj).parent().next().append("<form class='layui-form reply_textarea' action='${path}/comment/addReplay'>\n" +
            "                <div class='layui-form-item layui-form-text'>\n" +
            "                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +
            "                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +
            "                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +
            "                    <textarea name='content' placeholder='请输入内容' class='layui-textarea'></textarea>\n" +
            "                    <button type='submit' class='layui-btn' style='margin-top: 10px'>\n" +
            "                        <i class='layui-icon'>&#xe608;</i> 回复\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "            </form>");
    }

    function replay2(obj, commentId, toId, toName) {
        $(".reply_textarea").remove();
        $(obj).parent().append("<form class='layui-form reply_textarea' action='${path}/comment/addReplay'>\n" +
            "                <div class='layui-form-item layui-form-text'>\n" +
            "                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +
            "                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +
            "                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +
            "                    <textarea name='content' placeholder='请输入内容' class='layui-textarea'></textarea>\n" +
            "                    <button type='submit' class='layui-btn' style='margin-top: 10px'>\n" +
            "                        <i class='layui-icon'>&#xe608;</i> 回复\n" +
            "                    </button>\n" +
            "                </div>\n" +
            "            </form>");
    }
</script>
</body>
</html>
