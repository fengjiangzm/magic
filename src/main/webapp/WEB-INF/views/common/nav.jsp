<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: FJ
  Date: 2019/9/20
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<ul class="layui-nav layui-bg-orange" lay-filter="">
    <li class="layui-nav-item" style="margin-left: 100px"><a href="${path}/foster/index"><b
            style="font-size: 20px;">膜蛤猫舍</b></a></li>
    <li class="layui-nav-item"><a href="${path}/foster/fosterCreateView">寄养猫咪</a></li>
    <li class="layui-nav-item"><a href="${path}/adoptCat/adoptCatView">领养猫咪</a></li>
    <li class="layui-nav-item"><a href="${path}/aboutus/aboutusView">关于我们</a></li>

    <div class="layui-layout-right" style="padding-right: 100px">
        <c:if test="${sessionScope.user == null}">
            <li class="layui-nav-item">
                <a href="${path}/user/loginView">登录</a>
            </li>
            <li class="layui-nav-item">
                <a href="${path}/user/registerView">注册</a>
            </li>
        </c:if>
        <c:if test="${sessionScope.user != null}">
            <li class="layui-nav-item">

                <a href="${path}/personal/personalCenterMyOrderView">个人中心</a>
                <%--<a href="${path}/personal/personalCenterView">个人中心</a>--%>
            </li>
            <li class="layui-nav-item">
                <a href="${path}/personal/personalCenterView"><img src="${sessionScope.userInfo.avatar}"
                                                                   class="layui-nav-img">${sessionScope.userInfo.nickname}</a>
            </li>
            <li class="layui-nav-item">
                <a href="${path}/user/loginOut">退出</a>
            </li>
        </c:if>
        <li class="layui-nav-item "><a href="#contactus">联系我们</a></li>
    </div>
</ul>