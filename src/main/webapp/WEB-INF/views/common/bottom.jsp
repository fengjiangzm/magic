<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- 底部 -->
    <a href="#" name="contactus"></a>
    <div>
        <h3 class="welcome1">联&nbsp;系&nbsp;我&nbsp;们</h3>
    </div>
    <div class="layui-row">
        <div class="layui-col-md-offset2 layui-col-md3">
            <h4>
                <i class="	glyphicon glyphicon-map-marker"></i>&nbsp;&nbsp;Location
            </h4>
            <p>北京市顺义区</p>
            <p>木林镇蒋各庄村</p>
        </div>
        <div class="layui-col-md3">
            <h4>
                <i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;PHONE
            </h4>
            <p>13801131151</p>
            <p>17645113867</p>
        </div>
        <div class="layui-col-md3">
            <h4>
                <i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;E-MAIL
            </h4>
            <p>
                <a href="https://email.qq.com/">2830308340@qq.com</a>
            </p>
            <p>
                <a href="https://mail.163.com/">lvrui199@163.com</a>
            </p>
        </div>
    </div>

