<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/9/25
  Time: 21:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>领养猫咪详情</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/catInfoCss.css" type="text/css">

    <style>
        .m {
            margin: auto;
        }

        .t {
            margin-top: 20px;
            margin-bottom: 150px;
        }
    </style>
</head>

<body>

<!-- 导航栏 -->
<jsp:include page="../common/nav.jsp"></jsp:include>

<!-- 猫咪详细信息 -->
<div class="t">
    <div class="m" id="outer">
        <div id="word">
            <img src="${requestScope.adoptCat.imgUrl}" alt="">
        </div>
        <div id="pic">
            <div class='catType'>${requestScope.cat.name}</div>
            <div class='catIntro topspace'>${requestScope.adoptCat.introduction}</div>
            <div class='topspace'>
                <span>种类</span>
                <span style='margin-left: 27px;'>${requestScope.catSpecies.speciesName}</span>
            </div>
            <div class='topspace'>
                <span>年龄</span>
                <span style='margin-left: 27px;'>${requestScope.cat.age}</span>
            </div>
            <div class='topspace'>
                <span>性别</span>
                <span style='margin-left: 27px;'>${requestScope.cat.sex}</span>
            </div>
            <div class='topspace'>
                <span>价格</span>
                <span style='margin-left: 27px;'>${requestScope.adoptCat.catPrice}</span>
            </div>
            <div class='topspace'>
                <span>累计评价</span>
                <span>112312</span>
            </div>
            <div class='topspace' style="padding-left:10px;">
                <button type="button" class="layui-btn layui-btn-warm layui-btn-radius"
                        id="adoptIt" onclick="adoptIt(${requestScope.cat.id})">领养它
                </button>
            </div>
        </div>
    </div>
</div>

<!-- 底部 -->

<jsp:include page="../common/bottom.jsp"></jsp:include>


<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>
<script>
    function adoptIt(cat_id) {
        $.get('${path}/createAdopt/view', function (data) {
            if (data.flag == false) {
                alert(data.message);
                window.location.href = "${path}/user/loginView";
            } else {
                window.location.href = "${path}/createAdopt/createView?cat_id=" + cat_id;
            }
        })
    }
</script>

</html>