<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/9/28
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<select name="userAddressId">
    <option value="-1" selected>====选择地址====</option>

    <c:forEach var="ua" items="${requestScope.userAddresses}">
        <c:if test="${ua.status == 0}">
            <option value='${ua.id}'>${ua.provinceName}${ua.cityName}${ua.areaName}${ua.detailAddress}</option>
        </c:if>
    </c:forEach>

</select>


<script>
    layui.use(['form'], function () {
        var form = layui.form;
    });
    layui.use('element', function () {
        var $ = layui.jquery
            , element = layui.element;
    });
</script>

