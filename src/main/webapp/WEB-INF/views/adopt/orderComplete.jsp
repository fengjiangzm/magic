<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/9/28
  Time: 19:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>您的订单完成</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/orderComplete.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
</head>
<body>
<!-- 导航栏 -->
<jsp:include page="../common/nav.jsp"></jsp:include>

<%--主页面--%>
<div class="order" style="margin-bottom: 300px;margin-top: 200px">
    <i class="layui-icon layui-icon-ok-circle"></i>订单创建完成,您可以选择
    <a href="${path}/foster/index">前往主页</a>
    或者 <a href="${path}/adoptCat/adoptCatView">继续领养</a><br>
    页面<span style="color:red" id="second">5</span>s后自动跳转到
    <a href="${path}/adoptCat/adoptCatView">领养界面</a>
</div>

<!-- 底部 -->
<jsp:include page="../common/bottom.jsp"></jsp:include>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    window.onload = function(){
        var time = 5;
        var secondEle = document.getElementById("second");
        var timer = setInterval(function(){
            secondEle.innerHTML = time;
            time--;
            if(time==0){
                clearInterval(timer);
                location.href="${path}/adoptCat/adoptCatView";
            }
        },1000);
    }
</script>
</body>
</html>
