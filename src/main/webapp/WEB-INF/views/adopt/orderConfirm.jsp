<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/10/9
  Time: 16:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>确认订单</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/orderComplete.css" type="text/css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;margin: auto;width:70%">
    <legend>请您确认订单!</legend>
</fieldset>

<form class="layui-form" style="margin: auto;width:70%;margin-top: 10px">
    <div class="layui-form-item">
        <label class="layui-form-label"style="width: 100px;">猫名</label>
        <div class="layui-input-block">
            <label class="layui-form-label">${requestScope.cat.name}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">您的姓名</label>
        <div class="layui-input-block">
            <label class="layui-form-label">${requestScope.adoptApplication.userName}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">手机号</label>
        <div class="layui-input-block">
            <label class="layui-form-label">${requestScope.adoptApplication.phone}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">邮箱</label>
        <div class="layui-input-block">
            <label class="layui-form-label">${requestScope.adoptApplication.userEmail}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">工资</label>
        <div class="layui-input-block">
            <label class="layui-form-label">${requestScope.adoptApplication.salary}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">身份证</label>
        <div class="layui-input-block">
            <label class="layui-form-label">${requestScope.adoptApplication.identity}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">日期</label>
        <div class="layui-input-block">
            <label class="layui-form-label"  style="width: auto" autocomplete="off">${requestScope.date}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">性别</label>
        <div class="layui-input-block">
            <label class="layui-form-label" style="width: auto">${requestScope.adoptApplication.sex}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">取猫方式</label>
        <div class="layui-input-block">
            <label class="layui-form-label" style="width: auto">${requestScope.adoptApplication.takingWay}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">选择的地址</label>
        <div class="layui-input-block">
            <label class="layui-form-label" style="width: auto">
                ${requestScope.userAddress.provinceName}${requestScope.userAddress.cityName}${requestScope.userAddress.areaName}${requestScope.userAddress.detailAddress}
            </label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">订单金额</label>
        <div class="layui-input-block">
            <label class="layui-form-label" style="width: auto">${requestScope.adoptCat.catPrice}</label>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">备注</label>
        <div class="layui-input-block">
            <label class="layui-form-label" style="width: auto">${requestScope.adoptApplication.note}</label>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" class="btn btn-default" onclick="confirmOrder()">确认订单</button>
        </div>
    </div>
</form>

<jsp:include page="../common/bottom.jsp"></jsp:include>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

<script>
    function confirmOrder() {
//        console.log("123");
        $.ajax({
            type: 'get',
            url: "${path}/createAdopt/complete?cat_id=${requestScope.cat.id}&dateTime=${requestScope.adoptApplication.date}",
            data: {
                userName: '${requestScope.adoptApplication.userName}',
                phone: '${requestScope.adoptApplication.phone}',
                userEmail: '${requestScope.adoptApplication.userEmail}',
                salary: ${requestScope.adoptApplication.salary},
                identity: '${requestScope.adoptApplication.identity}',
                sex: ${requestScope.adoptApplication.sex},
                takingWay: ${requestScope.adoptApplication.takingWay},
                userAddressId: ${requestScope.adoptApplication.userAddressId},
                note: '${requestScope.adoptApplication.note}',
            },
            success : function (data) {
                if (data.flag){
                    window.location.href = "${path}/alipay/pay/web?amt=${requestScope.adoptCat.catPrice}&orderId="+data.data;
                }
            }
        })
        <%--window.location.href = "${path}/createAdopt/completeView"--%>
    }

    function changeOrder() {
        
    }
</script>

</body>
</html>
