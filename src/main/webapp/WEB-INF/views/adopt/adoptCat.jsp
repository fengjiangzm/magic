<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/9/25
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>领养猫咪展示</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
</head>

<body>

<!-- 导航栏 -->
<jsp:include page="../common/nav.jsp"></jsp:include>

<%--轮播图--%>
<div class="layui-carousel container" id="test1" style="background: none;margin-top: 30px">
    <div carousel-item="">
        <div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/adopt_c1.jpg" alt=""></div>
        <div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/adopt_m1.jpg" alt=""></div>
        <div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/adopt_img1.jpg" alt=""></div>
        <div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/adopt_img2.jpg" alt=""></div>
        <div><img class="carousel-img" src="<%=request.getContextPath()%>/imgs/adopt_img3.jpg" alt=""></div>
    </div>
</div>

<%--领养猫咪展示--%>
<div id="content">


</div>


<!-- 底部 -->
<jsp:include page="../common/bottom.jsp"></jsp:include>

<input type="hidden" value="${path}" id="path">

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/js/adopt/adoptCat.js"></script>

<script>
    $(function(){
        //如果js和jsp在同一个文件中,那么可以在js中使用EL表达式
        //如果js是单独的外部文件,则不允许使用EL表达式
        $("#content").load("${path}/adoptCat/list");
    });
</script>

<script>
    layui.use('carousel', function () {
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1'
            , width: '90%' //设置容器宽度
            , height: '400px'
            , arrow: 'hover' //始终显示箭头
            , anim: 'fade' //切换动画方式
        });
    });
</script>

</body>

</html>
