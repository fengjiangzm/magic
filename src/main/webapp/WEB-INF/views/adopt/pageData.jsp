<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/9/27
  Time: 13:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--按种类查询--%>
<div class='container' style='margin-top:20px;margin-bottom:20px'>
    <div class='row'>
        <div class='col-md-3'>
            <select class='form-control' id='catSpeciesType' onchange='changeType(this)'>

                <option value='-1' selected>===请您选择猫咪类型===</option>

                <c:forEach var="catSpecies" items="${requestScope.catSpeciesList}">
                <c:if test="${catSpecies.id.equals(requestScope.catSpecies_id)}">
                    <option selected value='${catSpecies.id}'>${catSpecies.speciesName}</option>
                </c:if>
                <c:if test="${!catSpecies.id.equals(requestScope.catSpecies_id)}">
                    <option value='${catSpecies.id}'>${catSpecies.speciesName}</option>
                </c:if>
            </c:forEach>

            </select>
        </div>
        <div class='col-md-3'>
            <div class='input-group'>
                <input type='text' class='form-control' id='searchCatName'
                       placeholder="此处可以按猫名查找猫咪" value='${requestScope.catName}'>
                <span class='input-group-btn'>
                    <button class='btn btn-default' type='button' onclick='searchCat()'>查询</button>
                </span>
            </div>
        </div>
    </div>
</div>


<!-- 领养猫咪展示 -->
<div class='container'>
    <div class='row'>

        <c:if test="${requestScope.pageInfo.list!=null && requestScope.pageInfo.list.size()>0}">
            <c:forEach var="c" items="${requestScope.pageInfo.list}">
                <div class='col-md-4'>
                    <div class='thumbnail'>

                        <a href='${path}/catInfo/view?cat_id=${c.catId}'>
                            <img src='${c.imgUrl}' style="height: 300px;width: 500px;" alt='通用的占位符缩略图'>
                        </a>
                        <div class='caption' style='overflow:hidden'>
                            <a href='${path}/catInfo/view?cat_id=${c.catId}'>
                                <h3 align='center' class='text-success'>
                                    <c:forEach items="${requestScope.cats}" var="cat">
                                        <c:if test="${c.catId == cat.id}">
                                            ${cat.name}
                                        </c:if>
                                    </c:forEach>
                                </h3>
                            </a>
                            <p style='text-align:center'>猫咪价格:${c.catPrice}</p>

                            <p style='text-align:center'>
                                猫咪品种:
                                <c:forEach items="${requestScope.catSpeciesList}" var="species">
                                <c:if test="${c.speciesId == species.id}">
                                    ${species.speciesName}
                                </c:if>
                            </c:forEach></p>

                            <p style='text-align:center;'>猫咪介绍:${c.introduction}</p>

                            <c:if test="${requestScope.list.size() <= 0}">
                                <p style='text-align:center'>
                                    <button type="button" class="layui-btn layui-btn-radius layui-btn-primary"
                                            onclick="collect(${c.catId},this)">点击收藏
                                    </button>
                                </p>
                            </c:if>
                            <c:if test="${requestScope.list != null && requestScope.list.size()>0}">
                                <c:if test="${requestScope.list.contains(c.catId) == true}">
                                    <p style='text-align:center'>
                                        <button type="button" class="layui-btn layui-btn-radius layui-btn-primary"
                                                onclick="collect(${c.catId},this)">已收藏
                                        </button>
                                    </p>
                                </c:if>
                                <c:if test="${requestScope.list.contains(c.catId) == false}">
                                    <p style='text-align:center'>
                                        <button type="button" class="layui-btn layui-btn-radius layui-btn-primary"
                                                onclick="collect(${c.catId},this)">点击收藏
                                        </button>
                                    </p>
                                </c:if>
                            </c:if>

                        </div>
                    </div>
                </div>
            </c:forEach>
        </c:if>

    </div>
</div>

<%@ include file="paging.jsp" %>
