<%--
  Created by IntelliJ IDEA.
  User: 陆锦鹏
  Date: 2019/9/25
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>领养订单</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" type="text/css">
    <style>
        .layui-form-label {
            text-align: center;
        }

        #welcome {
            font-size: 30px;
            background: rgba(225, 225, 225, 0.2);
            height: 100px;
            line-height: 100px;
            text-align: center;
        }
    </style>
</head>

<form class="layui-form layui-form-pane" action="${path}/createAdopt/createView?cat_id=${requestScope.cat.id}"
      method="post"
      id="addAddress" hidden style="margin-left: 10px;">
    <div class="layui-form-item">
        <label class="layui-form-label">收件人姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="userName" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">收件人号码</label>
        <div class="layui-input-inline">
            <input type="text" name="userPhone" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input" onblur="checkPhone2(this)">
        </div>
        <div class="layui-form-mid layui-word-aux" id="check_phone2"></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">地址备注</label>
        <div class="layui-input-inline">
            <input type="text" name="name" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">输入省</label>
        <div class="layui-input-inline">
            <input type="text" name="provinceName" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">输入市</label>
        <div class="layui-input-inline">
            <input type="text" name="cityName" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">输入区</label>
        <div class="layui-input-inline">
            <input type="text" name="areaName" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">详细地址</label>
        <div class="layui-input-inline">
            <input type="text" name="detailAddress" required lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <button class="layui-btn" lay-submit="" lay-filter="demo2" id="commit">确认提交</button>
    </div>
</form>

<body>

<!-- 导航栏 -->
<jsp:include page="../common/nav.jsp"></jsp:include>

<!-- 填写领养表 -->
<div class="container">

    <div>
        <h3 id="welcome">请填写领养表哦,然后就能把猫咪带回家了~!</h3>
    </div>
    <form class="layui-form" action="${path}/createAdopt/create?cat_id=${requestScope.cat.id}" method="post">

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>已选择的猫咪</legend>
        </fieldset>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">猫名</label>
                <div class="layui-input-block">
                    <label class="layui-form-label">${requestScope.cat.name}</label>
                    <input type="hidden" name="catId" value="${requestScope.cat.id}">
                </div>
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>填写地址(如果选择到店自取此处可以不用填)</legend>
        </fieldset>
        <div class="layui-form-item">
            <label class="layui-form-label">选择地址</label>
            <div class="layui-input-block" id="content" style="width: 300px">
                <select name="userAddressId" id="address" onchange="selectAddress(this)"
                        lay-filter="event" required lay-verify="required">
                    <option value="-1" selected>====选择地址====</option>
                    <c:forEach var="ua" items="${requestScope.userAddresses}">
                        <c:if test="${ua.status == 0}">
                            <option value='${ua.id}'>${ua.provinceName}${ua.cityName}${ua.areaName}${ua.detailAddress}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
            <div class="layui-input-block">
                <a role="button" id="add" data-method="offset" data-type="auto" class="layui-btn layui-btn-normal"
                   style="margin-top: 10px">添加地址</a>
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>填写姓名</legend>
        </fieldset>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">您的姓名</label>
                <div class="layui-input-block">
                    <input type="text" id="userName" name="userName"
                           class="layui-input" required  lay-verify="required">
                </div>
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>填写手机号</legend>
        </fieldset>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-block">
                    <input type="tel" id="phone" name="phone" lay-verify="required|phone"
                           class="layui-input" onblur="checkPhone(this)" required  lay-verify="required">
                </div>
                <div class="layui-form-mid layui-word-aux" id="check_phone1"></div>
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>填写邮箱</legend>
        </fieldset>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">邮箱</label>
                <div class="layui-input-block">
                    <input type="text" name="userEmail" lay-verify="email"
                           class="layui-input" onblur="checkEmail(this)" required  lay-verify="required">
                </div>
                <div class="layui-form-mid layui-word-aux" id="check_email"></div>
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>工资状况</legend>
        </fieldset>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">工资</label>
                <div class="layui-input-block">
                    <input type="text" name="salary" class="layui-input" required  lay-verify="required">
                </div>
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>填写身份证</legend>
        </fieldset>
        <div class="layui-form-item">
            <label class="layui-form-label">身份证</label>
            <div class="layui-input-block">
                <input type="text" name="identity" lay-verify="identity" placeholder="请输入身份证"
                       class="layui-input" onblur="checkIdentity(this)" required  lay-verify="required">
            </div>
            <div class="layui-form-mid layui-word-aux" id="check_identity"></div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>选择领养的时间</legend>
        </fieldset>
        <div class="layui-form-item">
            <label class="layui-form-label">日期</label>
            <div class="layui-input-block">
                <input type="text" name="date" id="date" lay-verify="date" placeholder="选择日期"
                       class="layui-input" required  lay-verify="required" autocomplete="off">
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>选择性别</legend>
        </fieldset>
        <div class="layui-form-item">
            <label class="layui-form-label" style="text-align: center">性别</label>
            <div class="layui-input-block">
                <input type="radio" name="sex" value="0" title="男" checked="">
                <input type="radio" name="sex" value="1" title="女">
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>选择取猫的方式</legend>
        </fieldset>
        <div class="layui-form-item">
            <label class="layui-form-label">取猫方式</label>
            <div class="layui-input-block">
                <input type="radio" name="takingWay" value="0" title="到店自取" checked="">
                <input type="radio" id="mmm" name="takingWay" value="1" title="店家配送">
            </div>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>亲,可以在这边添加备注哦</legend>
        </fieldset>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">备注区</label>
            <div class="layui-input-block">
                <textarea name="note" placeholder="填写备注" class="layui-textarea"></textarea>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="btn btn-default" lay-submit lay-filter="sub">立即提交</button>
                <button type="reset" class="btn btn-default">重置</button>
            </div>
        </div>

    </form>
</div>

<!-- 底部 -->
<jsp:include page="../common/bottom.jsp"></jsp:include>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script>
    layui.use('laydate', function () {
        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#date'
        });

    });
    layui.use(['form'], function () {
        var form = layui.form;
        form.on('select(event)', function (data) {

            console.log(data.value);

            if (data.value == -1) {
                var userName = document.getElementById("userName");
                var phone = document.getElementById("phone");

                userName.innerHTML = "";
                phone.innerHTML = "";
            } else {
                $.ajax({
                    type: 'get',
                    url: "${path}/createAdopt/selectAddress",
                    data: {
                        addressId: data.value
                    },
                    success: function (result) {
                        if (result.flag) {
                            $("#userName").val(result.data.userName);
                            $("#phone").val(result.data.userPhone);
                        }
                    }
                })
            }
        });

        form.on('submit(sub)', function (data) {

//            alert($("#mmm").prop("checked"));
            var address = document.getElementById("address");

            if ($("#mmm").prop("checked") && address.value <= 0){
                alert("请选择配送地址!");
                return false
            }

            return true;
        });
    });
    layui.use('element', function () {
        var $ = layui.jquery
            , element = layui.element;
    });

</script>

<script>
    layui.use('layer', function () { //独立版的layer无需执行这一句
        var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句

        //触发事件
        var active = {
            offset: function (othis) {
                var type = othis.data('type');

                layer.open({
                    type: 1
                    , offset: type //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
                    , id: 'layerDemo' + type //防止重复弹出
                    , content: $("#addAddress")
                    , btnAlign: 'c' //按钮居中
                    , shade: 0 //不显示遮罩
                });
            }
        };

        $('#add').on('click', function () {
            var othis = $(this), method = othis.data('method');
            active[method] ? active[method].call(this, othis) : '';
        });

    });
</script>

</body>

</html>