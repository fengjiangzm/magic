<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--分页--%>
<div class="container" style="text-align: center">
    <ul class="pagination">
        <li><a href="javascript:goPage(1)">首页</a></li>

        <%--上一页--%>
        <c:if test="${requestScope.pageInfo.pageNum>1}">
            <li><a href="javascript:goPage(${requestScope.pageInfo.pageNum-1})">&laquo;</a></li>
        </c:if>
        <c:if test="${requestScope.pageInfo.pageNum<=1}">
            <li class="disabled"><a>&laquo;</a></li>
        </c:if>

        <!--循环标签显示 - 显示5个
            思路 - 起始值 - 从当前页
        -->
        <%--处理一下最终的页数--%>
        <c:if test="${requestScope.pageInfo.pageNum+4<requestScope.pageInfo.pages}">
            <c:set var="startPage" value="${requestScope.pageInfo.pageNum}"></c:set>
        </c:if>

        <c:if test="${requestScope.pageInfo.pageNum+4>=requestScope.pageInfo.pages}">
            <c:set var="startPage" value="${requestScope.pageInfo.pages-4}"></c:set>
        </c:if>

        <c:forEach begin="${startPage<=0?1:startPage}" end="${startPage+4}" var="n">
            <%--判断当前页是否高亮显示--%>
            <li <c:if test="${requestScope.pageInfo.pageNum == n}">class='active'</c:if>><a
                    href="javascript:goPage(${n})">${n}</a></li>
        </c:forEach>

        <%--下一页--%>
        <c:if test="${requestScope.pageInfo.pageNum<requestScope.pageInfo.pages}">
            <li><a href="javascript:goPage(${requestScope.pageInfo.pageNum+1})">&raquo;</a></li>
        </c:if>
        <c:if test="${requestScope.pageInfo.pageNum>=requestScope.pageInfo.pages}">
            <li class="disabled"><a>&raquo;</a></li>
        </c:if>

        <li><a href="javascript:goPage(${requestScope.pageInfo.pages})">尾页</a></li>
        <li><a>当前页${requestScope.pageInfo.pageNum}/${requestScope.pageInfo.pages}总页</a></li>

        <li>
            <a style="border: 0px;padding-top: 0px;margin-left: 5px">
                <select class="form-control" id="sizeSelect" onchange="changeSize(this)">
                    <option value="2" <c:if test="${requestScope.pageInfo.pageSize == 2}">selected</c:if>>2</option>
                    <option value="4" <c:if test="${requestScope.pageInfo.pageSize == 4}">selected</c:if>>4</option>
                    <option value="6" <c:if test="${requestScope.pageInfo.pageSize == 6}">selected</c:if>>6</option>
                    <option value="8" <c:if test="${requestScope.pageInfo.pageSize == 8}">selected</c:if>>8</option>
                </select>
            </a>
        </li>
    </ul>
</div>

<script>
    //    下一页
    function goPage(pageNum) {

        //继续发送手机类型的数据
        var catSpeciesId = document.getElementById("catSpeciesType").value;
        //获取每页显示的条数
        var pageSize = $("#sizeSelect").val();

        //发送到后台
        $("#content").load("${path}/adoptCat/list", {
            catSpecies_id: catSpeciesId,
            pageNum: pageNum,
            pageSize: pageSize
        });
    }

    //重新选择每页显示的条数
    function changeSize(obj) {

        //继续发送手机类型的数据
        var catSpeciesId = document.getElementById("catSpeciesType").value;

        //发送到后台
        $("#content").load("${path}/adoptCat/list", {catSpecies_id: catSpeciesId, pageNum: 1, pageSize: obj.value});
    }
</script>

