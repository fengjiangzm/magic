<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<!DOCTYPE html>--%>
<%--<html lang="en">--%>
<%--<head>--%>
    <%--<meta charset="UTF-8">--%>
    <%--<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
    <%--<meta http-equiv="X-UA-Compatible" content="ie=edge">--%>
    <%--<title>个人中心侧导航栏</title>--%>
    <%--<link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">--%>
    <%--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">--%>
    <%--<link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">--%>
    <%--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">--%>
    <style>
        .con{
            border-radius: 8px;
            /*background-color: rgb(239, 170, 25);*/
            background-color: #fff;
            box-shadow: 0 3px 18px ;
            /*font-size: 16px;*/
            margin-top:130px;
            height:380px;

            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        /*table{*/
            /*height:400px;*/
        /*}*/

        li{
        height:60px;
        }
    </style>
</head>
<body>

<%--layui-tab layui-tab-brief--%>
<div class="con"  style="padding:10px;width:300px;">
    <ul class=""  style=" padding:20px;">
        <div class="link" style="width:300px;padding:20px;">
            <table class="" lay-size="lg" style=" ">
                 <tr><li>
                        <a href="<%=request.getContextPath()%>/personal/personalSettingList">个人设置&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="layui-icon layui-icon-edit"></i></a>
                    </li></tr>
                <tr><li>
                    <a href="<%=request.getContextPath()%>/personal/personalCenterView">我的消息&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-comment"></span></a>
                </li></tr>

                <tr><li>
                <a href="<%=request.getContextPath()%>/personal/personalCenterMyOrderView"> 我的订单</a>
               </li></tr>
                <tr><li>
                    <a href="<%=request.getContextPath()%>/personal/personalCenterMyAddressView"> 我的地址</a>
                </li></tr>
                <tr><li>
                    <a href="<%=request.getContextPath()%>/personal/collect"> 我的收藏</a>
                </li></tr>

            </table>
        </div>

    </ul>
</div>


<%--<ul class="list-group">--%>
    <%--<li class="layui-this">--%>
        <%--<a href="<%=request.getContextPath()%>/personal/personalSettingList">个人设置</a>--%>

    <%--</li>--%>
    <%--<li class="layui-this" >--%>
        <%--<a href="<%=request.getContextPath()%>/personal/personalCenterView">我的评论&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-comment"></span></a></tr>--%>

    <%--</li>--%>
    <%--<li class="layui-this">--%>
        <%--<a href="<%=request.getContextPath()%>/personal/personalCenterMyOrderView"> 我的订单</a>--%>

    <%--</li>--%>
   <%----%>
<%--</ul>--%>

<%--<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>--%>
<%--<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>--%>
<%--<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>--%>
<%--<script type="text/javascript">--%>
    <%--layui.use('element', function () {--%>
        <%--var $ = layui.jquery--%>
            <%--, element = layui.element;--%>
    <%--});--%>
<%--</script>--%>
</body>
</html>

