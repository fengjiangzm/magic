<%--
  Created by IntelliJ IDEA.
  User: LJX
<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>联系我们</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <style>
        #header {
            /* background-color: rgb(239, 161, 25); */
            background-color: darkgoldenrod;
        }

        #header a {
            color: black;
            font-size: 20px;
        }

        .bigbox {
            margin-top: 50px;
            background-image: url("<%=request.getContextPath()%>/imgs/person-all.jpg");

            background-size: 70% 100%;
            height: 800px;
            background-position: 200px 0px;
            background-repeat: no-repeat;
        }

        .header {
            margin-top: 50px;
        }

        .center {
            margin-top: 20px;
        }

        .centerR {
            margin-left: 700px;
        }

    </style>
</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>
<div class="bigbox">
    <!-- 标题------开始 -->
    <div class="header">
        <div class="container">
            <div class="lf erj">
                <a href="<%=request.getContextPath()%>/contect/contectView" class="ernn"><span style="color:darkgoldenrod; font-size:30px;">联系我们</span></a>
            </div>
            <div class="rf weizhi" style="margin-top:10px;">
                当前位置：<a href="<%=request.getContextPath()%>/index.jsp">首页</a><span> -&gt; </span>
                <a href="<%=request.getContextPath()%>/contect/contextView">联系我们</a>
            </div>
        </div>
    </div>
    <!-- 标题-----结束 -->


    <div class="container">
        <div class="row">
            <div class="center">
                <!-- 地图-----开始 -->
                <div class="centerL">
                    <div class="col-md-6">
                        <div class="mapk" style="float:left;">
                            <iframe src="http://www.doggyhome.com.cn/Public/kindeditor/plugins/baidumap/index.html?center=116.791965%2C40.219812&zoom=18&width=558&height=360&markers=116.791965%2C40.219812&markerStyles=l%2CA"
                                    style="width:560px;height:362px;">
                                <!-- <div class="mapk"><iframe src="https://map.baidu.com/search/%E8%8B%8F%E5%B7%9E%E5%B8%82/@13424432,3649831.25,12z?querytype=s&da_src=shareurl&wd=%E8%8B%8F%E5%B7%9E%E5%B8%82&c=224&src=0&pn=0&sug=0&l=12&b=(13176069.26,3724790.53;13274373.26,3773046.53)&from=webmap&biz_forward=%7B%22scaler%22:2,%22styles%22:%22pl%22%7D&device_ratio=2" frameborder="0" style="width:560px;height:362px;"> -->
                            </iframe>
                            <br/></div>
                    </div>
                </div>
                <!-- 地图-----结束 -->
                <!-- 文本-----开始 -->
                <div class="centerR">
                    <div class="col-md-6">

                        <div class="titleR" style="color: #EE7F10; font-size: 16px; padding-left:20px;">膜蛤猫舍宠物寄养领养服务中心
                        </div>
                        <div class=" codd">
                            <div style="padding-left:20px;"><span style="line-height:2;"><strong><span
                                    style="font-size:14px;">中心地址：</span></strong><span style="font-size:14px;">北京市顺义区木林镇蒋各庄村</span></span><br/>
                                <span style="line-height:2;"> <strong><span
                                        style="font-size:14px;">联系电话：</span></strong><span style="font-size:14px;"> 手机：13801131151孙女士</span></span><br/>
                                <span style="line-height:2;"> <span style="color:#E56600;font-size:14px;">如何</span><span
                                        style="color:#E56600;font-size:14px;">到达</span><span
                                        style="color:#E56600;font-size:14px;">：</span> </span><br/>
                                <span style="line-height:2;"><span style="font-size:14px;"> 一、</span><strong><span
                                        style="font-size:14px;">手机导航：</span></strong><span
                                        style="font-size:14px;"><strong>高德或百度导航，搜“尚谷奥普乐水上乐园”，<br>到达水上乐园门口再向北100米路东即到。</strong></span></span><br/>
                                <br/>
                                <span style="line-height:2;font-size:14px;"> </span><br/></div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- 文本-----结束 -->
        </div>
    </div>

</div>
</div>
</div>


<jsp:include page="../common/bottom.jsp"></jsp:include>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
