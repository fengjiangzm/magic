<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心--个人信息设置</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <style>

        .milder td {
            border: 1px solid lightgrey;
            padding: 10px;
        }

        td {
            width: 100px;
        }


    </style>
</head>
<body>

<jsp:include page="../common/nav.jsp"></jsp:include>


<div class="layui-row">
    <jsp:include page="personalcenter.jsp"></jsp:include>

    <div class="layui-col-md6  layui-col-lg-offset5" style="margin-top:-850px; background-color: white;">


        <div class="layui-tab " lay-filter="docDemoTabBrief" >


            <ul class="layui-tab-title" style="padding-left:20px;">
                <li class="layui-this">全部订单</li>
                <%--<span class="layui-badge">${purchases.size()}</span>--%>
                <li>已付款</li>
                <li>未付款</li>
            </ul>
                 <div class="layui-col-md-offset11">

                <button type="button"class="layui-btn layui-btn-primary layui-btn-xs" onclick='removeAnyLine()'><a>删除选中编号</a></button>

                 </div>
            <br>
            <c:if test="${purchases == null}">暂无订单 ，快去添加吧</c:if>
            <c:if test="${purchases != null}">

                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <%--<blockquote class="" style="background-color: white;">--%>
                            <div class="layui-text">
                                <div class="container" >
                                    <%--<blockquote class="" style="background-color: white;">--%>
                                    <table style="margin-left:30px;">
                                        <tr >
                                            <td>  <input type="checkbox" value="${p.id}" id="cks" onclick="selectAll(this)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全选</td>
                                            <%--<td>猫咪</td>--%>
                                            <td>种类</td>
                                            <td>订单金额</td>
                                            <td>订单详情</td>
                                            <td>交易状态</td>
                                            <td>订单状态</td>
                                            <td>操作</td>

                                        </tr>

                                        <!-- 所有合并的列----开始 -->
                                        <tr>
                                            <td colspan="7" style="height:10px;margin-top:20px;"></td>
                                        </tr>
                                    </table>
                                    <%--</blockquote>--%>


                                    <!-- 列的合并---结束 -->
                                        <%--全部订单--%>
                                    <div class="milder" style="margin-top:30px;">
                                        <c:forEach items="${purchases}" var="p">
                                            <%--<c:if test="${p.status == 1}">暂无订单</c:if>--%>
                                            <c:if test="${p.status == 0}">

                                            <blockquote class="" style="background-color: white;">
                                                         <table>
                                                    <tr>
                                                            <td>
                                                                    <input type="checkbox" value="${p.id}" name="ck">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        <%--<td><img src="http://py5ztkhmu.bkt.clouddn.com/400adda8-a125-4f29-9314-029f26bfa06a.jpg"--%>
                                                                 <%--style="width:50px;height:50px;border-radius: 50%;">--%>
                                                        <%--</td>--%>
                                                        <c:if test="${p.ordertype == 0}"><td>寄养</td></c:if>
                                                        <c:if test="${p.ordertype == 1}"><td>领养</td></c:if>
                                                        <td>${p.ordmoney}$</td>
                                                        <c:if test="${p.payment == 1}">
                                                            <td>
                                                                <a href="<%=request.getContextPath()%>/personal/personalMyOrderDeatilsView?id=${p.id}&username=${user.username}&userId=${user.id}&orderId=${p.id}">订单详情</a>
                                                            </td>
                                                            <td>已付款<br></td>
                                                        </c:if>
                                                        <c:if test="${p.payment==0}">
                                                            <td>订单详情</td>
                                                            <td> <a href="${path}/personal/pay/web?id=${p.id}&amt=${p.ordmoney}&purseId=${p.id}" class="h4" >未付款</a><br>
                                                                <%--<a>订单详情</a>--%>
                                                            </td>
                                                        </c:if>
                                                        <c:if test="${p.ordercancel == 0}">
                                                        <td><a href="${path}/personal/personalCenterMyOrdercancelList?id=${p.id}">取消订单</a></td></c:if>
                                                        <c:if test="${p.ordercancel == 1}">
                                                            <td>已取消</td>
                                                        </c:if>
                                                        <td><a href="${path}/personal/personalMyOrderDel?id=${p.id}">删除订单</a>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </blockquote>
                                            </c:if>
                                        </c:forEach>


                                    </div>
                                </div>
                            </div>

                        <%--</blockquote>--%>
                    </div>
                            <%--已付款--%>
                    <div class="layui-tab-item">
  <%--<blockquote class="layui-elem-quote" style="background-color: white;">--%>
                            <div class="layui-text">
                                <div class="container" style="margin-top:10px;">
                                    <%--<blockquote class="layui-elem-quote" style="background-color: white;">--%>
                                        <table style="margin-left:30px;">
                                        <tr>
                                            <td>  <input type="checkbox" value="${p.id}" name="cks" onclick="selectAll(this)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全选</td>
                                            <%--<td>猫咪</td>--%>
                                            <td>种类</td>
                                            <td>订单金额</td>
                                            <td>订单详情</td>
                                            <td>交易状态</td>
                                            <td>订单状态</td>
                                            <td>操作</td>
                                        </tr>
                                        <!-- 所有合并的列----开始 -->
                                        <tr>
                                            <td colspan="7" style="height:10px;"></td>
                                        </tr>

                                    </table>
                                        <%--</blockquote>--%>
                                    <!-- 列的合并---结束 -->
                                    <div class="milder" style="margin-top:30px;">

                                        <c:forEach items="${purchases}" var="p">
                                            <c:if test="${p.payment == 1}">
                                                <c:if test="${p.status == 0}">
                                                    <blockquote class="" style="background-color: white;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" value="${p.id}" name="ck">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                            <%--<td><img src="http://py5ztkhmu.bkt.clouddn.com/400adda8-a125-4f29-9314-029f26bfa06a.jpg"--%>
                                                                     <%--style="width:50px;height:50px;border-radius: 50%;">--%>
                                                            <%--</td>--%>
                                                            <c:if test="${p.ordertype == 0}"><td>寄养</td></c:if>
                                                            <c:if test="${p.ordertype == 1}"><td>领养</td></c:if>
                                                            <td>${p.ordmoney}</td>
                                                            <td>
                                                                <a href="<%=request.getContextPath()%>/personal/personalMyOrderDeatilsView?id=${p.id}&username=${user.username}&userId=${user.id}&orderId=${p.id}">订单详情</a>
                                                            </td>

                                                            <td>已付款<br> </td>
                                                            <c:if test="${p.ordercancel == 0}">
                                                                <td><a href="${path}/personal/personalCenterMyOrdercancelList?id=${p.id}">取消订单</a></td></c:if>
                                                            <c:if test="${p.ordercancel == 1}">
                                                                <td>已取消</td>
                                                            </c:if>
                                                            <td> <a href="${path}/personal/personalMyOrderDel?id=${p.id}"> 删除订单</a></td>
                                                        </tr>
                                                    </table></blockquote>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>


                                    </div>
                                </div>
                            </div>


                        <%--</blockquote>--%>

                    </div>
                    <%--未付款--%>
                    <div class="layui-tab-item">
                        <%--<blockquote class="layui-elem-quote" style="background-color: white;">--%>
                            <div class="layui-text">
                                <div class="container" style="margin-top:10px;">
                                    <%--<blockquote class="layui-elem-quote" style="background-color: white;">--%>


                                        <table style="margin-left:30px;">
                                        <tr>
                                            <td>  <input type="checkbox" value="${p.id}" name="cks" onclick="selectAll(this)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全选</td>
                                            <%--<td>猫咪</td>--%>
                                            <td>种类</td>
                                            <td>订单金额</td>
                                            <td>订单详情</td>
                                            <td>交易状态</td>
                                            <td>订单状态</td>
                                            <td>操作</td>
                                        </tr>
                                        <!-- 所有合并的列----开始 -->
                                        <tr>
                                            <td colspan="7" style="height:10px;"></td>
                                        </tr>
                                    </table>
                                        <%--</blockquote>--%>
                                    <!-- 列的合并---结束 -->
                                    <div class="milder" style="margin-top:30px;">
                                        <c:forEach items="${purchases}" var="p">
                                            <c:if test="${p.status == 0}">
                                                <c:if test="${p.payment == 0}">
                                                    <blockquote class="" style="background-color: white;">
                                           <table>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" value="${p.id}" name="ck">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                            <%--<td><img src="http://py5ztkhmu.bkt.clouddn.com/400adda8-a125-4f29-9314-029f26bfa06a.jpg"--%>
                                                                     <%--style="width:50px;height:50px;border-radius: 50%;">--%>
                                                            <%--</td>--%>
                                                            <%--<td>小猫咪</td>--%>
                                                            <c:if test="${p.ordertype == 0}"><td>寄养</td></c:if>
                                                            <c:if test="${p.ordertype == 1}"><td>领养</td></c:if>
                                                            <td>${p.ordmoney}</td>
                                                            <td> 订单详情 </td>
                                                            <td> <a href="${path}/personal/pay/web?id=${p.id}&amt=${p.ordmoney}&purseId=${p.id}" class="h4" >未付款</a><br>
                                                                    <%--<a>订单详情</a>--%>
                                                            </td>
                                                            <c:if test="${p.ordercancel == 0}">
                                                                <td><a href="${path}/personal/personalCenterMyOrdercancelList?id=${p.id}">取消订单</a></td></c:if>
                                                            <c:if test="${p.ordercancel == 1}">
                                                                <td>已取消</td>
                                                            </c:if>
                                                            <td>  <a href="${path}/personal/personalMyOrderDel?id=${p.id}">删除订单</a> </td>

                                                        </tr>
                                           </table></blockquote>
                                                </c:if></c:if>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        <%--</blockquote>--%>


                    </div>
                    <%--<jsp:include page="../adopt/paging.jsp"></jsp:include>--%>
                </div>
            </c:if>
        </div>

    </div>
</div>
<div class="layui-row" style="margin-top:50px;">

    <jsp:include page="../common/bottom.jsp"></jsp:include></div>
<script>
    function selectAll(obj) {
        //获取内容行所有的checkbox
        var cks = document.getElementsByName("ck");

        if (obj.checked) {
            //遍历所有的cks
            for (var i = 0; i < cks.length; i++) {
                cks[i].checked = true;
            }
        } else {
            for (var i = 0; i < cks.length; i++) {
                cks[i].checked = false;
            }
        }
    }

    // //删除任意行
    function removeAnyLine() {
        //找出勾选的行
        var cks = document.getElementsByName("ck");

        //定义一个计数器
        var count = 0;

        for (var i = 0; i < cks.length; i++) {
            // 判断checkbox是否勾选
            if (cks[i].checked) {

                count++;
            }
        }

        if (count == 0) {
            alert("请您先勾选!");
            return;
        }

        //说明已经勾选了
        var flag = confirm("确定删除吗?");

        if (flag) {
            var pids = "";

            //下标 - 删除 - 保险操作 - 倒过来.
            for (var i = cks.length - 1; i >= 0; i--) {
                if (cks[i].checked) {

                    pids += "id=" + cks[i].value + "&";
                }
            }
            var url = '<%=request.getContextPath()%>/personal/del?' + pids;
            window.location  = url.substring(0,url.length-1)

        }
    }
</script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    layui.use('element', function () {
        var $ = layui.jquery
            , element = layui.element;
    });
</script>
</body>
</html>
