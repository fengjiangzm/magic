<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心--个人信息设置</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <style>
        input {
            width: 300px;
        }


    </style>
</head>
<body>
<%--${user}--%>
<%--${userInfo}--%>
<%--${userAddresses}--%>
<%--${userAddress}--%>
<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="layui-row">
    <jsp:include page="personalcenter.jsp"></jsp:include>

    <div class="layui-col-md6  layui-col-lg-offset5" style="margin-top:-850px;">

        <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">

            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <ul class="layui-tab-title">
                        <li class="layui-this">收货地址&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li class=""><a class="" data-toggle="modal" data-target="#myModal1" lay-event="edit">
                            添加新的地址
                        </a></li>

                    </ul>
                    <c:forEach items="${userAddresses}" var="u">
                        <c:if test="${u.status == 0}">
                            <%--${path}/personal/personalCenterMyAddressupdate--%>
                            <form id="fff" method="post" name="testname"
                                  action="${path}/personal/personalCenterMyAddressupdate">
                                <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                    <div class="layui-row">
                                        <div class="layui-col-md9 ">
                                            <div class="boder" style="padding-left:10px;">
                                                <c:if test="${u.name == '家'}"> <i class="layui-icon layui-icon-home"
                                                                                  style="font-size:30px;color:rgb(239, 170, 25);"></i></c:if>
                                                <c:if test="${u.name == '公司'}"> <i class="layui-icon layui-icon-read"
                                                                                   style="font-size:30px;color:rgb(239, 170, 25);"></i></c:if>
                                                <c:if test="${u.name == null}"> <i
                                                        class="layui-icon layui-icon-face-smile"
                                                        style="font-size:30px;color:rgb(239, 170, 25);"></i></c:if>
                                                <c:if test="${u.name == '学校'}"> <i class="layui-icon layui-icon-flag"
                                                                                   style="font-size:30px;color:rgb(239, 170, 25);"></i></c:if>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                姓名：&nbsp;&nbsp;&nbsp;&nbsp;${u.userName}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                号码：&nbsp;&nbsp;&nbsp;&nbsp;${u.userPhone}
                                                <%--<span class="layui-badge-dot"></span>--%>
                                                <br><br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地址：&nbsp;&nbsp;&nbsp;&nbsp;${u.provinceName}&nbsp;&nbsp;&nbsp;&nbsp;${u.cityName}&nbsp;&nbsp;${u.areaName}&nbsp;&nbsp;&nbsp;&nbsp;${u.detailAddress}
                                                <%--<span class="layui-badge-dot layui-bg-green"></span>--%>

                                                <c:if test="${u.isdefault  == 0}">
                                                    默认
                                                </c:if>

                                            </div>
                                        </div>
                                        <div class="layui-col-md2 layui-col-md-offset11"
                                             style="margin-left:30px;margin-top:30px;"></div>
                                            <%--href="${path}/personal/personalCenterMyAddressupdate?id=${u.id}"--%>
                                        <a class="layui-btn layui-btn-xs" data-toggle="modal"
                                           data-target="#myModal${u.id}" lay-event="edit">
                                            编辑
                                        </a>
                                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"
                                           href="${path}/personal/personalCenterMyAddressDel?id=${u.id}">删除</a>
                                    </div>
                                </blockquote>
                                    <%--</form>--%>
                                    <%--修改--模态框--%>
                                 <div class="modal fade" id="myModal${u.id}" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <%--${path}/personal/personalCenterMyAddressupdate--%>
                                                <%--<form id="fff2"  name = "testname" method="post" action="${path}/personal/personalCenterMyAddressupdate">--%>

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                    修改收获地址
                                                </h4>
                                            </div>
                                            <div class="modal-body">
                                                <input hidden name="id" value="${u.id}" required="required">
                                                收货人：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="text" name="userName" value="${u.userName}" required="required"><br><br>
                                                手机号：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input pattern="^1[345678][0-9]{9}$"  name="userPhone" value="${u.userPhone}" required="required"><br><br>
                                                    <%--<form id="fff1" name = "testname" method="post" action="${path}/personal/personalCenterMyAddressupdate">--%>
                                                省：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="text" name="provinceName"
                                                       value="${u.provinceName}" required="required"><br><br>
                                                市：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="text" name="cityName" value="${u.cityName}" required="required"><br><br>
                                                区：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="text" name="areaName" value="${u.areaName}" required="required"><br><br>
                                                详细地址：&nbsp;&nbsp;<input type="text" name="detailAddress"
                                                                        value="${u.detailAddress}" required="required"><br><br>
                                                地址备注：&nbsp;&nbsp;<input type="text" name="name" placeholder="家/学校/公司"
                                                                        value="${u.name}" required="required"><br>
                                                    <%--</form>--%>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default"
                                                        data-dismiss="modal">关闭
                                                </button>
                                                <button type="submit" style="width:40px;height:30px;">
                                                    修改
                                                </button>
                                                <c:if test="${u.isdefault  == 0}">
                                                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"
                                                       href="${path}/personal/personalCenterMyAddressisdefault?id=${u.id}">取消默认</a>
                                                </c:if>
                                                <c:if test="${u.isdefault  == 1}">
                                                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"
                                                       href="${path}/personal/personalCenterMyAddressisdefault1?id=${u.id}">默认</a>
                                                </c:if>
                                            </div>
                                                <%--</form>--%>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                            </form>

                        </c:if>
                    </c:forEach>
                    <%--添加收货地址        --%>
                    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form id="fff1" name="testname" method="post"
                                      action="${path}/personal/personalCenterMyAddressSave">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">×
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel1">
                                            添加收获地址
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        收货人：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="userName1"
                                                                                       value="" required="required"><br><br>
                                        手机号：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input pattern="^1[345678][0-9]{9}$"   name="userPhone1"
                                                                                       value="" required="required"><br><br>

                                        <%--<form id="fff1" name = "testname" method="post" action="${path}/personal/personalCenterMyAddressupdate">--%>
                                        省：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                                            type="text" name="provinceName1" value="" required="required"><br><br>
                                        市：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                                            type="text" name="cityName1" value="" required="required"><br><br>
                                        区：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                                            type="text" name="areaName1" value="" required="required"><br><br>
                                        详细地址：&nbsp;&nbsp;<input type="text" name="detailAddress1" value="" required="required"><br><br>
                                        地址备注：&nbsp;&nbsp;<input type="text" name="name1" placeholder="家/学校/公司" required="required"><br>
                                        <%--</form>--%>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal">关闭
                                        </button>
                                        <input type="submit" value="提交更改" style="width:60px;height:30px;"/>

                                    </div>
                                </form>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->


                    <span class="layui-badge-dot layui-bg-orange"></span>
                    <span class="layui-badge-dot layui-bg-green"></span>
                    <span class="layui-badge-dot layui-bg-cyan"></span>
                    <span class="layui-badge-dot layui-bg-blue"></span>
                    <span class="layui-badge-dot layui-bg-black"></span>
                    <span class="layui-badge-dot layui-bg-gray"></span>
                    </blockquote>

                </div>


            </div>
        </div>
    </div>
</div>
<div class="layui-row" style="margin-top:50px;">

    <jsp:include page="../common/bottom.jsp"></jsp:include>
</div>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script>
    layui.use('form', function(){
        var form = layui.form;

    });
</script>

<%--<script type="text/javascript">--%>
<%--var inputs = document.getElementById("fff").getElementsByTagNames("input");--%>
<%--var ls = [];--%>
<%--for(var i=0;i<inputs.length;i++)--%>
<%--{--%>
<%--var type = inputs[i].getAttribute("type")--%>
<%--if(!type||type==""||type=="text")--%>
<%--{--%>
<%--(function(ele){--%>
<%--ls[ls.length] = ele--%>
<%--}(inputs[i]))--%>
<%--}--%>

<%--}--%>


<%--</script>--%>
<%--<script type="text/javascript">--%>
<%--var inputs = document.getElementById("fff1").getElementsByTagNames("input");--%>
<%--var ls = [];--%>
<%--for(var i=0;i<inputs.length;i++)--%>
<%--{--%>
<%--var type = inputs[i].getAttribute("type")--%>
<%--if(!type||type==""||type=="text")--%>
<%--{--%>
<%--(function(ele){--%>
<%--ls[ls.length] = ele--%>
<%--}(inputs[i]))--%>
<%--}--%>

<%--}--%>


<%--</script>--%>


</body>
</html>

