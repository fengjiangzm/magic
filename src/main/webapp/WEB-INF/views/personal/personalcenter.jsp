<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--<!DOCTYPE html>--%>
<%--<html lang="en">--%>
<%--<head>--%>
<%--<meta charset="UTF-8">--%>
<%--<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
<%--<meta http-equiv="X-UA-Compatible" content="ie=edge">--%>
<%--<title>个人中心</title>--%>
<%--<link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">--%>
<%--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">--%>
<%--<link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">--%>
<%--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">--%>
<%--<style>--%>

<%--li{--%>
<%--height:60px;--%>
<%--}--%>

<%--</style>--%>
<%--</head>--%>
<%--<body>--%>


<div class="layui-row" style="margin-top:50px;">

    <div class="layui-col-md3 layui-col-lg-offset1">
        <%--<div id="imgPreview">--%>
        <%--<div id="prompt3">--%>
        <%--<span id="imgSpan">--%>
        <%--点击上传--%>
        <%--<br />--%>
        <%--<i class="aui-iconfont aui-icon-plus"></i>--%>
        <%--<!--AUI框架中的图标-->--%>
        <%--</span>--%>
        <%--<input type="file" id="file" class="filepath" onchange="changepic(this)" accept="image/jpg,image/jpeg,image/png,image/PNG">--%>
        <%--<!--当vaule值改变时执行changepic函数，规定上传的文件只能是图片-->--%>
        <%--</div>--%>
        <%--<img src="<%=request.getContextPath()%>/imgs/personal-head.gif" class="img-circle" style="border-radius:50%; width:200px;height:200px;" id="pic" >--%>
        <%--</div>--%>
        <c:if test="${userInfo.avatar == null} ">
            <img src="<%=request.getContextPath()%>/imgs/personal-head.gif" class="img-circle"
                 style="border-radius:50%; width:200px;height:200px;" id="pic">
        </c:if>
        <c:if test="${userInfo.avatar != null}">
            <img src="${userInfo.avatar}" class="img-circle" style="border-radius:50%; width:200px;height:200px;"
                 id="pic">
        </c:if>


        <br>
        <br>
        <h3></h3><span style="color:black;padding-left: 50px;font-size: 20px;">${user.username}</span></h3><br>
        <%--<iframe scrolling="no" src="https://tianqiapi.com/api.php?style=tt&skin=pitaya" frameborder="0" width="590" height="98" allowtransparency="true"></iframe>--%>
        <div class="tt" style="margin-top:30px;margin-left:-10px;">
            <i class="layui-icon layui-icon-note"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;每日好心情从这里开始
            <%--<iframe scrolling="no" src="https://tianqiapi.com/api.php?style=tt&skin=pitaya" frameborder="0" width="590" height="98" allowtransparency="true"></iframe>--%>
            <%--<div class="pull-right pull-none-xs text-center" style="margin-top:30px;">--%>
                <%--<div class="col-md-6">--%>
                    <%--<a href="<%=request.getContextPath()%>/personal/collect" class="m-b-md inline m">--%>
                        <%--<span class="h3 block font-bold" id="guan">${cats.size()}</span>--%>
                        <%--<br>--%>
                        <%--<small>MyCollects</small>--%>
                    <%--</a></div>--%>

            <%--</div>--%>

            <jsp:include page="personalSideNav.jsp"></jsp:include>
        </div>
    </div>


</div>

