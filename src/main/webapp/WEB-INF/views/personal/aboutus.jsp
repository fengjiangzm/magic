<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/25
  Time: 18:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>关于我们</title>

    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">

    <style>


        .bigbox {
            margin-top: 50px;
            margin: auto;
        }

        .level {
            padding-top: 50px;
            /* background-color:rgba(255,255,255,0.2); */
        }

        .foot {
            padding-top: 50px;

        }


        .header {
            margin-top: 50px;
        }

        .center {
            margin-top: 50px;
        }

        /* 导航栏 */
    </style>

</head>
<body>

<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="bigbox">
    <!-- 标题------开始 -->
    <div class="header">
        <div class="container">
            <div class="lf erj">
                <a href="<%=request.getContextPath()%>/aboutus/aboutusView" class="ernn"><span
                        style="color:rgb(239, 161, 25); font-size:30px;">关于我们</span></a>
            </div>
            <div class="rf weizhi" style="margin-top:10px;">
                当前位置：
                <%--<a href="<%=request.getContextPath()%>/index.jsp">--%>
                首页
                    <%--</a>--%>
                <span> -&gt; </span>
                <a href="<%=request.getContextPath()%>/aboutus/aboutusView">关于我们</a>
            </div>
        </div>
    </div>
    <!-- 标题-----结束 -->

    <div class="center">
        <div class="container">
            <div class="row">
                <!-- 上半部分  左面图片----开始 -->
                <div class="col-md-3">
                    <img src="http://py5ztkhmu.bkt.clouddn.com/d3974eb8-1726-4477-8cb6-3030291bab13.jpg"
                         style="height:250px; border-radius: 50%;">
                </div>
                <!-- 图片----结束 -->
                <!-- 上半部分  右面文字----开始 -->
                <div class="col-md-9">
                    <div class="hright" style="margin-left:100px; padding-top:30px;">
                        <div class="yell"><h4>膜蛤猫舍宠物寄养领养服务中心</h4></div>
                        <div class="abjs">
                            <p style="text-indent: 2em;"><span
                                    style="color: rgb(58, 58, 58); font-family: 微软雅黑; font-size: 12px; line-height: 30px; background-color: rgb(255, 255, 255); "><span><span
                                    style="font-size:14px;">中心成立于2005年，为京城成立最早的大型宠物寄养训练专业企业。</span><br/>
            <span style="font-size:14px;"> 由公安、畜牧和环保等部门验收合格并颁发证照的正规宠物寄养服务机构。</span><br/>
            <span style="font-size:14px;"> 我中心拥有近2000平米的室内饲养空间150间超大宠舍，及6000多平米的室外活动空间，环境优雅、设施齐备。</span><br/>
            <span style="font-size:14px;"> 特别安装全方位视频监控系统，可对寄养宠物的日常生活进行实时监控。</span><br/>
            <span style="font-size:14px;"><span
                    style="color:rgb(239, 161, 25);"> 其它配套设施有：猫舍、宠物医疗室、美容室、洗浴室、育幼室、隔离室等专业设施</span></span></span></span></p></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 文字----结束 -->

    </div>
</div>
<!-- 水平线--开始 -->
<div class="level">
    <hr>
</div>
<!-- 水平线----结束 -->
</div>
<div class="foot">
    <div class="container">
        <div class="row">
            <!-- 下半部分  左面文字-----开始 -->
            <div class="col-md-9">
                <div class="footerL">
                    <div class="yell">安心 舒心 放心</div>
                    <div class="abjs" style="padding-right:100px;">
                        <!-- <p> -->
                        <span style="color: rgb(58, 58, 58); font-family: 微软雅黑; font-size: 12px; line-height: 30px;
                                         background-color: rgb(255, 255, 255);">
                                            <span style="font-size:14px;">每间（猫）舍包括室内和外院，其中（猫）舍室内面积10—15平米/间，房内高2.7米，外院面积5—9平米/间，设有防晒网。 </span><br/>
                    <span style="font-size:14px;"> 宠舍面积堪称京城之最。宠舍为砖结构复合彩钢顶结构，集中供暖，每间配有落地塑钢玻璃门，保温采光，冬暖夏凉，犬舍内配有专用犬床，上下水，通风换气设备等，是名副其实的星级宠物宾馆。我中心还专门开设了4间豪华犬舍，超大面积，超大活动空间，专人喂养。 <br/>
                    为了保证您的宠物在寄养时的身体健康，由资深专业的兽医巡诊，保证寄养宠物的健康。并提供一流的宠物医疗服务，承接各种动物手术、术后住院护理、康复疗养服务。</span><br/>
                    <span style="font-size:14px;">我中心饲养人员均经过严格挑选，并经过系统的岗前培训。<br/>
                         </span><span style="font-size:14px;"><span
                                style="rgb(239, 161, 25);">本着爱心、诚心、细心的企业服务宗旨，竭诚为您服务</span>，为您的爱宠提供像家一样的温馨舒适环境，解决您的后顾之忧。</span></span></p>
                    </div>


                </div>
            </div>
            <%--</div>--%>

            <!-- 左面文字---结束 -->
            <div class="col-md-3">
                <img src="http://py5ztkhmu.bkt.clouddn.com/d3974eb8-1726-4477-8cb6-3030291bab13.jpg"
                     style="height:250px; float:left; border-radius: 50%;">
            </div>

        </div>

    </div>
</div>


</div>

<jsp:include page="../common/bottom.jsp"></jsp:include>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
