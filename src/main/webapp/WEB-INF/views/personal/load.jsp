<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 20:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
<style>
    .registe{
        background-image: url("<%=request.getContextPath()%>/imgs/person-all.jpg");

        background-size: 70%  100%;
        height:650px;
        background-position:  200px 0px;
        background-repeat: no-repeat;

    }
    .container{
        width:400px;
        margin-top:100px;
        padding-top:50px;
        margin-left:550px;
  }

    .link a{
        text-decoration: none;
        color:white;
        margin-left:300px;
    }

    button{
        border:1px solid lightgray;
        border-radius:25px 25px 25px 25px

    }
    input{
        background-color:rgba(207, 203, 203,0.2);
    }
</style>
</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>
<div class="registe" >
    <div class="container">

        <form action="<%=request.getContextPath()%>/user/loadList" method='post'>
        <div class="row">
            <!-- 文字  账号登录 -->
            <div class="header" style="float:left; margin-left:60px">
                <h3 > <font color=""> 账号登录</font></h3></div>

            <div class="header" style="padding-top:70px;width:400px;">
                <hr color="">
            </div>

            <!-- 手机号  表单 -->
                <input type="text" name="username" placeholder="用户名" style="margin-top:20px; margin-left:30px;width:330px; height:45px;">
                <br>
            <c:if test="${param.error==0}">
                    <span class='text-danger' style="margin-left:270px;">用户不存在!</span>
            </c:if>
                <br>
                <input type="password" name="password" placeholder="请输入密码" style="margin-top:20px; margin-left:30px;width:330px; height:45px;">
                <br>
            <c:if test="${param.error==1}">
                    <span class='text-danger'style="margin-left:270px;">密码错误!</span>
            </c:if>
                <br>
            <a href="forgetpassword.html" style="margin-top:30px; margin-left:300px; "> <font color="lightsalmon">忘记密码</font> </a>

        <br>
            <!-- 登录 按钮 -->
                <button type="submit" class="layui-btn layui-btn-warm layui-btn-radius" style=" height:40px; width:300px; margin-left:40px; margin-top:30px;">登录</button>
      <div class="footer" style="margin-top:30px;margin-left:50px;">
                <a href=""><span id="qqLoginBtn"><img src="<%=request.getContextPath()%>/imgs/personal-qq.jpg" style="width:40px;height:40px;border-radius: 50%;"></span></a>

                <a href=""><img src="<%=request.getContextPath()%>/imgs/personal-weixin.jpg" style="width:40px;height:40px;border-radius: 50%;margin-left:60px;"></a>

                <a href><img src="<%=request.getContextPath()%>/imgs/personal-weibo.jpeg" style="width:40px;height:40px;border-radius: 50%;margin-left:60px;"></a>

            </div>

        </div>
        </form>

    </div>
</div>
<jsp:include page="../common/bottom.jsp"></jsp:include>

<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
