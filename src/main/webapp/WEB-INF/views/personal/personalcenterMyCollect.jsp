<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心--我的收藏</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <style>
        input{
            width:300px;
        }


    </style>
</head>
<body>

<jsp:include page="../common/nav.jsp"></jsp:include>


<div class="layui-row">
    <jsp:include page="personalcenter.jsp"></jsp:include>

    <div class="layui-col-md6  layui-col-lg-offset5" style="margin-top:-840px;">

        <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">

            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <ul class="layui-tab-title">
                        <li class="layui-this" >我的收藏</li>
                        <li>    <button type="button"class="layui-btn layui-btn-primary layui-btn-xs" onclick='removeAny()'><a>取消收藏</a></button></li>


                    </ul>
                </div>
            </div>


    <%--<c:forEach items="${cat}" var="a">--%>
        <%--<c:forEach items="${collects}" var="c">--%>
           <c:forEach items="${cats}" var="c">
           <c:if test="${c.status == 0}">
       <div class="layui-row" style="margin-top:40px;">
           <div class="milder">
               <div class="row">
                   <div class="milderL">
                       <div class="col-md-6">
                           <div class="order" style = "width:700px;height:400px;border: 0.7px solid black;">
                               <input type="checkbox" value="${c.cat.id}" name="ck" style="margin-left:-130px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               <div class="orderimg" style="border:40px solid white;">

                                       <a href="<%=request.getContextPath()%>/catInfo/view?cat_id=${c.cat.id}">

                                   <img src="${c.imgUrl}" style="height:200px;width:400px;"> </a></div>


                           </div>
                       </div>
                   </div>
                   <div class="milderR">
                       <div class="col-md-6"
                            style="border:0.3px solid gray; background-color:white;  padding:15px;height:200px;margin-top:170px">
                           <strong class="daily-family-name"><h3>${c.cat.speciesName}</h3></strong>

                           <h2>@膜蛤猫舍</h2>
                      <%--<c:forEach items="${adoptCats}" var="d">--%>
                                <div class="daily-family-content">
                                    <%--添加时间：&nbsp;&nbsp;&nbsp;&nbsp;${c.addTime}<br>--%>
                                        添加时间：&nbsp;&nbsp;&nbsp;&nbsp;${time}<br>
                                    单价&nbsp;&nbsp;&nbsp;&nbsp;：&nbsp;${c.catPrice}<br>


                                    详细信息：&nbsp;${c.introduction}<br>

                                </div>
                       <%--</c:forEach>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </c:if>
        </c:forEach>
        <br>
        <br>
        <br>
    </div>
</div>

<div class="layui-row" style="margin-top:50px;">
    <jsp:include page="../common/bottom.jsp"></jsp:include></div>
<script>

    function removeAny() {
        //找出勾选的行
        var cks = document.getElementsByName("ck");

        //定义一个计数器
        var count = 0;

        for (var i = 0; i < cks.length; i++) {
            // 判断checkbox是否勾选
            if (cks[i].checked) {

                count++;
            }
        }

        if (count == 0) {
            alert("请您先勾选!");
            return;
        }

        //说明已经勾选了
        var flag = confirm("确定删除吗?");

        if (flag) {
            var pids = "";

            //下标 - 删除 - 保险操作 - 倒过来.
            for (var i = cks.length - 1; i >= 0; i--) {
                if (cks[i].checked) {

                    pids += "id=" + cks[i].value + "&";
                }
            }
            var url = '<%=request.getContextPath()%>/personal/collectdel?' + pids;
            window.location  = url.substring(0,url.length-1)

        }
    }
</script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>


</body>
</html>
