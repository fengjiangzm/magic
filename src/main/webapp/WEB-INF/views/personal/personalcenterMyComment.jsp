<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心----我的消息</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <style>


    </style>
</head>
<body>
<%--${orderComments}--%>
<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="layui-row">
    <jsp:include page="personalcenter.jsp"></jsp:include>

<div class="layui-col-md6  layui-col-lg-offset5" style="margin-top:-850px;">

    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">

        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">

                <ul class="layui-tab-title">
                    <li class="layui-this">我的消息</li>

                </ul>
                <%--<jsp:include page="../foster/commentTemplate.jsp"></jsp:include>--%>
                <c:forEach items="${replays}" var="r">

                <blockquote class="" style="background-color: white;">
                    <div class="layui-text">
                        <div class="streamline b-l b-info m-l-lg m-b padder-v" style="padding:10px;">
                            <div>
                                <a class="pull-left thumb-sm avatar m-l-n-md" style="padding-left:8px;">
                                    <%--<img src="<%=request.getContextPath()%>/imgs/24.jpg" style="width:30px;height:30px;border-radius: 50%;" class="img-circle" alt="..." style="border-radius: 50%;">--%>
                                </a>
                                <div class="m-l-lg">
                                    <div class="m-b-xs" style="padding:10px;">
                                        <%--<a href class="h4" style="padding:10px;">${userInfo.nickname}</a>--%>
                                            <a href="${path}/article/replayView?id=${r.id}" class="h4" style="padding:10px;" >${r.fromName}</a>
                                        <span class="text-muted m-l-sm pull-right">
                                                <%--${r.data}--%>

                                            <%--@${r.toName}<a onclick="replay(this,${r.commentId},${r.fromId},'${r.fromName}')"--%>
                                                           <%--class="layui-layout-right reply_btn" role="button"--%>
                                                           <%--style="margin-right: 20px">回复</a>--%>
                                                    <%--${r.commentId} <br>${r.fromId}--%>
                                             <%--<a onclick="replay(this,${replay.commentId},${replay.fromId},'${replay.fromName}')"--%>
                                                <%--class="layui-layout-right reply_btn" role="button"--%>
                                                <%--style="margin-right: 20px">回复</a>--%>
                                                          <%--</span>--%>
                                    </div>
                                    <br>
                                    <div class="m-b">
                                        <div>${r.content}</div>
                                        <br>
                                        <%--<div class="m-t-sm">--%>
                                            <%--<span class="glyphicon glyphicon-share-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                            <%--<span class="glyphicon glyphicon-star"></span>&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                            <%--<span class="glyphicon glyphicon-refresh"></span>&nbsp;13--%>
                                        <%--</div>--%>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </blockquote>

                </c:forEach>

            </div>
        </div>
    </div>
</div>
</div>

<div class="layui-row" style="margin-top:50px;">

    <jsp:include page="../common/bottom.jsp"></jsp:include></div>



<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<%--<script type="text/javascript">--%>
    <%--function replay(obj, commentId, toId, toName) {--%>
        <%--$(".reply_textarea").remove();--%>
        <%--$(obj).parent().next().append("<form class='layui-form reply_textarea' action='${path}/comment/addReplay'>\n" +--%>
            <%--"                <div class='layui-form-item layui-form-text'>\n" +--%>
            <%--"                    <input type='text' hidden name='commentId' value='" + commentId + "'>\n" +--%>
            <%--"                    <input type='text' hidden name='toId' value='" + toId + "'>\n" +--%>
            <%--"                    <input type='text' hidden name='toName' value='" + toName + "'>\n" +--%>
            <%--"                    <textarea name='content' placeholder='请输入内容' class='layui-textarea'></textarea>\n" +--%>
            <%--"                    <button type='submit' class='layui-btn' style='margin-top: 10px'>\n" +--%>
            <%--"                        <i class='layui-icon'>&#xe608;</i> 回复\n" +--%>
            <%--"                    </button>\n" +--%>
            <%--"                </div>\n" +--%>
            <%--"            </form>");--%>
    <%--}--%>
<%--</script>--%>
</body>
</html>
