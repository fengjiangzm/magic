<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心--个人信息设置</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <%--<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>--%>
    <%--<script src=" https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>--%>
    <%--<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>--%>
    <style>
        input {
            width: 300px;
        }


    </style>
</head>
<body>
<jsp:include page="../common/nav.jsp"></jsp:include>

<div class="layui-row">
    <jsp:include page="personalcenter.jsp"></jsp:include>

    <div class="layui-col-md7  layui-col-lg-offset5" style="margin-top:-850px;">

        <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">

            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <ul class="layui-tab-title">
                        <li class="layui-this" onclick="update()">完善信息</li>
                    <li class=""><a class="" data-toggle="modal" data-target="#myModal1" lay-event="edit">
                        更换头像
                    </a></li>


                    </ul>


                    <c:if test="${userInfo != null}">
                        <form method="post" id="fff" name="testname" action="${path}/personal/personalSettingListUpdate">


                            <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                &nbsp;&nbsp;姓名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${userInfo.nickname}"
                                                                             name="nickname">
                                <span class="layui-badge-dot"></span></blockquote>
                            <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                &nbsp;&nbsp;邮箱&nbsp;&nbsp;&nbsp;&nbsp;<input type="email"  value="${userInfo.useremail}"
                                                                             name="useremail">
                                <span class="layui-badge-dot layui-bg-orange"></span></blockquote>
                            <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                &nbsp;&nbsp;地址&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="${userInfo.address}"
                                                                             name="address">
                                <span class="layui-badge-dot layui-bg-green"></span></blockquote>
                            <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                &nbsp;&nbsp;号码&nbsp;&nbsp;&nbsp;&nbsp;<input pattern="^1[345678][0-9]{9}$" value="${userInfo.phone}"
                                                                             name="phone">
                                <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                            <c:if test="${userInfo.gender == 0}">
                                <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                    &nbsp;&nbsp;性别&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="女" name="gender">
                                    <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                            </c:if>
                            <c:if test="${userInfo.gender == 1}">
                                <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                    &nbsp;&nbsp;性别&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="男" name="gender">
                                    <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                            </c:if>
                            <c:if test="${userInfo.gender == null}">
                                <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                                    &nbsp;&nbsp;性别&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="" name="gender">
                                    <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                            </c:if>
                            <blockquote class="layui-elem-quote layui-quote-nm" style="">
                                    <%--生日：<input type="text" value="${userInfo.birthday}" name="birthday">--%>
                                <div class="layui-inline" style="">
                                    <label class="layui-form-label">生日</label>
                                    <div class="layui-input-inline">
                                        <input type="text" value="${birthday}" name="birthday"
                                               class="layui-input" id="test1" placeholder="yyyy-MM-dd"
                                               style="width:300px;height:30px;">
                                    </div>
                                </div>
                                <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                            <input type="submit" value="提交"/>
                            </blockquote >


                        </form>
                    </c:if>
                    <c:if test="${userInfo == null}">
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        姓名：<input type="text">
                        <span class="layui-badge-dot"></span></blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        邮箱：<input type="text">
                        <span class="layui-badge-dot layui-bg-orange"></span></blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        地址：<input type="text">
                        <span class="layui-badge-dot layui-bg-green"></span></blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        号码：<input type="text">
                        <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        性别：<input type="text">
                        <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                    <blockquote class="layui-elem-quote layui-quote-nm" style="padding:30px;">
                        生日：<input type="text">
                        <span class="layui-badge-dot layui-bg-cyan"></span></blockquote>
                    <blockquote>
                        </c:if>
                        <span class="layui-badge-dot layui-bg-orange"></span>
                        <span class="layui-badge-dot layui-bg-green"></span>
                        <span class="layui-badge-dot layui-bg-cyan"></span>
                        <span class="layui-badge-dot layui-bg-blue"></span>
                        <span class="layui-badge-dot layui-bg-black"></span>
                        <span class="layui-badge-dot layui-bg-gray"></span>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <%--<form id="fff1" name = "testname" method="post" action="${path}/personal/imgupload">--%>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×
                </button>
                <h4 class="modal-title" id="myModalLabel1">
                    更换头像
                </h4>
            </div>
            <div class="modal-body">

                <form method="post" enctype="multipart/form-data" id="imgform">
                    <input type="file" name="myfile" id="myfile">

                </form>

                <div id="preview">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal" onclick="send()">关闭
                </button>
                <%--<input type="submit" value="提交更改" style="width:60px;height:30px;" />--%>

            </div>
            <%--</form>--%>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="layui-row" style="margin-top:50px;">

    <jsp:include page="../common/bottom.jsp"></jsp:include>
</div>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src=" https://cdn.bootcss.com/jquery.form/4.2.2/jquery.form.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajaxfileupload.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>


<script>
    layui.use('laydate', function () {
        var laydate = layui.laydate;

        //常规用法
        laydate.render({
            elem: '#test1'
        });
    });
</script>
<script type="text/javascript" language="JavaScript">

    $('#myfile').change(function () {
        var er = " ";
        $("#imgform").ajaxSubmit({
            url: "${path}/per/imgupload",

            method: 'POST',
            datType: 'json',

            success: function (data) {
                console.log(data.imgUrl);
                er = data.imgUrl;

                console.log(typeof data.imgUrl);
                var ele = "<img src=\"\"/>"
                $("#preview").append(ele);//编程一个jquery的对
                // 象
                $("#preview").find("img").attr("src", data.imgUrl);//调用jquery的方法找到
//                        图片的标签的名称，再设置里面的src的属性的值

                window.location = '/magic/personal/img?avatar=' + er;
            },
            error: function (data) {
                alert(data);
            }
        })
        <%--$("#myfile").load("${path}/personal/imgupload",{avatar:er});--%>
        <%--window.location='${path}/personal/imgupload?avatar='+er;--%>
    })

</script>

<%--</script>--%>
<%--<script>--%>
<%--function changepic() {--%>
<%--$("#prompt3").css("display", "none");--%>
<%--var reads = new FileReader();--%>
<%--f = document.getElementById('file').files[0];--%>
<%--reads.readAsDataURL(f);--%>
<%--reads.onload = function(e) {--%>
<%--document.getElementById('pic').src = this.result;--%>
<%--$("#pic").css("display", "block");--%>
<%--};--%>
<%--}--%>
<%--</script>--%>


</body>
</html>
