<%--
  Created by IntelliJ IDEA.
  User: success
  Date: 2019/9/12 0012
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.net.URLDecoder" %>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>用户登录</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">

    <style>
        .passport-pageMainCnt {
            width: 100%;
            height: 650px;
            position: relative;
            clear: both;
            background-repeat: no-repeat;
            background-position: center top;
        }

        .w {
            max-width: 1190px;
            min-width: 1190px;
            margin: 0 auto;
        }

        .pr {
            position: relative;
        }

        .login-page-title {
            padding: 10px;
            position: absolute;
            z-index: 1;
            top: 260px;
            left: 20px;
            color: #fff;
        }

        .login-page-title h1 {
            font-size: 40px;
        }

        .login-page-title h2 {
            font-size: 20px;
            line-height: 60px;
        }

        .loginBlock {
            width: 400px;
            z-index: 0;
            top: 40px;
            padding: 40px 30px;
            border-radius: 8px;
            border: 1px solid #e2e2e2;
            background: #fff;
            margin-top: 150px;
        }

        .base-mr10 {
            margin-right: 10px;
        }

        .xs-Element {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
        }

        .fr {
            float: right;
            display: inline;
        }

        .wel {
            font-size: 20px;
            float: left;
        }

        .go-login {
            color: #00a2d4;
            float: right;
        }

        .error-tip {
            color: red;
            margin-left: 10px;
            font-size: 12px;
        }

        .fl {
            float: left;
            display: inline;
        }

        .login-page-a {
            background: #f9f9f9;
            color: #333;
            font-size: 14px;
            line-height: 40px;
            width: 50%;
            text-align: center;
            cursor: pointer;
        }

        .ipt {
            position: relative;
        }

        .ipt-t input {
            border-radius: 10px;
        }

        .ipt i {
            position: absolute;
            top: 10px;
            left: 15px;
        }

        .ipt input {
            text-indent: 30px;
            border-radius: 10px;
        }

        .layui-inline {
            width: 100%;

        }


        .d-code {
            width: 230px;
            float: left;
        }

        .get-imgcode {
            width: 100px;
            height: 40px;
            border-radius: 10px;
            float: right;
        }

        .get-imgcode img {
            width: 100px;
            height: 40px;
            border-radius: 10px;

        }

        .get-code {
            width: 100px;
            border-radius: 10px;
            background-color: #cfcfcf;
            color: #fff;
            float: right;
        }


        .register-btn {
            border-radius: 10px;
            /* background-color: #cfcfcf; */
            background-color: #008ff4;
            width: 100%;
        }

        .agreement {
            font-size: 12px;
            vertical-align: bottom;
            color: #008ff4;
        }
    </style>
</head>
<body style="background-color: turquoise">
<%
    //获取cookie
    //每次请求的时候,会将所有的cookie全部发送给server
    Cookie[] cookies = request.getCookies();

    String uname="";
    String pwd="";
    //定义一个标记位
    boolean flag = false;

    if(null!=cookies && cookies.length>0){
        for(Cookie c:cookies){
            //找到name=username的cookie
            if("userLogin".equals(c.getName())){
                String value = c.getValue();

                if(value.trim().length()==0){
                    break;
                }
                // System.out.println(value.split(":")[0]);

                //如果对用户名进行二级编码,那么此处需要进行二级解码
                uname = value.split(":")[0];

                uname = URLDecoder.decode(URLDecoder.decode(uname,"utf-8"),"utf-8");

                pwd = value.split(":")[1];

                flag = true;
                break;
            }
        }
    }

    pageContext.setAttribute("uname",uname);
    pageContext.setAttribute("pwd",pwd);
    pageContext.setAttribute("flag",flag);


%>

<jsp:include page="../common/nav.jsp"></jsp:include>


<div class="passport-pageMainCnt" >
    <%--    background-image:url('<%=request.getContextPath()%>/imgs/person-all.jpg'--%>
    <div class="w clearFix pr">
        <div class="login-page-title">
            <h1>值得信任的寄养领养平台</h1>
            <h2>环境优美&nbsp;&nbsp;值得信赖&nbsp;&nbsp;是您的不二选择</h2>
        </div>
        <div class="fr  loginBlock base-mr10 xs-Element">
            <%--<input type="hidden" name="key" th:value="${key}">--%>
            <form class="layui-form" action="<%=request.getContextPath()%>/user/login" method='post'>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <span class="wel">登录</span>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <div class="ipt">
                            <i class="layui-icon layui-icon-username"></i>
                            <input type="text" id="ustring" value='${uname}'  name="username" placeholder="请输入您的用户名"
                                   autocomplete="off" class="layui-input">
                            <span class='glyphicon glyphicon-user form-control-feedback'></span>
                            <span id="uname_span"></span>
                        </div>
                        <c:if test="${param.error==0}">
                            <span class="error-tip">用户不存在!</span>
                        </c:if>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <div class="ipt">
                            <i class="layui-icon layui-icon-password"></i>
                            <input type="password" id="pstring" value='${pwd}' name="password"  lay-verify="pass" placeholder="请输入登录密码"
                                   autocomplete="off" class="layui-input">
                            <span class='form-control-feedback glyphicon glyphicon-lock'></span>
                        </div>
                        <c:if test="${param.error==1}">
                            <span class='error-tip'>密码错误!</span>
                        </c:if>
                    </div>
                </div>
                <div class="layui-form-item" style="margin-right: 40px">
                    <div class="c-box">
                        <c:if test="${flag}">
                            <input type="checkbox" name='flag' lay-skin="primary" title="记住密码" checked >
                        </c:if>
                        <c:if test="${!flag}">
                            <input type="checkbox" name='flag' lay-skin="primary" title="记住密码">
                        </c:if>
                    </div>
                </div>
                <div class="layui-form-item">
                    <button type="submit" id="login-btn" class="layui-btn register-btn"   lay-submit>登录</button>
                </div>

            </form>
        </div>
    </div>
</div>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script>

    layui.use('form',function () {
        var form = layui.form;
    })
</script>


<%--<script>--%>
    <%--//获取public key--%>
    <%--var publicKey = null;--%>
    <%--function getPublicKey(dologin){--%>
        <%--$.ajax({--%>
            <%--url: "/login/Rdspwd",--%>
            <%--type: "post",--%>
            <%--dataType: "text",--%>
            <%--success: function(data) {--%>
                <%--if(data) publicKey = data;--%>
                <%--if(dologin==1){--%>
                    <%--if(publicKey==null){--%>
                        <%--$("#msg").html("获取publicKey失败，请联系管理员！");--%>
                        <%--$("#login-btn").removeAttr("disabled");--%>
                    <%--}else{--%>
                        <%--doLogin(1);--%>
                    <%--}--%>
                <%--}--%>
            <%--}--%>
        <%--});--%>
    <%--}--%>

    <%--var ustring = $.trim($("#ustring").val());--%>
    <%--var pstring = $.trim($("#pstring").val());--%>
    <%--var encrypt = new JSEncrypt();--%>
    <%--if(publicKey != null){--%>
        <%--encrypt.setPublicKey(publicKey);--%>
        <%--var password = encrypt.encrypt(pstring);--%>
        <%--var username = encrypt.encrypt(ustring);--%>
        <%--//提交之前，检查是否已经加密。假设用户的密码不超过20位，加密后的密码不小于20位--%>
        <%--if(password.length < 20) {--%>
            <%--//加密失败提示--%>
            <%--alert("登录失败，请稍后重试...");--%>
        <%--}else{--%>
            <%--$.ajax({--%>
                <%--url: "${contextPath}/dologin",--%>
                <%--type: "post",--%>
                <%--data: {"usname": username,"pwd": password,"vcstring": vcstring},--%>
                <%--dataType: "json",--%>
            <%--})--%>
        <%--}--%>
    <%--}--%>



<%--</script>--%>
</body>
    </html>