<%--
  Created by IntelliJ IDEA.
  User: LJX
  Date: 2019/9/26
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心--详细订单</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/indexCss.css" media="all">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adopt/adoptCatCss.css" type="text/css">
    <style>
        input{
            width:300px;
        }


    </style>
</head>
<body>
<%--${foster}--%>
<%--${adoptApplication}--%>
<jsp:include page="../common/nav.jsp"></jsp:include>


<div class="layui-row">
    <jsp:include page="personalcenter.jsp"></jsp:include>

        <div class="layui-col-md6  layui-col-lg-offset5" style="margin-top:-800px;">




            <div class="layui-row">
                <div class="milder">
                    <div class="row">
                        <div class="milderL">
                            <div class="col-md-6">

                                <%--<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">--%>

                                    <%--<div class="layui-tab-content">--%>
                                        <%--<div class="layui-tab-item layui-show">--%>
                                            <%--<ul class="layui-tab-title">--%>
                                                <%--<li class="layui-this" onclick="update()">订单详情</li>--%>



                                            <%--</ul></div>--%>
                                    <%--</div>--%>


                                <h5>订单编号:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${purchase.ordno}</h5>
                                <h5>订单时间:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${time}</h5><br><br>

                                <div class="order" style = "width:700px;height:400px;border: 0.7px solid black;">
                                    <div class="orderimg" style="border:40px solid white;">
                                      <c:if test="${purchase.ordertype == 0}">
                                        <img src="<%=request.getContextPath()%>/imgs/adopt_img1.jpg" style="height:200px;width:400px;">
                                    </c:if>
                                        <c:if test="${purchase.ordertype == 1}">
                                            <img src="${adoptCat.imgUrl}" style="height:200px;width:400px;">
                                        </c:if>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </div>
                        </div>
                        <div class="milderR">
                            <div class="col-md-6"
                                 style="border:0.3px solid gray; background-color:white;  padding:15px;height:250px;margin-top:290px">
                                <strong class="daily-family-name"><h3>${user.username}</h3></strong>
                                <h2>@膜蛤猫舍</h2>
                                <c:if test="${purchase.ordertype == 0}">
                                <div class="daily-family-content">
                                    寄养开始时间：&nbsp;&nbsp;&nbsp;&nbsp;${startTime}<br>
                                    寄养结束时间：&nbsp;&nbsp;&nbsp;&nbsp;${endTime}<br>
                                    寄养状态：&nbsp;&nbsp;&nbsp;&nbsp;${foster.fosterState}<br>

                                    备注内容：&nbsp;&nbsp;&nbsp;&nbsp;${foster.remark}<br>

                                </div>
                                </c:if>
                                <c:if test="${purchase.ordertype == 1}">
                                    领养订单时间：${data}<br>
                                    备注内容:${adoptApplication.note}
                                    
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <br>
</div>
</div>

<div class="layui-row" style="margin-top:50px;">

    <jsp:include page="../common/bottom.jsp"></jsp:include></div>
<script src="<%=request.getContextPath()%>/plugins/layui/layui.js" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    var inputs = document.getElementById("fff").getElementsByTagNames("input");
    var ls = [];
    for(var i=0;i<inputs.length;i++)
    {
        var type = inputs[i].getAttribute("type")
        if(!type||type==""||type=="text")
        {
            (function(ele){
                ls[ls.length] = ele
            }(inputs[i]))
        }

    }


</script>
<%--<script type="text/javascript">--%>
    <%--//默认提交状态为false--%>
    <%--var commitStatus = false;--%>
    <%--function dosubmit(){--%>
        <%--if(commitStatus==false){--%>
            <%--//提交表单后，讲提交状态改为true--%>
            <%--commitStatus = true;--%>
            <%--return true;--%>
        <%--}else{--%>
            <%--return false;--%>
        <%--}--%>
    <%--}--%>
<%--</script>--%>

</body>
</html>
