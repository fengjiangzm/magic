package tech.aistar.service.mycat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.mycat.CatDynamics;

import java.util.List;

/**
 * Created by Administrator on 2019/10/8.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCatDynamicsService {
    @Autowired
    private CatDynamicsService service;

    @Test
    public void findAll() {
        List<CatDynamics> catDynamics = service.findAll();

        System.out.println(catDynamics);
    }

    @Test
    public void save(){
        CatDynamics catDynamics = new CatDynamics(3,3,"sdlj",1,"akjshf");
        service.save(catDynamics);
    }

    @Test
    public void testfindById(){
        List<CatDynamics> catDynamics = service.findById(1);

        System.out.println(catDynamics);
    }
}
