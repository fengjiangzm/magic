package tech.aistar.service.foster;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IFosterServiceTest {

    @Autowired
    private IFosterService fosterService;

    @Test
    public void findAllPage() {
        System.out.println(fosterService.findAllPage(2, 3));
        System.out.println(fosterService.findAllPage(1, 3));
    }
}