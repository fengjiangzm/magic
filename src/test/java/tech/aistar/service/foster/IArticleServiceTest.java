package tech.aistar.service.foster;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.foster.Article;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class IArticleServiceTest {

    @Autowired
    private IArticleService articleService;
    @Test
    public void findByType() {
        for (Article article : articleService.findByType("learn", 0, 2)) {
            System.out.println(article);
        }

    }
}