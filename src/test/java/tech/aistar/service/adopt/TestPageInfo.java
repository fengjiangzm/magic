package tech.aistar.service.adopt;

import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.adopt.AdoptCat;

/**
 * Created by 陆锦鹏 on 2019/10/8.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestPageInfo {
    @Autowired
    private IAdoptCatService adoptCatService;

    @Test
    public void testPageInfo(){
        PageInfo<AdoptCat> catVoPageInfo = adoptCatService.find(null,"吕蕊男",1,2);

        System.out.println(catVoPageInfo);

        catVoPageInfo.getList().forEach(System.out::println);
    }
}
