package tech.aistar.mapper.mycat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.mycat.CatDetail;

import java.util.List;

/**
 * Created by Administrator on 2019/10/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCatDetailMapper {
    @Autowired
    private CatDetailMapper mapper;

    @Test
    public void testFindAll(){
        List<CatDetail> catDetails = (List<CatDetail>) mapper.findAll();

        System.out.println(catDetails);
    }
}
