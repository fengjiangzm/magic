package tech.aistar.mapper.mycat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.mycat.CatDynamics;

import java.util.List;

/**
 * Created by Administrator on 2019/10/8.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCatDynamicsMapper {
    @Autowired
    private CatDynamicsMapper mapper;

    @Test
    public void testFindAll(){
     List<CatDynamics> catDynamics = mapper.findAll();

        for (CatDynamics c : catDynamics){
            System.out.println(c);
        }
    }

    @Test
    public void testSave(){

        CatDynamics catDynamics = new CatDynamics(2,2,"kadjfgsdvh",1,"sdjhknasl");

        mapper.save(catDynamics);
    }

    @Test
    public void testFindById(){
        List<CatDynamics> catDynamics = mapper.findById(1);

        System.out.println(catDynamics);
    }
}
