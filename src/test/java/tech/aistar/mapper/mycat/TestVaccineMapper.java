package tech.aistar.mapper.mycat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.mycat.Vaccine;

/**
 * Created by Administrator on 2019/9/28.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestVaccineMapper {

    @Autowired
    private VaccineMapper mapper;

    @Test
    public void testSave(){
        Vaccine vaccine = new Vaccine(null,2,"abc",null,"案例",1,"16841515",1,"l看来是大家");
        mapper.save(vaccine);
    }
}
