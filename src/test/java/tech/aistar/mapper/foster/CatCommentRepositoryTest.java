package tech.aistar.mapper.foster;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.foster.CatComment;
import tech.aistar.entity.foster.Replay;
import tech.aistar.utill.DateUtil;

import java.util.ArrayList;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CatCommentRepositoryTest {

    @Autowired
    private CatCommentRepository catCommentRepository;

    @Test
    public void testAdd(){
        CatComment catComment = new CatComment("2", DateUtil.dateFormat(new Date()),"1","1","tom","3","评论内容", new ArrayList<Replay>());


        catCommentRepository.save(catComment);
    }


}