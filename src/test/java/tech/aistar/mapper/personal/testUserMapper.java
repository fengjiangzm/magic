package tech.aistar.mapper.personal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.personal.User;

/**
 * Created by LJX on 2019/9/24.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class testUserMapper {
    @Autowired
    private UserMapper userMapper;
    @Test
    public void testByName(){
        User u = userMapper.findByName("lvn");
        System.out.println(u);
    }
    @Test
    public void testsave(){
        User u = new User();
        u.setUsername("lvn2");
        u.setPassword("123456");
        userMapper.save(u);
    }


}
