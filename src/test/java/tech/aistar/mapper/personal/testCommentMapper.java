package tech.aistar.mapper.personal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.personal.OrderComment;

import java.util.Date;
import java.util.List;

/**
 * Created by LJX on 2019/9/24.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class testCommentMapper {
    @Autowired
    private OrderCommentMapper commentMapper;
    @Test
    public void testfindByOrderid(){
//        List<OrderComment> comments = commentMapper.findByOrderId(1);
//        comments.forEach(System.out::println);
        OrderComment comment = commentMapper.findByOrderId(1);
        System.out.println(comment);
    }
    @Test
    public void testupdate(){
        commentMapper.update(1);
    }

    @Test
    public void testfindById(){

        OrderComment orderComment= commentMapper.findById(1);
    }
    @Test
    public void testsave(){
        OrderComment comment=new OrderComment(null,1,new Date(),"环境不错，猫咪与猫咪之间回隔离开，价格也很优惠，服务也很好",1,0);

        commentMapper.save(comment);
    }
}
