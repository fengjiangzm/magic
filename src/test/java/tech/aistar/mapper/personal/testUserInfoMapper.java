package tech.aistar.mapper.personal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.personal.User;
import tech.aistar.entity.personal.UserInfo;

import java.util.Date;
import java.util.List;


/**
 * Created by LJX on 2019/9/24.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class testUserInfoMapper {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private UserMapper userMapper;
    @Test
    public void testfindByname(){
       List<UserInfo> userInfos = userInfoMapper.findByName("love");
         userInfos.forEach(System.out::println);
    }
    @Test
    public void testupdate(){
        userInfoMapper.update(1);
    }

    @Test
    public void testsave(){
        User u = new User(null,"陆锦鹏","201908");
        userMapper.save(u);
        UserInfo userInfo = new UserInfo
                (u.getId(),0,new Date(),1,new Date(),"192.168.1.138","love","15776548507","123",0,new Date(),new Date(),"12");

        userInfoMapper.save(userInfo);

    }
    @Test
    public void testupdateById(){
        UserInfo userInfo = new UserInfo(2,0,new Date(),1,new Date(),"192.168.1.152","hate","17504598196","123",0,new Date(),new Date(),"2423765@qq.com");
          userInfoMapper.updateById(userInfo);

    }



}
