package tech.aistar.mapper.personal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import tech.aistar.entity.personal.Purchase;

import java.util.Date;
import java.util.List;

/**
 * Created by LJX on 2019/9/25.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class testPurchaseMapper {
    @Autowired
    private PurchaseMapper purchaseMapper;
    @Test
    public void testUpdate(){
        purchaseMapper.update(1);
    }
    @Test
    public void testsave(){
        Purchase purchase = new Purchase(null,"2345",new Date(),2,0,0,0,200.00,1,1);
        purchaseMapper.save(purchase);
    }
    @Test
    public void testfinduserId(){
        List<Purchase> purchases = purchaseMapper.findByuserId(2);
        purchases.forEach(System.out::println);
    }
    @Test
    public void testfindByOrdno(){
        Purchase p = purchaseMapper.findByOrderno("1234");
        System.out.println(p);
    }
}
