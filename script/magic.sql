/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : magic

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-09-27 16:42:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `adopt_application`
-- ----------------------------
DROP TABLE IF EXISTS `adopt_application`;
CREATE TABLE `adopt_application` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `salary` float(10,2) DEFAULT NULL,
  `userAddress_id` int(5) DEFAULT NULL,
  `ord_id` int(10) DEFAULT NULL,
  `taking_way` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adopt_application
-- ----------------------------
INSERT INTO `adopt_application` VALUES ('1', '冯江', '1', '19237109184', '99999.99', '1', '1', '0', '1');
INSERT INTO `adopt_application` VALUES ('2', '姜德明', '2', '19237109184', '88888.88', '2', '2', '1', '1');

-- ----------------------------
-- Table structure for `adopt_cat`
-- ----------------------------
DROP TABLE IF EXISTS `adopt_cat`;
CREATE TABLE `adopt_cat` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `cat_id` int(5) DEFAULT NULL,
  `growth_id` int(2) DEFAULT NULL,
  `cat_price` float(10,2) DEFAULT NULL,
  `introduction` varchar(1000) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of adopt_cat
-- ----------------------------
INSERT INTO `adopt_cat` VALUES ('1', '1', '1', '1234.50', '这是一只无毛猫', '0');
INSERT INTO `adopt_cat` VALUES ('2', '2', '2', '1234.50', '这是一只波斯猫', '1');
INSERT INTO `adopt_cat` VALUES ('3', '3', '3', '1234.50', '这是一只无毛猫', '0');
INSERT INTO `adopt_cat` VALUES ('4', '4', '4', '1234.50', '这是一只无毛猫', '0');
INSERT INTO `adopt_cat` VALUES ('5', '5', '5', '1234.50', '这是一只无毛猫', '0');

-- ----------------------------
-- Table structure for `cat`
-- ----------------------------
DROP TABLE IF EXISTS `cat`;
CREATE TABLE `cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) DEFAULT '' COMMENT 'name',
  `age` int(11) DEFAULT '-1' COMMENT 'age',
  `sex` int(11) DEFAULT '-1' COMMENT 'sex',
  `weight` double(16,4) DEFAULT '-1.0000' COMMENT 'weight',
  `is_sterilization` int(11) DEFAULT '-1' COMMENT 'isSterilization',
  `species_id` int(11) DEFAULT NULL COMMENT 'species',
  `user_id` int(11) DEFAULT '-1' COMMENT 'userId',
  `status` int(11) DEFAULT '-1' COMMENT 'status',
  `img_url` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='`cat`';

-- ----------------------------
-- Records of cat
-- ----------------------------
INSERT INTO `cat` VALUES ('1', 'a', '1', '0', '1.0000', '1', '1', '1', '1', null);
INSERT INTO `cat` VALUES ('2', 'b', '1', '0', '1.0000', '1', '1', '1', '0', null);
INSERT INTO `cat` VALUES ('3', 'c', '1', '0', '1.0000', '1', '1', '1', '0', null);
INSERT INTO `cat` VALUES ('4', '冯江', '3', '1', '20.0000', '1', '2', '2', '0', null);
INSERT INTO `cat` VALUES ('5', '六', '3', '1', '20.0000', '1', '2', '2', '0', null);
INSERT INTO `cat` VALUES ('6', 'QI', '3', '1', '20.0000', '1', '2', '2', '0', null);
INSERT INTO `cat` VALUES ('7', '六', '3', '1', '20.0000', '1', '2', '2', '0', null);

-- ----------------------------
-- Table structure for `cat_comment`
-- ----------------------------
DROP TABLE IF EXISTS `cat_comment`;
CREATE TABLE `cat_comment` (
  `comment_id` int(20) NOT NULL AUTO_INCREMENT,
  `cat_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `content` text NOT NULL,
  `state` int(4) NOT NULL,
  `prase_count` int(11) NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cat_comment
-- ----------------------------
INSERT INTO `cat_comment` VALUES ('1', '2', '2', '222', '2', '2', null);
INSERT INTO `cat_comment` VALUES ('2', '1', '1', '123', '0', '0', null);
INSERT INTO `cat_comment` VALUES ('3', '1', '1', '123', '0', '0', '2019-09-26 19:05:55');

-- ----------------------------
-- Table structure for `cat_growth`
-- ----------------------------
DROP TABLE IF EXISTS `cat_growth`;
CREATE TABLE `cat_growth` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cat_id` int(11) DEFAULT '-1' COMMENT 'catId',
  `birthday` date DEFAULT '1000-01-01' COMMENT 'birthday',
  `cat_age` int(11) DEFAULT '-1' COMMENT 'catAge',
  `description` varchar(50) DEFAULT '' COMMENT 'description',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='`cat_growth`';

-- ----------------------------
-- Records of cat_growth
-- ----------------------------
INSERT INTO `cat_growth` VALUES ('1', '1', '2019-09-09', '1', '你的猫咪呆的可好了');
INSERT INTO `cat_growth` VALUES ('2', '2', '1000-01-01', '1', '你的猫咪呆的可好了');

-- ----------------------------
-- Table structure for `cat_live_request`
-- ----------------------------
DROP TABLE IF EXISTS `cat_live_request`;
CREATE TABLE `cat_live_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `species_name` varchar(50) DEFAULT '' COMMENT 'speciesName',
  `text` varchar(50) DEFAULT '' COMMENT 'text',
  `species_id` int(5) DEFAULT NULL,
  `cat_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='`cat_live_request`';

-- ----------------------------
-- Records of cat_live_request
-- ----------------------------
INSERT INTO `cat_live_request` VALUES ('1', '英短', '这猫很好看', '1', '1');

-- ----------------------------
-- Table structure for `cat_species`
-- ----------------------------
DROP TABLE IF EXISTS `cat_species`;
CREATE TABLE `cat_species` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `species_name` varchar(50) DEFAULT '' COMMENT 'speciesName',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='`cat_species`';

-- ----------------------------
-- Records of cat_species
-- ----------------------------
INSERT INTO `cat_species` VALUES ('1', '英短');
INSERT INTO `cat_species` VALUES ('2', '无毛猫');

-- ----------------------------
-- Table structure for `cat_vaccine`
-- ----------------------------
DROP TABLE IF EXISTS `cat_vaccine`;
CREATE TABLE `cat_vaccine` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cat_id` int(11) DEFAULT '-1' COMMENT 'catId',
  `vaccine_id` int(11) DEFAULT '-1' COMMENT 'vaccineId',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='`cat_vaccine`';

-- ----------------------------
-- Records of cat_vaccine
-- ----------------------------
INSERT INTO `cat_vaccine` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `collect`
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) DEFAULT NULL,
  `cat_id` int(5) DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES ('1', '1', '1', '2019-09-25 12:13:39', '1');
INSERT INTO `collect` VALUES ('2', '2', '2', '2019-09-25 12:17:12', '1');

-- ----------------------------
-- Table structure for `combo`
-- ----------------------------
DROP TABLE IF EXISTS `combo`;
CREATE TABLE `combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foster_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of combo
-- ----------------------------
INSERT INTO `combo` VALUES ('1', '1', '1');
INSERT INTO `combo` VALUES ('2', '1', '1');
INSERT INTO `combo` VALUES ('3', '1', '1');
INSERT INTO `combo` VALUES ('4', '1', '1');
INSERT INTO `combo` VALUES ('5', '1', '1');
INSERT INTO `combo` VALUES ('6', '1', '1');
INSERT INTO `combo` VALUES ('7', '1', '1');
INSERT INTO `combo` VALUES ('8', '2', '2');
INSERT INTO `combo` VALUES ('9', '2', '2');
INSERT INTO `combo` VALUES ('10', '2', '2');

-- ----------------------------
-- Table structure for `event`
-- ----------------------------
DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` varchar(23) NOT NULL,
  `price` double(11,2) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of event
-- ----------------------------
INSERT INTO `event` VALUES ('1', '剪毛', '10.00', '给你的猫猫剪一个帅气独特的发型');
INSERT INTO `event` VALUES ('2', '洗澡', '40.00', '为什么贵.因为猫会动');
INSERT INTO `event` VALUES ('3', '绝育', '998.00', '不要999,只要998.给您的宠物一个交代');

-- ----------------------------
-- Table structure for `foster`
-- ----------------------------
DROP TABLE IF EXISTS `foster`;
CREATE TABLE `foster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` date NOT NULL,
  `end_time` date NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `remark` text,
  `order_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of foster
-- ----------------------------
INSERT INTO `foster` VALUES ('3', '1', '1', '2019-09-01', '2019-09-02', null, '123', '1', '0');
INSERT INTO `foster` VALUES ('4', '1', '1', '2019-09-01', '2019-09-02', '1', '321', '1', '0');
INSERT INTO `foster` VALUES ('5', '1', '1', '2019-09-01', '2019-09-02', '1', 'qwe', '1', '0');
INSERT INTO `foster` VALUES ('6', '3', '1', '2019-09-01', '2019-09-26', '2', '', '1', '0');
INSERT INTO `foster` VALUES ('7', '1', '1', '2019-08-31', '2019-09-01', '1', 'asd', '1', '0');

-- ----------------------------
-- Table structure for `order_comment`
-- ----------------------------
DROP TABLE IF EXISTS `order_comment`;
CREATE TABLE `order_comment` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `order_id` int(7) DEFAULT NULL,
  `order_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_id` int(7) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_comment
-- ----------------------------
INSERT INTO `order_comment` VALUES ('1', '1', '121', '123211', '10', '1');
INSERT INTO `order_comment` VALUES ('2', '1', '0', '环境不错，猫咪与猫咪之间回隔离开，价格也很优惠，服务也很好', '1', '0');

-- ----------------------------
-- Table structure for `purchase`
-- ----------------------------
DROP TABLE IF EXISTS `purchase`;
CREATE TABLE `purchase` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `ordno` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '订单号',
  `time` timestamp NULL DEFAULT NULL COMMENT '订单时间',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `ordercancel` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `ordmoney` double(10,2) DEFAULT NULL COMMENT '订单价钱',
  `payment` int(2) DEFAULT NULL,
  `comment_id` int(7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase
-- ----------------------------
INSERT INTO `purchase` VALUES ('1', '1234', '2019-09-25 18:32:06', '1', '0', '1', '323.00', '0', '1');
INSERT INTO `purchase` VALUES ('2', '2345', '2019-09-26 21:41:38', '2', '0', '0', '200.00', '0', '1');
INSERT INTO `purchase` VALUES ('3', '3456', '2019-09-26 21:43:40', '2', '0', '0', '200.00', '0', '1');

-- ----------------------------
-- Table structure for `replay`
-- ----------------------------
DROP TABLE IF EXISTS `replay`;
CREATE TABLE `replay` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `comment_id` int(7) DEFAULT NULL,
  `reply_id` int(7) DEFAULT NULL,
  `reply_type` varchar(200) DEFAULT NULL,
  `content` varchar(200) DEFAULT NULL,
  `from_uid` int(7) DEFAULT NULL,
  `to_uid` int(7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of replay
-- ----------------------------

-- ----------------------------
-- Table structure for `room`
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES ('1', '大', '8');
INSERT INTO `room` VALUES ('2', '中', '19');
INSERT INTO `room` VALUES ('3', '小', '40');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_bin NOT NULL,
  `password` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'lvn', '123456');
INSERT INTO `user` VALUES ('2', 'lvn1', '123456');
INSERT INTO `user` VALUES ('3', 'lvn2', '123456');
INSERT INTO `user` VALUES ('4', '姜德明', '201908');
INSERT INTO `user` VALUES ('5', '姜德明', '201908');
INSERT INTO `user` VALUES ('7', '姜德明', '201908');
INSERT INTO `user` VALUES ('8', '姜德明', '201908');
INSERT INTO `user` VALUES ('11', '冯江', '201908');
INSERT INTO `user` VALUES ('12', '陆锦鹏', '201908');
INSERT INTO `user` VALUES ('13', '冯将', '199808');
INSERT INTO `user` VALUES ('14', 'lvn2', '123456');
INSERT INTO `user` VALUES ('15', '陆锦鹏', '123432');
INSERT INTO `user` VALUES ('16', '陆锦鹏', '123432');
INSERT INTO `user` VALUES ('17', '陆锦鹏', '123432');
INSERT INTO `user` VALUES ('18', 'asjh', '123');
INSERT INTO `user` VALUES ('19', 'tom', '123');

-- ----------------------------
-- Table structure for `user_address`
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `province_name` varchar(100) DEFAULT NULL,
  `city_name` varchar(100) DEFAULT NULL,
  `area_name` varchar(100) DEFAULT NULL,
  `detail_address` varchar(1000) DEFAULT NULL,
  `isdefault` int(2) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_address
-- ----------------------------
INSERT INTO `user_address` VALUES ('1', '公司', '1', '江苏省', '苏州市', '工业园区', '星湖街328号', '1', '1');
INSERT INTO `user_address` VALUES ('2', '公司', '2', '江苏省', '苏州市', '工业园区', '星湖街328号', '1', '1');

-- ----------------------------
-- Table structure for `user_info`
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` int(2) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `cat_id` int(11) DEFAULT NULL COMMENT '猫咪id',
  `login_time` timestamp NULL DEFAULT NULL COMMENT '登录时间',
  `login_ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登录的ip地址',
  `nickname` varchar(63) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '昵称',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '头像',
  `status` int(3) DEFAULT NULL COMMENT '状态',
  `add_time` timestamp NULL DEFAULT NULL COMMENT '添加信息时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改信息时间',
  `useremail` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1', '0', '2018-05-01', null, '2019-09-11 12:24:28', '12321', '吕蕊男', '13836448048', 'de12', '1', '2019-09-25 00:00:00', '2019-09-25 00:00:00', '2830308340@qq.com');
INSERT INTO `user_info` VALUES ('2', '0', '2019-09-25', '1', '2019-09-25 16:53:01', '192.168.1.152', 'hate', '17504598196', '123', '1', '2019-09-25 16:53:01', '2019-09-25 16:53:01', '2423765@qq.com');
INSERT INTO `user_info` VALUES ('3', '0', '2019-09-25', '1', '2019-09-25 16:26:45', '17221', 'love', '1232', '123', '0', '2019-09-25 16:26:45', '2019-09-25 16:26:45', '12');
INSERT INTO `user_info` VALUES ('4', '0', '2019-09-25', '1', '2019-09-25 16:28:07', '17221', 'love', '1232', '123', '0', '2019-09-25 16:28:07', '2019-09-25 16:28:07', '12');
INSERT INTO `user_info` VALUES ('5', '0', '2019-09-25', '1', '2019-09-25 16:28:48', '17221', 'love', '1232', '123', '0', '2019-09-25 16:28:48', '2019-09-25 16:28:48', '12');
INSERT INTO `user_info` VALUES ('6', '0', '2019-09-25', '1', '2019-09-25 16:30:33', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:30:33', '2019-09-25 16:30:33', '12');
INSERT INTO `user_info` VALUES ('7', '0', '2019-09-25', '1', '2019-09-25 16:33:46', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:33:46', '2019-09-25 16:33:46', '12');
INSERT INTO `user_info` VALUES ('8', '0', '2019-09-25', '1', '2019-09-25 16:34:32', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:34:32', '2019-09-25 16:34:32', '12');
INSERT INTO `user_info` VALUES ('9', '0', '2019-09-25', '1', '2019-09-25 16:36:40', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:36:40', '2019-09-25 16:36:40', '12');
INSERT INTO `user_info` VALUES ('10', '0', '2019-09-25', '1', '2019-09-25 16:37:31', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:37:31', '2019-09-25 16:37:31', '12');
INSERT INTO `user_info` VALUES ('11', '0', '2019-09-25', '1', '2019-09-25 16:37:49', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:37:49', '2019-09-25 16:37:49', '12');
INSERT INTO `user_info` VALUES ('12', '0', '2019-09-25', '1', '2019-09-25 16:39:19', '192.168.1.138', 'love', '15776548507', '123', '0', '2019-09-25 16:39:19', '2019-09-25 16:39:19', '12');
INSERT INTO `user_info` VALUES ('13', '1', '2019-09-26', '1', '2019-09-26 16:55:24', '192.168.1.152', 'hate', '17504598196', '123', '0', '2019-09-26 16:55:24', '2019-09-26 16:55:24', '2423765@qq.com');
INSERT INTO `user_info` VALUES ('14', null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `vaccine`
-- ----------------------------
DROP TABLE IF EXISTS `vaccine`;
CREATE TABLE `vaccine` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cat_id` int(11) DEFAULT '-1' COMMENT 'catId',
  `vaccine_name` varchar(50) DEFAULT '' COMMENT 'vaccineName',
  `date` date DEFAULT '1000-01-01' COMMENT 'date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='`vaccine`';

-- ----------------------------
-- Records of vaccine
-- ----------------------------
INSERT INTO `vaccine` VALUES ('1', '1', '疫苗', '1000-01-01');
